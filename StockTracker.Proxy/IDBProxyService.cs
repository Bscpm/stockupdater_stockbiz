﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace StockTracker.Proxy
{
    [ServiceContract]
    public interface IDBProxyService
    {
        #region InfoGate 3
        [OperationContract]
        void IG3_UpdateIndexInfo(string xml);

        [OperationContract]
        void IG3_UpdateBasketInfo(string xml);

        [OperationContract]
        void IG3_UpdateStockSymbol(string xml);

        [OperationContract]
        void IG3_UpdateStockPriceInfo(string xml);

        [OperationContract]
        void IG3_UpdatePutThrough(string xml);

        [OperationContract]
        void IG3_ResetStockTop3Price();

        [OperationContract]
        void IG3_ResetTopNPrice();

        [OperationContract]
        void IG3_UpdateStockTop3Price(string xml);

        [OperationContract]
        void IG3_UpdateTopNPrice(string xml);
        //[OperationContract]
        //void IG3_ResetPutAdInfo();

        //[OperationContract]
        //void IG3_InsertPutAd(string xml);
        #endregion

        [OperationContract]
        void HAGW_UpdateMarketInfo(DateTime date, string status, double indexBasic, double indexCurrent, double indexHigh, double indexLow, double totalTrade, double totalVolume, double putthroughVolume, double totalValue, double putthroughValue, int advances, int nochange, int declines);

        [OperationContract]
        void HAGW_UpdateMarketDetails(string xml);

        [OperationContract]
        void HAGW_UpdateStockPriceInfo(string xml);

        [OperationContract]
        void HAGW_UpdatePutThrough(string xml);

        [OperationContract]
        void HAGW_ProcessDeList(string symbols);

        [OperationContract]
        void UpdateNewSymbol(string stockID, string symbol, string exchange, int type);

        [OperationContract]
        void HAGW_ResetStockTop3Price();

        [OperationContract]
        void HAGW_UpdateStockTop3Price(string xml);

        [OperationContract]
        void HAGW_ResetPutAdInfo();

        [OperationContract]
        void HAGW_InsertPutAd(string xml);

        [OperationContract]
        void HOGW_UpdateMarketInfo(DateTime date, string status, double indexCurrent, double totalTrade, double totalVolume, double totalValue, int advances, int nochange, int declines);

        [OperationContract]
        void HOGW_UpdateVN30Info(DateTime date, double indexCurrent, double totalVolume, double totalValue, int advances, int nochange, int declines);

        [OperationContract]
        void HOGW_UpdateHOSEIndicesInfo(string index, DateTime date, double indexCurrent, double totalVolume, double totalValue, double totalVolumePT, double totalValuePT, int advances, int nochange, int declines, int ceiling, int floor);

        [OperationContract]
        void HOGW_Update3SessionsInfo(double totalTrade1, double totalShare1, double totalValue1, double index1, double totalTrade2, double totalShare2, double totalValue2, double index2, double totalTrade3, double totalShare3, double totalValue3, double index3);

        [OperationContract]
        void HOGW_ResetMarketInfo();

        [OperationContract]
        void HOGW_UpdateStockInfo(string xml);

        [OperationContract]
        void HOGW_DeleteLEInfo();

        [OperationContract]
        void HOGW_UpdateLEInfo(string xml);

        [OperationContract]
        void UpdateListingStatus(string symbol, bool isListing);

        [OperationContract]
        void HOGW_DeleteAllPutAd();

        [OperationContract]
        void HOGW_InsertPutAd(string xml);

        [OperationContract]
        void HOGW_DeleteAllPutExec();

        [OperationContract]
        void HOGW_InsertPutExec(string xml);

        [OperationContract]
        void HOGW_UpdateCurrentFRoom(string xml);

        [OperationContract]
        void HOGW_ResetStockVolume();

        [OperationContract]
        void UPCOM_UpdateMarketInfo(DateTime date, string status, double indexBasic, double indexCurrent, double totalTrade, double totalVolume, double putthroughVolume, double totalValue, int advances, int nochange, int declines);

        [OperationContract]
        void UPCOM_UpdateStockPriceInfo(string xml);

        [OperationContract]
        void UPCOM_ResetStockTop3Price();

        [OperationContract]
        void UPCOM_UpdateStockTop3Price(string xml);
    }
}
