﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Configuration;
using System.Data.Common;

namespace StockTracker.Proxy
{
    public class DBProxyService : IDBProxyService
    {
        private bool Enabled { get; set; }
        private bool UpdateHSX { get; set; }
        private bool UpdateHSXIndices { get; set; }
        private bool UpdateHNX { get; set; }
        private bool UpdateUPCOM { get; set; }

        public DBProxyService()
        {
            Enabled = ConfigurationManager.AppSettings["Enabled"] == "true" ? true : false;
            UpdateHSX = ConfigurationManager.AppSettings["UpdateHSX"] == "true" ? true : false;
            UpdateHSXIndices = ConfigurationManager.AppSettings["UpdateHSXIndices"] == "true" ? true : false;
            UpdateHNX = ConfigurationManager.AppSettings["UpdateHNX"] == "true" ? true : false;
            UpdateUPCOM = ConfigurationManager.AppSettings["UpdateUPCOM"] == "true" ? true : false;
        }

        #region InfoGate 3
        public void IG3_UpdateIndexInfo(string xml)
        {
            if (!Enabled)
            {
                return;
            }

            try
            {
                Database db = DatabaseFactory.CreateDatabase();
                db.ExecuteNonQuery("IG3_UpdateIndexInfo", xml);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void IG3_UpdateBasketInfo(string xml)
        {
            if (!Enabled)
            {
                return;
            }

            try
            {
                Database db = DatabaseFactory.CreateDatabase();
                db.ExecuteNonQuery("IG3_UpdateBasketInfo", xml);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void IG3_UpdateStockSymbol(string xml)
        {
            if (!Enabled)
            {
                return;
            }

            try
            {
                Database db = DatabaseFactory.CreateDatabase();
                db.ExecuteNonQuery("IG3_UpdateStockSymbol", xml);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void IG3_UpdateStockPriceInfo(string xml)
        {
            if (!Enabled)
            {
                return;
            }

            try
            {
                Database db = DatabaseFactory.CreateDatabase();
                db.ExecuteNonQuery("IG3_UpdateBasketInfo", xml);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void IG3_UpdatePutThrough(string xml)
        {

            if (!Enabled)
            {
                return;
            }

            try
            {
                Database db = DatabaseFactory.CreateDatabase();
                db.ExecuteNonQuery("IG3_UpdatePutThrough", xml);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void IG3_ResetStockTop3Price()
        {
            if (!Enabled)
            {
                return;
            }

            try
            {
                Database db = DatabaseFactory.CreateDatabase();
                db.ExecuteNonQuery("IG3_ResetStockTop3Price");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void IG3_ResetTopNPrice()
        {
            if (!Enabled)
            {
                return;
            }

            try
            {
                Database db = DatabaseFactory.CreateDatabase();
                db.ExecuteNonQuery("IG3_ResetTopNPrice");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void IG3_UpdateStockTop3Price(string xml)
        {
            if (!Enabled)
            {
                return;
            }

            try
            {
                Database db = DatabaseFactory.CreateDatabase();
                db.ExecuteNonQuery("IG3_UpdateStockTop3Price", xml);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void IG3_UpdateTopNPrice(string xml)
        {
            if (!Enabled)
            {
                return;
            }

            try
            {
                Database db = DatabaseFactory.CreateDatabase();
                db.ExecuteNonQuery("IG3_UpdateTopNPrice", xml);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        #endregion

        public void HAGW_UpdateMarketInfo(DateTime date, string status, double indexBasic, double indexCurrent, double indexHigh, double indexLow, double totalTrade, double totalVolume, double putthroughVolume, double totalValue, double putthroughValue, int advances, int nochange, int declines)
        {
            if (Enabled && UpdateHNX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HAGW_UpdateMarketInfo", date, status, indexBasic, indexCurrent, indexHigh, indexLow, totalTrade, totalVolume, putthroughVolume, totalValue, putthroughValue, advances, nochange, declines);
                    Console.WriteLine("HAGW_UpdateMarketInfo completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HAGW_UpdateMarketInfo. Detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void HAGW_UpdateMarketDetails(string xml)
        {
            if (Enabled && UpdateHNX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HAGW_UpdateMarketDetails", xml);
                    Console.WriteLine("HAGW_UpdateMarketDetails completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HAGW_UpdateMarketDetails. Detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void HAGW_UpdateStockPriceInfo(string xml)
        {
            if (Enabled && UpdateHNX)
            {
                try
                {
                    DateTime startTime = DateTime.Now;
                    Console.WriteLine("HAGW_UpdateStockPriceInfo start from [" + startTime + "].");
                    Database db = DatabaseFactory.CreateDatabase();
                    DbCommand cmd = db.GetStoredProcCommand("HAGW_UpdateStockPriceInfo", xml);
                    cmd.CommandTimeout = 600;// 600 seconds
                    //cmd.ExecuteNonQuery();
                    db.ExecuteNonQuery(cmd);
                    DateTime endTime = DateTime.Now;
                    //db.ExecuteNonQuery(("HAGW_UpdateStockPriceInfo", xml);
                    Console.WriteLine("HAGW_UpdateStockPriceInfo completed [" + (endTime.Subtract(startTime).Milliseconds) + " ms].");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HAGW_UpdateStockPriceInfo. Detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void HAGW_UpdatePutThrough(string xml)
        {
            if (Enabled && UpdateHNX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HAGW_UpdatePutThrough", xml);
                    Console.WriteLine("HAGW_UpdatePutThrough completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HAGW_UpdatePutThrough. Detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void HAGW_ProcessDeList(string symbols)
        {
            if (Enabled && UpdateHNX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HAGW_ProcessDeList", symbols);
                    Console.WriteLine("HAGW_ProcessDeList completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HAGW_ProcessDeList. Detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void UpdateNewSymbol(string stockID, string symbol, string exchange, int type)
        {
            if (!Enabled)
            {
                return;
            }

            try
            {
                Database db = DatabaseFactory.CreateDatabase();
                db.ExecuteNonQuery("stock_Symbols_UpdateNewSymbol", stockID, symbol, exchange, type);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void HAGW_ResetStockTop3Price()
        {
            if (Enabled && UpdateHNX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HAGW_ResetStockTop3Price");
                    Console.WriteLine("HAGW_ResetStockTop3Price completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HAGW_ResetStockTop3Price. Detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void HAGW_UpdateStockTop3Price(string xml)
        {
            if (Enabled && UpdateHNX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HAGW_UpdateStockTop3Price", xml);
                    Console.WriteLine("HAGW_UpdateStockTop3Price completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HAGW_UpdateStockTop3Price. Detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void HAGW_ResetPutAdInfo()
        {
            if (Enabled && UpdateHNX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HAGW_ResetPutAdInfo");
                    Console.WriteLine("HAGW_ResetPutAdInfo completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HAGW_ResetPutAdInfo. Detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void HAGW_InsertPutAd(string xml)
        {
            if (Enabled && UpdateHNX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HAGW_InsertPutAd", xml);
                    Console.WriteLine("HAGW_InsertPutAd completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HAGW_InsertPutAd. Detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void HOGW_UpdateMarketInfo(DateTime date, string status, double indexCurrent, double totalTrade, double totalVolume, double totalValue, int advances, int nochange, int declines)
        {
            if (Enabled && UpdateHSX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HOGW_UpdateMarketInfo", date, status, indexCurrent, totalTrade, totalVolume, totalValue, advances, nochange, declines);
                    Console.WriteLine("Update HOSE's market info complete!");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HOGW_UpdateMarketInfo, detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void HOGW_UpdateVN30Info(DateTime date, double indexCurrent, double totalVolume, double totalValue, int advances, int nochange, int declines)
        {
            if (Enabled && UpdateHSXIndices)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HOGW_UpdateVN30Info", date, indexCurrent, totalVolume, totalValue, advances, nochange, declines);
                    Console.WriteLine("Update VN30 info complete!");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HOGW_UpdateVN30Info, detail:" + e.Message);
                    Console.WriteLine(string.Format("Date={0}, IndexCurrent={1},TotalVolume={2}, TotalValue={3}, Advances={4}, NoChanges={5}, Declines={6}",
                        date.ToString("dd/MM/yyyy HH:mm:ss"), indexCurrent,
                        totalVolume, totalValue, advances, nochange, declines));
                    Console.ResetColor();
                }
            }
        }

        public void HOGW_UpdateHOSEIndicesInfo(string symbol, DateTime date, double indexCurrent, double totalVolume, double totalValue, double totalVolumePT, double totalValuePT, int advances, int nochange, int declines, int ceiling, int floor)
        {
            if (Enabled && UpdateHSXIndices)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HOGW_UpdateHOSEIndicesInfo", symbol, date, indexCurrent, totalVolume, totalValue, totalVolumePT, totalValuePT, advances, nochange, declines, ceiling, floor);
                    Console.WriteLine("HOGW_UpdateHOSEIndicesInfo '" + symbol + "' completed at " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HOGW_UpdateHOSEIndicesInfo, detail:" + e.Message);
                    Console.WriteLine(string.Format("Symbol = {0}, Date={1}, IndexCurrent={2},TotalVolume={3}, TotalValue={4}, TotalVolumePT={5}, TotalValuePT={6}, Advances={7}, NoChanges={8}, Declines={9}, Ceiling={10}, Floor={11}",
                        symbol, date.ToString("dd/MM/yyyy HH:mm:ss"), indexCurrent,
                        totalVolume, totalValue, totalVolumePT, totalValuePT, advances, nochange, declines, ceiling, floor));
                    Console.ResetColor();
                }
            }
        }

        public void HOGW_Update3SessionsInfo(double totalTrade1, double totalShare1, double totalValue1, double index1, double totalTrade2, double totalShare2, double totalValue2, double index2, double totalTrade3, double totalShare3, double totalValue3, double index3)
        {
            if (Enabled && UpdateHSX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HOGW_Update3SessionsInfo", totalTrade1, totalShare1, totalValue1, index1, totalTrade2, totalShare2, totalValue2, index2, totalTrade3, totalShare3, totalValue3, index3);
                    Console.WriteLine("HOGW_Update3SessionsInfo completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HOGW_Update3SessionsInfo, detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void HOGW_ResetMarketInfo()
        {
            if (Enabled && UpdateHSX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HOGW_ResetMarketInfo");
                    Console.WriteLine("HOGW_ResetMarketInfo completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HOGW_ResetMarketInfo, detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void HOGW_UpdateStockInfo(string xml)
        {
            if (Enabled && UpdateHSX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HOGW_UpdateStockInfo", xml);
                    Console.WriteLine("HOGW_UpdateStockInfo completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HOGW_UpdateStockInfo, detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void HOGW_DeleteLEInfo()
        {
            if (Enabled && UpdateHSX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HOGW_DeleteLEInfo");
                    Console.WriteLine("HOGW_DeleteLEInfo completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HOGW_DeleteLEInfo, detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void HOGW_UpdateLEInfo(string xml)
        {
            if (Enabled && UpdateHSX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HOGW_UpdateLEInfo", xml);
                    Console.WriteLine("HOGW_UpdateLEInfo completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HOGW_UpdateLEInfo, detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void UpdateListingStatus(string symbol, bool isListing)
        {
            if (!Enabled)
            {
                return;
            }

            try
            {
                Database db = DatabaseFactory.CreateDatabase();
                db.ExecuteNonQuery("stock_Symbols_UpdateListingStatus", symbol, isListing);
                Console.WriteLine("UpdateListingStatus completed");
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error in UpdateListingStatus, detail:" + e.Message);
                Console.ResetColor();
            }
        }

        public void HOGW_DeleteAllPutAd()
        {
            if (Enabled && UpdateHSX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HOGW_DeleteAllPutAd");
                    Console.WriteLine("HOGW_DeleteAllPutAd completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HOGW_DeleteAllPutAd, detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void HOGW_InsertPutAd(string xml)
        {
            if (Enabled && UpdateHSX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HOGW_InsertPutAd", xml);
                    Console.WriteLine("HOGW_InsertPutAd completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HOGW_InsertPutAd, detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void HOGW_DeleteAllPutExec()
        {
            if (Enabled && UpdateHSX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HOGW_DeleteAllPutExec");
                    Console.WriteLine("HOGW_DeleteAllPutExec completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HOGW_DeleteAllPutExec, detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void HOGW_InsertPutExec(string xml)
        {
            if (Enabled && UpdateHSX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HOGW_InsertPutExec", xml);
                    Console.WriteLine("HOGW_InsertPutExec completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HOGW_InsertPutExec, detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void HOGW_UpdateCurrentFRoom(string xml)
        {
            if (Enabled && UpdateHSX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HOGW_UpdateCurrentFRoom", xml);
                    Console.WriteLine("HOGW_UpdateCurrentFRoom completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HOGW_UpdateCurrentFRoom, detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void HOGW_ResetStockVolume()
        {
            if (Enabled && UpdateHSX)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("HOGW_ResetStockVolume");
                    Console.WriteLine("HOGW_ResetStockVolume completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HOGW_ResetStockVolume, detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void UPCOM_UpdateMarketInfo(DateTime date, string status, double indexBasic, double indexCurrent, double totalTrade, double totalVolume, double putthroughVolume, double totalValue, int advances, int nochange, int declines)
        {
            if (Enabled && UpdateUPCOM)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("UPCOM_UpdateMarketInfo", date, status, indexBasic, indexCurrent, totalTrade, totalVolume, putthroughVolume, totalValue, advances, nochange, declines);
                    Console.WriteLine("UPCOM_UpdateMarketInfo completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in UPCOM_UpdateMarketInfo, detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void UPCOM_UpdateStockPriceInfo(string xml)
        {
            if (Enabled && UpdateUPCOM)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("UPCOM_UpdateStockPriceInfo", xml);
                    Console.WriteLine("UPCOM_UpdateStockPriceInfo completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in UPCOM_UpdateStockPriceInfo, detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void UPCOM_ResetStockTop3Price()
        {
            if (Enabled && UpdateUPCOM)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("UPCOM_ResetStockTop3Price");
                    Console.WriteLine("UPCOM_ResetStockTop3Price completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in UPCOM_ResetStockTop3Price, detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }

        public void UPCOM_UpdateStockTop3Price(string xml)
        {
            if (Enabled && UpdateUPCOM)
            {
                try
                {
                    Database db = DatabaseFactory.CreateDatabase();
                    db.ExecuteNonQuery("UPCOM_UpdateStockTop3Price", xml);
                    Console.WriteLine("UPCOM_UpdateStockTop3Price completed");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in UPCOM_UpdateStockTop3Price, detail:" + e.Message);
                    Console.ResetColor();
                }
            }
        }
    }
}
