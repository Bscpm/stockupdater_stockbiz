﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace StockTracker.Proxy
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost host = new ServiceHost(typeof(DBProxyService));
            host.Open();
            Console.Read();
            host.Close();
        }
    }
}
