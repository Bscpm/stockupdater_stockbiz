﻿using StockTracker.Lib;
using StockTracker.Lib.DBProxies;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Globalization;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;

namespace StockTracker.Lib.Handlers
{
    public class UPCOMIF5DatabaseHandler : CommonHandler
    {
        public const int UPCOM_STOCK_TYPE = 2;

        private static string _if5Connection;

        static UPCOMIF5DatabaseHandler()
        {
            UPCOMIF5DatabaseHandler._if5Connection = ConfigurationManager.AppSettings["Infoshow5Database"];
        }

        public UPCOMIF5DatabaseHandler()
        {
        }

        public static void ProcessMarketInfo(out string _status)
        {
            OracleConnection oracleConnection = new OracleConnection(UPCOMIF5DatabaseHandler._if5Connection);
            oracleConnection.Open();
            OracleCommand oracleCommand = new OracleCommand()
            {
                Connection = oracleConnection,
                CommandText = "SELECT * FROM COL_UPCOM_MARKET_INFO",
                CommandType = CommandType.Text
            };
            OracleDataReader oracleDataReader = oracleCommand.ExecuteReader();
            MarketInfo marketInfo = null;
            _status = null;
            if (oracleDataReader.Read())
            {
                marketInfo = new MarketInfo()
                {
                    Symbol = "UPCOM",
                    TotalTrade = Convert.ToDouble(oracleDataReader["TOTAL_TRADE"].ToString())
                };
                DateTime item = (DateTime)oracleDataReader["TRADING_DATE"];
                marketInfo.Date = item.Add(TimeSpan.Parse(oracleDataReader["TIME"].ToString()));
                marketInfo.TotalValue = Convert.ToDouble(oracleDataReader["TOTAL_VALUE"].ToString());
                marketInfo.TotalVolume = Convert.ToDouble(oracleDataReader["TOTAL_QTTY"].ToString());
                marketInfo.BasicIndex = Convert.ToDouble(oracleDataReader["PRIOR_MARKET_INDEX"].ToString());
                marketInfo.CurrentIndex = Convert.ToDouble(oracleDataReader["MARKET_INDEX"].ToString());
                marketInfo.Advances = Convert.ToInt32(oracleDataReader["ADVANCES"].ToString());
                marketInfo.NoChange = Convert.ToInt32(oracleDataReader["NOCHANGE"].ToString());
                marketInfo.Declines = Convert.ToInt32(oracleDataReader["DECLINES"].ToString());
                marketInfo.Status = oracleDataReader["CURRENT_STATUS"].ToString();
                marketInfo.TotalVolumePT = Convert.ToDouble(oracleDataReader["PT_TOTAL_QTTY"].ToString());
                marketInfo.TotalValuePT = Convert.ToDouble(oracleDataReader["PT_TOTAL_VALUE"].ToString());
            }
            oracleConnection.Dispose();
            if (marketInfo != null)
            {
                _status = marketInfo.Status;
                foreach (DBProxyServiceClient databaseClient in CommonHandler.GetDatabaseClients())
                {
                    string name = databaseClient.Endpoint.Name;
                    try
                    {
                        databaseClient.UPCOM_UpdateMarketInfo(marketInfo.Date, marketInfo.Status, marketInfo.BasicIndex, marketInfo.CurrentIndex, marketInfo.TotalTrade, marketInfo.TotalVolume, marketInfo.TotalVolumePT, marketInfo.TotalValue, marketInfo.Advances, marketInfo.NoChange, marketInfo.Declines);
                        object[] str = new object[] { marketInfo.Date.ToString("dd/MM/yyyy HH:mm:ss"), marketInfo.Status, marketInfo.CurrentIndex, marketInfo.TotalTrade, marketInfo.TotalVolume, name };
                        Console.WriteLine(string.Format("{5}.UPCOM_UpdateMarketInfo Date:{0}, Status:{1}, Current Index:{2}, TotalTrade:{3}, TotalVolume:{4} successfully", str));
                    }
                    catch (Exception exception1)
                    {
                        Exception exception = exception1;
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(string.Concat("Error in ", name, ".UPCOMIF5DatabaseHandler.ProcessMarketInfo, detail:", exception.Message));
                        Console.WriteLine("Re-trying...");
                        Console.ResetColor();
                        databaseClient.UPCOM_UpdateMarketInfo(marketInfo.Date, marketInfo.Status, marketInfo.BasicIndex, marketInfo.CurrentIndex, marketInfo.TotalTrade, marketInfo.TotalVolume, marketInfo.TotalVolumePT, marketInfo.TotalValue, marketInfo.Advances, marketInfo.NoChange, marketInfo.Declines);
                    }
                }
            }
        }

        public static void ProcessStockInfo()
        {
            OracleConnection oracleConnection = new OracleConnection(UPCOMIF5DatabaseHandler._if5Connection);
            oracleConnection.Open();
            OracleCommand oracleCommand = new OracleCommand()
            {
                Connection = oracleConnection,
                CommandText = "SELECT * FROM COL_UPCOM_STOCKS_INFO",
                CommandType = CommandType.Text
            };
            OracleDataReader oracleDataReader = oracleCommand.ExecuteReader();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("<ROOT>");
            int num = 0;
            while (oracleDataReader.Read())
            {
                if (Convert.ToInt32(oracleDataReader["STOCK_TYPE"].ToString()) != 2)
                {
                    continue;
                }
                StockInfo stockInfo = new StockInfo()
                {
                    StockId = string.Concat("UC", oracleDataReader["STOCK_ID"].ToString()),
                    Symbol = oracleDataReader["CODE"].ToString()
                };
                DateTime item = (DateTime)oracleDataReader["TRADING_DATE"];
                stockInfo.Date = item.Add(TimeSpan.Parse(oracleDataReader["TIME"].ToString()));
                stockInfo.PriceBasic = (oracleDataReader["BASIC_PRICE"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["BASIC_PRICE"]));
                stockInfo.PriceCeiling = (oracleDataReader["CEILING_PRICE"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["CEILING_PRICE"]));
                stockInfo.PriceFloor = (oracleDataReader["FLOOR_PRICE"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["FLOOR_PRICE"]));
                stockInfo.PriceCurrent = (oracleDataReader["MATCH_PRICE"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["MATCH_PRICE"]));
                stockInfo.PriceHigh = (oracleDataReader["HIGHEST_PRICE"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["HIGHEST_PRICE"]));
                stockInfo.PriceLow = (oracleDataReader["LOWEST_PRICE"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["LOWEST_PRICE"]));
                stockInfo.PriceOpen = (oracleDataReader["OPEN_PRICE"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["OPEN_PRICE"]));
                stockInfo.PriceClose = (oracleDataReader["CLOSE_PRICE"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["CLOSE_PRICE"]));
                stockInfo.PriceAverage = (oracleDataReader["AVERAGE_PRICE"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["AVERAGE_PRICE"]));
                stockInfo.Volume = (oracleDataReader["MATCH_QTTY"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["MATCH_QTTY"]));
                stockInfo.TotalVolume = (oracleDataReader["TOTAL_TRADING_QTTY"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["TOTAL_TRADING_QTTY"]));
                stockInfo.TotalValue = (oracleDataReader["TOTAL_TRADING_VALUE"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["TOTAL_TRADING_VALUE"]));
                stockInfo.PT_TotalTradedQtty = (oracleDataReader["PT_TOTAL_TRADED_QTTY"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["PT_TOTAL_TRADED_QTTY"]));
                stockInfo.PT_TotalTradedValue = (oracleDataReader["PT_TOTAL_TRADED_VALUE"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["PT_TOTAL_TRADED_VALUE"]));
                stockInfo.CurrentForeignRoom = (oracleDataReader["REMAIN_FOREIGN_QTTY"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["REMAIN_FOREIGN_QTTY"]));
                stockInfo.BuyCount = (oracleDataReader["BID_COUNT"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["BID_COUNT"]));
                stockInfo.SellCount = (oracleDataReader["OFFER_COUNT"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["OFFER_COUNT"]));
                stockInfo.BuyQuantity = (oracleDataReader["TOTAL_BID_QTTY"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["TOTAL_BID_QTTY"]));
                stockInfo.SellQuantity = (oracleDataReader["TOTAL_OFFER_QTTY"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["TOTAL_OFFER_QTTY"]));
                stockInfo.BuyForeignQuantity = (oracleDataReader["BUY_FOREIGN_QTTY"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["BUY_FOREIGN_QTTY"]));
                stockInfo.SellForeignQuantity = (oracleDataReader["SELL_FOREIGN_QTTY"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["SELL_FOREIGN_QTTY"]));
                stockInfo.BuyForeignValue = (oracleDataReader["BUY_FOREIGN_VALUE"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["BUY_FOREIGN_VALUE"]));
                stockInfo.SellForeignValue = (oracleDataReader["SELL_FOREIGN_VALUE"] == DBNull.Value ? 0 : Convert.ToDouble(oracleDataReader["SELL_FOREIGN_VALUE"]));
                object[] stockId = new object[26];
                stockId[0] = stockInfo.StockId;
                stockId[1] = stockInfo.Symbol;
                stockId[2] = stockInfo.Date.ToString("MM/dd/yyyy HH:mm:ss");
                double priceBasic = stockInfo.PriceBasic;
                stockId[3] = priceBasic.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double priceCeiling = stockInfo.PriceCeiling;
                stockId[4] = priceCeiling.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double priceFloor = stockInfo.PriceFloor;
                stockId[5] = priceFloor.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double priceCurrent = stockInfo.PriceCurrent;
                stockId[6] = priceCurrent.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double volume = stockInfo.Volume;
                stockId[7] = volume.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double priceHigh = stockInfo.PriceHigh;
                stockId[8] = priceHigh.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double priceLow = stockInfo.PriceLow;
                stockId[9] = priceLow.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double priceOpen = stockInfo.PriceOpen;
                stockId[10] = priceOpen.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double priceClose = stockInfo.PriceClose;
                stockId[11] = priceClose.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double priceAverage = stockInfo.PriceAverage;
                stockId[12] = priceAverage.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double totalVolume = stockInfo.TotalVolume;
                stockId[13] = totalVolume.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double totalValue = stockInfo.TotalValue;
                stockId[14] = totalValue.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double currentForeignRoom = stockInfo.CurrentForeignRoom;
                stockId[15] = currentForeignRoom.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double buyCount = stockInfo.BuyCount;
                stockId[16] = buyCount.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double sellCount = stockInfo.SellCount;
                stockId[17] = sellCount.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double buyForeignQuantity = stockInfo.BuyForeignQuantity;
                stockId[18] = buyForeignQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double sellForeignQuantity = stockInfo.SellForeignQuantity;
                stockId[19] = sellForeignQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double buyForeignValue = stockInfo.BuyForeignValue;
                stockId[20] = buyForeignValue.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double sellForeignValue = stockInfo.SellForeignValue;
                stockId[21] = sellForeignValue.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double buyQuantity = stockInfo.BuyQuantity;
                stockId[22] = buyQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double sellQuantity = stockInfo.SellQuantity;
                stockId[23] = sellQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double pTTotalTradedQtty = stockInfo.PT_TotalTradedQtty;
                stockId[24] = pTTotalTradedQtty.ToString(CultureInfo.InvariantCulture.NumberFormat);
                double pTTotalTradedValue = stockInfo.PT_TotalTradedValue;
                stockId[25] = pTTotalTradedValue.ToString(CultureInfo.InvariantCulture.NumberFormat);
                stringBuilder.AppendFormat("<Stock StockID=\"{0}\" Symbol=\"{1}\" Date=\"{2}\" PriceBasic=\"{3}\" PriceCeiling=\"{4}\" PriceFloor=\"{5}\" PriceCurrent=\"{6}\" Volume=\"{7}\" PriceHigh=\"{8}\" PriceLow=\"{9}\" PriceOpen=\"{10}\" PriceClose=\"{11}\" PriceAverage=\"{12}\" TotalVolume=\"{13}\" TotalValue=\"{14}\" CurrentForeignRoom=\"{15}\" BuyCount=\"{16}\" SellCount=\"{17}\" BuyForeignQuantity=\"{18}\" SellForeignQuantity=\"{19}\" BuyForeignValue=\"{20}\" SellForeignValue=\"{21}\" BuyQuantity=\"{22}\" SellQuantity=\"{23}\" TotalPTVolume=\"{24}\" TotalPTValue=\"{25}\" ></Stock>", stockId);
                num++;
            }
            oracleConnection.Dispose();
            stringBuilder.Append("</ROOT>");
            foreach (DBProxyServiceClient databaseClient in CommonHandler.GetDatabaseClients())
            {
                string name = databaseClient.Endpoint.Name;
                try
                {
                    databaseClient.UPCOM_UpdateStockPriceInfo(stringBuilder.ToString());
                    Console.WriteLine(string.Concat(name, ".UPCOM_UpdateStockPriceInfo successfully, stocks count:", num));
                }
                catch (Exception exception1)
                {
                    Exception exception = exception1;
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(string.Concat("Error in ", name, ".UPCOMIF5DatabaseHandler.ProcessStockInfo, detail:", exception.Message));
                    Console.WriteLine("Re-trying...");
                    Console.ResetColor();
                    databaseClient.UPCOM_UpdateStockPriceInfo(stringBuilder.ToString());
                }
            }
        }

        public static void ProcessTopPriceInfo()
        {
            OracleConnection oracleConnection = new OracleConnection(UPCOMIF5DatabaseHandler._if5Connection);
            oracleConnection.Open();
            OracleCommand oracleCommand = new OracleCommand()
            {
                Connection = oracleConnection,
                CommandText = "SELECT * FROM COL_UPCOM_BESTPRICE WHERE NORP=3",
                CommandType = CommandType.Text
            };
            OracleDataReader oracleDataReader = oracleCommand.ExecuteReader();
            List<DBProxyServiceClient> databaseClients = CommonHandler.GetDatabaseClients();
            if (oracleDataReader.HasRows)
            {
                int num = 0;
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("<ROOT>");
                while (oracleDataReader.Read())
                {
                    object[] objArray = new object[] { string.Concat("UC", oracleDataReader["STOCK_ID"].ToString()), Convert.ToDouble(oracleDataReader["B_ORD_PRICE_1"].ToString()), Convert.ToDouble(oracleDataReader["B_ORD_QTTY_1"].ToString()), Convert.ToDouble(oracleDataReader["B_ORD_PRICE_2"].ToString()), Convert.ToDouble(oracleDataReader["B_ORD_QTTY_2"].ToString()), Convert.ToDouble(oracleDataReader["B_ORD_PRICE_3"].ToString()), Convert.ToDouble(oracleDataReader["B_ORD_QTTY_3"].ToString()), Convert.ToDouble(oracleDataReader["S_ORD_PRICE_1"].ToString()), Convert.ToDouble(oracleDataReader["S_ORD_QTTY_1"].ToString()), Convert.ToDouble(oracleDataReader["S_ORD_PRICE_2"].ToString()), Convert.ToDouble(oracleDataReader["S_ORD_QTTY_2"].ToString()), Convert.ToDouble(oracleDataReader["S_ORD_PRICE_3"].ToString()), Convert.ToDouble(oracleDataReader["S_ORD_QTTY_3"].ToString()) };
                    stringBuilder.AppendFormat("<Stock StockID=\"{0}\" PriceBid1=\"{1}\" QuantityBid1=\"{2}\" PriceBid2=\"{3}\" QuantityBid2=\"{4}\" PriceBid3=\"{5}\" QuantityBid3=\"{6}\" PriceOffer1=\"{7}\" QuantityOffer1=\"{8}\" PriceOffer2=\"{9}\" QuantityOffer2=\"{10}\" PriceOffer3=\"{11}\" QuantityOffer3=\"{12}\" ></Stock>", objArray);
                    num++;
                }
                stringBuilder.Append("</ROOT>");
                foreach (DBProxyServiceClient databaseClient in databaseClients)
                {
                    try
                    {
                        databaseClient.UPCOM_UpdateStockTop3Price(stringBuilder.ToString());
                        Console.WriteLine(string.Concat("UPCOMIF5DatabaseHandler.ProcessTopPriceInfo successfully, Stocks Count:", num));
                    }
                    catch (Exception exception1)
                    {
                        Exception exception = exception1;
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(string.Concat("Error in ", databaseClient.Endpoint.Name, "UPCOMIF5DatabaseHandler.ProcessTopPriceInfo, detail:", exception.Message));
                        Console.WriteLine("Re-trying...");
                        Console.ResetColor();
                        databaseClient.UPCOM_UpdateStockTop3Price(stringBuilder.ToString());
                    }
                }
            }
            else
            {
                foreach (DBProxyServiceClient dBProxyServiceClient in databaseClients)
                {
                    dBProxyServiceClient.UPCOM_ResetStockTop3Price();
                    Console.WriteLine(string.Concat(dBProxyServiceClient.Endpoint.Name, ".UPCOMIF5DatabaseHandler.UPCOM_ResetStockTop3Price successfully"));
                }
            }
            oracleConnection.Dispose();
        }

        public static void UpdateUPCOMSecuritiesList()
        {
        }
    }
}