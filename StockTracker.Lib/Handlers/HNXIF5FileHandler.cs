using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Runtime.InteropServices;
using System.IO;
using System.Globalization;
using StockTracker.Lib.DBProxies;
using System.Data.OracleClient;
using System.Configuration;
using System.Linq;

namespace StockTracker.Lib.Handlers
{
    public class HNXIF5FileHandler
    {
        public const string HASTC_FLOOR_CODE = "02";
        public const int HASTC_STOCK_TYPE = 2;
        private static DBProxyServiceClient _client = null;
        private static string _if5FilePath = ConfigurationManager.AppSettings["Infoshow5FilePath"];
 
        private static DBProxyServiceClient GetDatabaseClient()
        {
            if (_client == null || _client.State != System.ServiceModel.CommunicationState.Opened)
            {
                _client = new DBProxyServiceClient();
            }

            return _client;
        }

        private static string ParseNumber(string number)
        {
            if (number == "NULL")
            {
                return "0";
            }

            return number;
        }
        private static DataTable BuildStocksTable()
        {
            DataTable dt = new DataTable("STS_STOCKS_INFO");
            dt.Columns.Add("STOCK_INFO_ID", typeof(string));
            dt.Columns.Add("TRADING_DATE", typeof(string));
            dt.Columns.Add("TIME", typeof(string));
            dt.Columns.Add("STOCK_ID", typeof(string));
            dt.Columns.Add("CODE", typeof(string));
            dt.Columns.Add("STOCK_TYPE", typeof(string));
            dt.Columns.Add("TOTAL_LISTING_QTTY", typeof(string));
            dt.Columns.Add("TRADING_UNIT", typeof(string));
            dt.Columns.Add("LISTTING_STATUS", typeof(string));
            dt.Columns.Add("ADJUST_QTTY", typeof(string));
            dt.Columns.Add("REFERENCE_STATUS", typeof(string));
            dt.Columns.Add("ADJUST_RATE", typeof(string));
            dt.Columns.Add("DIVIDENT_RATE", typeof(string));
            dt.Columns.Add("STATUS", typeof(string));
            dt.Columns.Add("TOTAL_ROOM", typeof(string));
            dt.Columns.Add("CURRENT_ROOM", typeof(string));
            dt.Columns.Add("BASIC_PRICE", typeof(string));
            dt.Columns.Add("OPEN_PRICE", typeof(string));
            dt.Columns.Add("CLOSE_PRICE", typeof(string));
            dt.Columns.Add("CURRENT_PRICE", typeof(string));
            dt.Columns.Add("CURRENT_QTTY", typeof(string));
            dt.Columns.Add("HIGHEST_PRICE", typeof(string));
            dt.Columns.Add("LOWEST_PRICE", typeof(string));
            dt.Columns.Add("BEST_OFFER_PRICE", typeof(string));
            dt.Columns.Add("BEST_BID_PRICE", typeof(string));
            dt.Columns.Add("CEILING_PRICE", typeof(string));
            dt.Columns.Add("FLOOR_PRICE", typeof(string));
            dt.Columns.Add("TOTAL_OFFER_QTTY", typeof(string));
            dt.Columns.Add("TOTAL_BID_QTTY", typeof(string));
            dt.Columns.Add("BEST_OFFER_QTTY", typeof(string));
            dt.Columns.Add("BEST_BID_QTTY", typeof(string));
            dt.Columns.Add("PRIOR_PRICE", typeof(string));
            dt.Columns.Add("PRIOR_CLOSE_PRICE", typeof(string));
            dt.Columns.Add("MATCH_PRICE", typeof(string));
            dt.Columns.Add("MATCH_QTTY", typeof(string));
            dt.Columns.Add("DELETED", typeof(string));
            dt.Columns.Add("DATE_CREATED", typeof(string));
            dt.Columns.Add("DATE_MODIFIED", typeof(string));
            dt.Columns.Add("MODIFIED_BY", typeof(string));
            dt.Columns.Add("CREATED_BY", typeof(string));
            dt.Columns.Add("NAME", typeof(string));
            dt.Columns.Add("PARVALUE", typeof(string));
            dt.Columns.Add("MATCH_VALUE", typeof(string));
            dt.Columns.Add("FLOOR_CODE", typeof(string));
            dt.Columns.Add("IS_CALCINDEX", typeof(string));
            dt.Columns.Add("IS_DETERMINECL", typeof(string));
            dt.Columns.Add("DATE_NO", typeof(string));
            dt.Columns.Add("OFFER_COUNT", typeof(string));
            dt.Columns.Add("BID_COUNT", typeof(string));
            dt.Columns.Add("AVERAGE_PRICE", typeof(string));
            dt.Columns.Add("INDEX_PRICE", typeof(string));
            dt.Columns.Add("PREV_PRIOR_PRICE", typeof(string));
            dt.Columns.Add("YIELDMAT", typeof(string));
            dt.Columns.Add("PREV_PRIOR_CLOSE_PRICE", typeof(string));
            dt.Columns.Add("NM_TOTAL_TRADED_QTTY", typeof(string));
            dt.Columns.Add("NM_TOTAL_TRADED_VALUE", typeof(string));
            dt.Columns.Add("PT_MATCH_QTTY", typeof(string));
            dt.Columns.Add("PT_MATCH_PRICE", typeof(string));
            dt.Columns.Add("PT_TOTAL_TRADED_QTTY", typeof(string));
            dt.Columns.Add("PT_TOTAL_TRADED_VALUE", typeof(string));
            dt.Columns.Add("TOTAL_BUY_TRADING_QTTY", typeof(string));
            dt.Columns.Add("BUY_COUNT", typeof(string));
            dt.Columns.Add("TOTAL_BUY_TRADING_VALUE", typeof(string));
            dt.Columns.Add("TOTAL_SELL_TRADING_QTTY", typeof(string));
            dt.Columns.Add("SELL_COUNT", typeof(string));
            dt.Columns.Add("TOTAL_SELL_TRADING_VALUE", typeof(string));
            dt.Columns.Add("TOTAL_TRADING_QTTY", typeof(string));
            dt.Columns.Add("TOTAL_TRADING_VALUE", typeof(string));
            dt.Columns.Add("BUY_FOREIGN_QTTY", typeof(string));
            dt.Columns.Add("BUY_FOREIGN_VALUE", typeof(string));
            dt.Columns.Add("SELL_FOREIGN_QTTY", typeof(string));
            dt.Columns.Add("SELL_FOREIGN_VALUE", typeof(string));
            dt.Columns.Add("REMAIN_FOREIGN_QTTY", typeof(string));
            dt.Columns.Add("PT_YIELDMAT", typeof(string));
            dt.Columns.Add("DML_TYPE", typeof(string));
            dt.Columns.Add("NEW_DATA", typeof(string));

            return dt;
        }

        private static void BuildStocksRow(DataRow row, string[] fields)
        {
            row["STOCK_INFO_ID"] = fields[0];
            row["TRADING_DATE"] = fields[1].Replace("'", "");
            row["TIME"] = fields[2].Replace("'", "");
            row["STOCK_ID"] = fields[3];
            row["CODE"] = fields[4].Replace("'", "");
            row["STOCK_TYPE"] = fields[5];
            row["TOTAL_LISTING_QTTY"] = ParseNumber(fields[6]);
            row["TRADING_UNIT"] = ParseNumber(fields[7]);
            row["LISTTING_STATUS"] = fields[8];
            row["ADJUST_QTTY"] = ParseNumber(fields[9]);
            row["REFERENCE_STATUS"] = fields[10];
            row["ADJUST_RATE"] = ParseNumber(fields[11]);
            row["DIVIDENT_RATE"] = ParseNumber(fields[12]);
            row["STATUS"] = fields[13];
            row["TOTAL_ROOM"] = ParseNumber(fields[14]);
            row["CURRENT_ROOM"] = ParseNumber(fields[15]);
            row["BASIC_PRICE"] = ParseNumber(fields[16]);
            row["OPEN_PRICE"] = ParseNumber(fields[17]);
            row["CLOSE_PRICE"] = ParseNumber(fields[18]);
            row["CURRENT_PRICE"] = ParseNumber(fields[19]);
            row["CURRENT_QTTY"] = ParseNumber(fields[20]);
            row["HIGHEST_PRICE"] = ParseNumber(fields[21]);
            row["LOWEST_PRICE"] = ParseNumber(fields[22]);
            row["BEST_OFFER_PRICE"] = ParseNumber(fields[23]);
            row["BEST_BID_PRICE"] = ParseNumber(fields[24]);
            row["CEILING_PRICE"] = ParseNumber(fields[25]);
            row["FLOOR_PRICE"] = ParseNumber(fields[26]);
            row["TOTAL_OFFER_QTTY"] = ParseNumber(fields[27]);
            row["TOTAL_BID_QTTY"] = ParseNumber(fields[28]);
            row["BEST_OFFER_QTTY"] = ParseNumber(fields[29]);
            row["BEST_BID_QTTY"] = ParseNumber(fields[30]);
            row["PRIOR_PRICE"] = ParseNumber(fields[31]);
            row["PRIOR_CLOSE_PRICE"] = ParseNumber(fields[32]);
            row["MATCH_PRICE"] = ParseNumber(fields[33]);
            row["MATCH_QTTY"] = ParseNumber(fields[34]);
            row["DELETED"] = fields[35];
            row["DATE_CREATED"] = fields[36];
            row["DATE_MODIFIED"] = fields[37];
            row["MODIFIED_BY"] = fields[38];
            row["CREATED_BY"] = fields[39];
            row["NAME"] = fields[40];
            row["PARVALUE"] = ParseNumber(fields[41]);
            row["MATCH_VALUE"] = ParseNumber(fields[42]);
            row["FLOOR_CODE"] = fields[43].Replace("'", "");
            row["IS_CALCINDEX"] = fields[44];
            row["IS_DETERMINECL"] = fields[45];
            row["DATE_NO"] = fields[46];
            row["OFFER_COUNT"] = ParseNumber(fields[47]);
            row["BID_COUNT"] = ParseNumber(fields[48]);
            row["AVERAGE_PRICE"] = ParseNumber(fields[49]);
            row["INDEX_PRICE"] = ParseNumber(fields[50]);
            row["PREV_PRIOR_PRICE"] = ParseNumber(fields[51]);
            row["YIELDMAT"] = ParseNumber(fields[52]);
            row["PREV_PRIOR_CLOSE_PRICE"] = ParseNumber(fields[53]);
            row["NM_TOTAL_TRADED_QTTY"] = ParseNumber(fields[54]);
            row["NM_TOTAL_TRADED_VALUE"] = ParseNumber(fields[55]);
            row["PT_MATCH_QTTY"] = ParseNumber(fields[56]);
            row["PT_MATCH_PRICE"] = ParseNumber(fields[57]);
            row["PT_TOTAL_TRADED_QTTY"] = ParseNumber(fields[58]);
            row["PT_TOTAL_TRADED_VALUE"] = ParseNumber(fields[59]);
            row["TOTAL_BUY_TRADING_QTTY"] = ParseNumber(fields[60]);
            row["BUY_COUNT"] = ParseNumber(fields[61]);
            row["TOTAL_BUY_TRADING_VALUE"] = ParseNumber(fields[62]);
            row["TOTAL_SELL_TRADING_QTTY"] = ParseNumber(fields[63]);
            row["SELL_COUNT"] = ParseNumber(fields[64]);
            row["TOTAL_SELL_TRADING_VALUE"] = ParseNumber(fields[65]);
            row["TOTAL_TRADING_QTTY"] = ParseNumber(fields[66]);
            row["TOTAL_TRADING_VALUE"] = ParseNumber(fields[67]);
            row["BUY_FOREIGN_QTTY"] = ParseNumber(fields[68]);
            row["BUY_FOREIGN_VALUE"] = ParseNumber(fields[69]);
            row["SELL_FOREIGN_QTTY"] = ParseNumber(fields[70]);
            row["SELL_FOREIGN_VALUE"] = ParseNumber(fields[71]);
            row["REMAIN_FOREIGN_QTTY"] = ParseNumber(fields[72]);
            row["PT_YIELDMAT"] = ParseNumber(fields[73]);
            row["DML_TYPE"] = fields[74];
            row["NEW_DATA"] = fields[75];
        }

        private static DataTable BuildMarketTable()
        {
            DataTable dt = new DataTable("STS_MARKET_INFO");
            dt.Columns.Add("MARKET_INFO_ID", typeof(string));
            dt.Columns.Add("TRADING_DATE", typeof(string));

            dt.Columns.Add("CURRENT_STATUS", typeof(string));
            dt.Columns.Add("TIME", typeof(string));
            dt.Columns.Add("TOTAL_TRADE", typeof(string));
            dt.Columns.Add("TOTAL_STOCK", typeof(string));
            dt.Columns.Add("TOTAL_QTTY", typeof(string));
            dt.Columns.Add("TOTAL_VALUE", typeof(string));
            dt.Columns.Add("UP_VOLUME", typeof(string));
            dt.Columns.Add("NOCHANGE_VOLUME", typeof(string));
            dt.Columns.Add("DOWN_VOLUME", typeof(string));
            dt.Columns.Add("ADVANCES", typeof(string));
            dt.Columns.Add("NOCHANGE", typeof(string));
            dt.Columns.Add("DECLINES", typeof(string));
            dt.Columns.Add("PRIOR_MARKET_INDEX", typeof(string));
            dt.Columns.Add("CHG_INDEX", typeof(string));
            dt.Columns.Add("PCT_INDEX", typeof(string));
            dt.Columns.Add("CURRENT_INDEX", typeof(string));
            dt.Columns.Add("MARKET_INDEX", typeof(string));
            dt.Columns.Add("BASE_INDEX", typeof(string));
            dt.Columns.Add("HIGHEST_INDEX", typeof(string));
            dt.Columns.Add("LOWEST_INDEX", typeof(string));
            dt.Columns.Add("FLOOR_CODE", typeof(string));
            dt.Columns.Add("DATE_NO", typeof(string));
            dt.Columns.Add("SESSION_NO", typeof(string));
            dt.Columns.Add("PRV_PRIOR_MARKET_INDEX", typeof(string));
            dt.Columns.Add("AVR_MARKET_INDEX", typeof(string));
            dt.Columns.Add("AVR_PRIOR_MARKET_INDEX", typeof(string));
            dt.Columns.Add("AVR_CHG_INDEX", typeof(string));
            dt.Columns.Add("AVR_PCT_INDEX", typeof(string));
            dt.Columns.Add("PT_TOTAL_QTTY", typeof(string));
            dt.Columns.Add("PT_TOTAL_VALUE", typeof(string));
            dt.Columns.Add("PT_TOTAL_TRADE", typeof(string));
            dt.Columns.Add("SYSTIME", typeof(string));
            dt.Columns.Add("DML_TYPE", typeof(string));
            dt.Columns.Add("NEW_DATA", typeof(string));

            return dt;
        }

        private static void BuildMarketRow(DataRow row, string[] fields)
        {
            row["MARKET_INFO_ID"] = fields[0];
            row["TRADING_DATE"] = fields[1].Replace("'", "");
            row["CURRENT_STATUS"] = fields[2];
            row["TIME"] = fields[3].Replace("'", "");
            row["TOTAL_TRADE"] = ParseNumber(fields[4]);
            row["TOTAL_STOCK"] = ParseNumber(fields[5]);
            row["TOTAL_QTTY"] = ParseNumber(fields[6]);
            row["TOTAL_VALUE"] = ParseNumber(fields[7]);
            row["UP_VOLUME"] = ParseNumber(fields[8]);
            row["NOCHANGE_VOLUME"] = ParseNumber(fields[9]);
            row["DOWN_VOLUME"] = ParseNumber(fields[10]);
            row["ADVANCES"] = ParseNumber(fields[11]);
            row["NOCHANGE"] = ParseNumber(fields[12]);
            row["DECLINES"] = ParseNumber(fields[13]);
            row["PRIOR_MARKET_INDEX"] = ParseNumber(fields[14]);
            row["CHG_INDEX"] = ParseNumber(fields[15]);
            row["PCT_INDEX"] = ParseNumber(fields[16]);
            row["CURRENT_INDEX"] = ParseNumber(fields[17]);
            row["MARKET_INDEX"] = ParseNumber(fields[18]);
            row["BASE_INDEX"] = ParseNumber(fields[19]);
            row["HIGHEST_INDEX"] = ParseNumber(fields[20]);
            row["LOWEST_INDEX"] = ParseNumber(fields[21]);
            row["FLOOR_CODE"] = fields[22].Replace("'", "");
            row["DATE_NO"] = fields[23];
            row["SESSION_NO"] = fields[24];
            row["PRV_PRIOR_MARKET_INDEX"] = ParseNumber(fields[25]);
            row["AVR_MARKET_INDEX"] = ParseNumber(fields[26]);
            row["AVR_PRIOR_MARKET_INDEX"] = ParseNumber(fields[27]);
            row["AVR_CHG_INDEX"] = ParseNumber(fields[28]);
            row["AVR_PCT_INDEX"] = ParseNumber(fields[29]);
            row["PT_TOTAL_QTTY"] = ParseNumber(fields[30]);
            row["PT_TOTAL_VALUE"] = ParseNumber(fields[31]);
            row["PT_TOTAL_TRADE"] = ParseNumber(fields[32]);
            row["SYSTIME"] = fields[33].Replace("'", "");
            row["DML_TYPE"] = fields[34].Replace("'", "");
            row["NEW_DATA"] = fields[35];
        }

        private static DataTable BuildTopPriceTable()
        {
            DataTable dt = new DataTable("STS_TOP3_PRICE_A");
            dt.Columns.Add("STOCK_ID", typeof(string));
            dt.Columns.Add("TRADING_DATE", typeof(string));
            dt.Columns.Add("B_ORD_QTTY_3", typeof(string));
            dt.Columns.Add("B_ORD_PRICE_3", typeof(string));
            dt.Columns.Add("B_ORD_QTTY_2", typeof(string));
            dt.Columns.Add("B_ORD_PRICE_2", typeof(string));
            dt.Columns.Add("B_ORD_QTTY_1", typeof(string));
            dt.Columns.Add("B_ORD_PRICE_1", typeof(string));
            dt.Columns.Add("S_ORD_PRICE_1", typeof(string));
            dt.Columns.Add("S_ORD_QTTY_1", typeof(string));
            dt.Columns.Add("S_ORD_PRICE_2", typeof(string));
            dt.Columns.Add("S_ORD_QTTY_2", typeof(string));
            dt.Columns.Add("S_ORD_PRICE_3", typeof(string));
            dt.Columns.Add("S_ORD_QTTY_3", typeof(string));
            dt.Columns.Add("NORP", typeof(string));
            dt.Columns.Add("DML_TYPE", typeof(string));
            dt.Columns.Add("NEW_DATA", typeof(string));

            return dt;
        }

        private static void BuildTopPriceRow(DataRow row, string[] fields)
        {
            row["STOCK_ID"] = fields[0];
            row["TRADING_DATE"] = fields[1].Replace("'", "");
            row["B_ORD_QTTY_3"] = fields[2];
            row["B_ORD_PRICE_3"] = fields[3];
            row["B_ORD_QTTY_2"] = fields[4];
            row["B_ORD_PRICE_2"] = fields[5];
            row["B_ORD_QTTY_1"] = fields[6];
            row["B_ORD_PRICE_1"] = fields[7];
            row["S_ORD_PRICE_1"] = fields[8];
            row["S_ORD_QTTY_1"] = fields[9];
            row["S_ORD_PRICE_2"] = fields[10];
            row["S_ORD_QTTY_2"] = fields[11];
            row["S_ORD_PRICE_3"] = fields[12];
            row["S_ORD_QTTY_3"] = fields[13];
            row["NORP"] = fields[14];
            row["DML_TYPE"] = fields[15].Replace("'", "");
            row["NEW_DATA"] = fields[16];
        }

        private const int BUFFER_SIZE = 4096;
        private static MemoryStream ReadAllBytes(string filePath)
        {
            MemoryStream ms = new MemoryStream();
            using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                byte[] buffer = new byte[BUFFER_SIZE];

                int read = 0;
                while ((read = fs.Read(buffer, 0, BUFFER_SIZE)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                ms.Seek(0, SeekOrigin.Begin);
            }

            return ms;
        }

        private static int _lastLine = -1;

        // Temporary table used to create structured rows
        private static DataTable _dtMarketInfo = BuildMarketTable();
        private static DataTable _dtStocksInfo = BuildStocksTable();
        private static DataTable _dtTopPrice = BuildTopPriceTable();

        // Dictionary to store data rows
        private static Dictionary<string, DataRow> _dicMarketRows = new Dictionary<string, DataRow>();
        private static Dictionary<string, DataRow> _dicStocksRows = new Dictionary<string, DataRow>();
        private static Dictionary<string, DataRow> _dicTopPriceRows = new Dictionary<string, DataRow>();

        public static void ProcessData()
        {
            MemoryStream ms = ReadAllBytes(_if5FilePath);

            int lineIndex = -1;
            using (StreamReader sr = new StreamReader(ms))
            {
                string line = null;
                while ((line = sr.ReadLine()) != null)
                {
                    lineIndex++;
                    if (line.Trim() != string.Empty && lineIndex > _lastLine)
                    {
                        string[] fields = line.Split(new char[] { '|' });
                        string memberID = fields[0];
                        string coMemberID = fields[1];
                        string idMain = fields[2];
                        string idPublic = fields[3];
                        string idPrivate = fields[4];
                        string dmlType = fields[5];
                        string norp = fields[6];
                        string norc = fields[7];
                        string orderStatus = fields[8];
                        string codeFilter = fields[9];
                        string tableName = fields[10];
                        string[] values = fields[11].Split(new char[] { ',' });

                        DataRow row = null;

                        if (tableName == "STS_MARKET_INFO")
                        {
                            row = _dtMarketInfo.NewRow();

                            BuildMarketRow(row, values);

                            if (_dicMarketRows.ContainsKey(row["MARKET_INFO_ID"].ToString()))
                            {
                                _dicMarketRows[row["MARKET_INFO_ID"].ToString()] = row;
                            }
                            else
                            {
                                _dicMarketRows.Add(row["MARKET_INFO_ID"].ToString(), row);
                            }
                        }
                        else if (tableName == "STS_STOCKS_INFO")
                        {
                            row = _dtStocksInfo.NewRow();

                            BuildStocksRow(row, values);

                            if (_dicStocksRows.ContainsKey(row["STOCK_ID"].ToString()))
                            {
                                _dicStocksRows[row["STOCK_ID"].ToString()] = row;
                            }
                            else
                            {
                                _dicStocksRows.Add(row["STOCK_ID"].ToString(), row);
                            }
                        }
                        else if (tableName == "STS_TOP3_PRICE_A")
                        {
                            row = _dtTopPrice.NewRow();

                            BuildTopPriceRow(row, values);

                            if (_dicTopPriceRows.ContainsKey(row["STOCK_ID"].ToString()))
                            {
                                _dicTopPriceRows[row["STOCK_ID"].ToString()] = row;
                            }
                            else
                            {
                                _dicTopPriceRows.Add(row["STOCK_ID"].ToString(), row);
                            }
                        }
                    }
                }
            }

            if (lineIndex > _lastLine)
            {
                _lastLine = lineIndex;
            }
            else if (lineIndex < _lastLine) // New day, total line < last line
            {
                _lastLine = -1;
            }
        }

        public static void ProcessMarketInfo(out string _status)
        {
            DataRow dr = _dicMarketRows.Values.Where(r => r.Field<string>("FLOOR_CODE") == HASTC_FLOOR_CODE).SingleOrDefault();
            MarketInfo market = null;
            _status = null;
            double putthroughVolume = 0;
            double putthroughValue = 0;
            if (dr != null)
            {
                market = new MarketInfo();
                market.Symbol = "HASTC";
                market.TotalTrade = Convert.ToDouble(dr["TOTAL_TRADE"].ToString());

                market.Date = (DateTime.ParseExact(dr["TRADING_DATE"].ToString(), "dd-MMM-yyyy", CultureInfo.CreateSpecificCulture("en-US"))).Add(TimeSpan.Parse(dr["TIME"].ToString()));

                market.TotalValue = Convert.ToDouble(dr["TOTAL_VALUE"].ToString());
                market.TotalVolume = Convert.ToDouble(dr["TOTAL_QTTY"].ToString());
                market.BasicIndex = Convert.ToDouble(dr["PRIOR_MARKET_INDEX"].ToString());
                market.CurrentIndex = Convert.ToDouble(dr["MARKET_INDEX"].ToString());
                market.HighestIndex = Convert.ToDouble(dr["HIGHEST_INDEX"].ToString());
                market.LowestIndex = Convert.ToDouble(dr["LOWEST_INDEX"].ToString());
                market.Advances = Convert.ToInt32(dr["ADVANCES"].ToString());
                market.NoChange = Convert.ToInt32(dr["NOCHANGE"].ToString());
                market.Declines = Convert.ToInt32(dr["DECLINES"].ToString());
                market.Status = dr["CURRENT_STATUS"].ToString();
                putthroughVolume = Convert.ToDouble(dr["PT_TOTAL_QTTY"].ToString());
                putthroughValue = Convert.ToDouble(dr["PT_TOTAL_VALUE"].ToString());
            }

            if (market != null)
            {
                _status = market.Status;

                DBProxyServiceClient client = GetDatabaseClient();
                client.HAGW_UpdateMarketInfo(market.Date, market.Status, market.BasicIndex, market.CurrentIndex, market.HighestIndex, market.LowestIndex, market.TotalTrade, market.TotalVolume, putthroughVolume, market.TotalValue, putthroughValue, market.Advances, market.NoChange, market.Declines);
            }
        }

        public static void ProcessStockInfo()
        {
            List<DataRow> listRows = _dicStocksRows.Values.Where(r => r.Field<string>("FLOOR_CODE") == HASTC_FLOOR_CODE).ToList();

            StringBuilder xml = new StringBuilder();
            xml.Append("<ROOT>");
            StringBuilder xmlPT = new StringBuilder();
            xmlPT.Append("<ROOT>");

            foreach (DataRow dr in listRows)
            {
                if (Convert.ToInt32(dr["STOCK_TYPE"].ToString()) == HASTC_STOCK_TYPE && dr["FLOOR_CODE"].ToString() == HASTC_FLOOR_CODE)
                {
                    StockInfo stock = new StockInfo();
                    stock.StockId = "HA" + dr["STOCK_ID"].ToString();
                    stock.Symbol = dr["CODE"].ToString();
                    stock.Date = (DateTime.ParseExact(dr["TRADING_DATE"].ToString(), "dd-MMM-yyyy", CultureInfo.CreateSpecificCulture("en-US"))).Add(TimeSpan.Parse(dr["TIME"].ToString()));

                    stock.PriceBasic = (dr["BASIC_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["BASIC_PRICE"]);

                    stock.PriceCeiling = (dr["CEILING_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["CEILING_PRICE"]);

                    stock.PriceFloor = (dr["FLOOR_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["FLOOR_PRICE"]);

                    stock.PriceCurrent = (dr["MATCH_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["MATCH_PRICE"]);

                    stock.PriceHigh = (dr["HIGHEST_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["HIGHEST_PRICE"]);

                    stock.PriceLow = (dr["LOWEST_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["LOWEST_PRICE"]);

                    stock.PriceOpen = (dr["OPEN_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["OPEN_PRICE"]);

                    stock.PriceClose = (dr["CLOSE_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["CLOSE_PRICE"]);

                    stock.PriceAverage = (dr["AVERAGE_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["AVERAGE_PRICE"]);

                    stock.Volume = (dr["MATCH_QTTY"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["MATCH_QTTY"]);

                    stock.TotalVolume = (dr["NM_TOTAL_TRADED_QTTY"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["NM_TOTAL_TRADED_QTTY"]);

                    stock.TotalValue = (dr["NM_TOTAL_TRADED_VALUE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["NM_TOTAL_TRADED_VALUE"]);

                    stock.CurrentForeignRoom = (dr["REMAIN_FOREIGN_QTTY"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["REMAIN_FOREIGN_QTTY"]);                    

                    stock.BuyCount = (dr["BID_COUNT"] == DBNull.Value) ?
                                      0 : Convert.ToDouble(dr["BID_COUNT"]);
                    stock.SellCount = (dr["OFFER_COUNT"] == DBNull.Value) ?
                                      0 : Convert.ToDouble(dr["OFFER_COUNT"]);
                    stock.BuyQuantity = (dr["TOTAL_BID_QTTY"] == DBNull.Value) ?
                                      0 : Convert.ToDouble(dr["TOTAL_BID_QTTY"]);
                    stock.SellQuantity = (dr["TOTAL_OFFER_QTTY"] == DBNull.Value) ?
                                      0 : Convert.ToDouble(dr["TOTAL_OFFER_QTTY"]);
                    stock.BuyForeignQuantity = (dr["BUY_FOREIGN_QTTY"] == DBNull.Value) ?
                                                0 : Convert.ToDouble(dr["BUY_FOREIGN_QTTY"]);
                    stock.SellForeignQuantity = (dr["SELL_FOREIGN_QTTY"] == DBNull.Value) ?
                                                0 : Convert.ToDouble(dr["SELL_FOREIGN_QTTY"]);
                    stock.BuyForeignValue = (dr["BUY_FOREIGN_VALUE"] == DBNull.Value) ?
                                                0 : Convert.ToDouble(dr["BUY_FOREIGN_VALUE"]);
                    stock.SellForeignValue = (dr["SELL_FOREIGN_VALUE"] == DBNull.Value) ?
                                                0 : Convert.ToDouble(dr["SELL_FOREIGN_VALUE"]);

                    xml.AppendFormat("<Stock StockID=\"{0}\" Symbol=\"{1}\" Date=\"{2}\" PriceBasic=\"{3}\" PriceCeiling=\"{4}\" PriceFloor=\"{5}\" PriceCurrent=\"{6}\" Volume=\"{7}\" " +
                                        "PriceHigh=\"{8}\" PriceLow=\"{9}\" PriceOpen=\"{10}\" PriceClose=\"{11}\" PriceAverage=\"{12}\" TotalVolume=\"{13}\" TotalValue=\"{14}\" " +
                                        "CurrentForeignRoom=\"{15}\" BuyCount=\"{16}\" SellCount=\"{17}\" " +
                                        "BuyForeignQuantity=\"{18}\" SellForeignQuantity=\"{19}\" BuyForeignValue=\"{20}\" SellForeignValue=\"{21}\" BuyQuantity=\"{22}\" SellQuantity=\"{23}\" " +
                                        "></Stock>", stock.StockId, stock.Symbol, stock.Date.ToString("MM/dd/yyyy HH:mm:ss"),
                                        stock.PriceBasic.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceCeiling.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceFloor.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceCurrent.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.Volume.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceHigh.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceLow.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceOpen.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceClose.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceAverage.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.TotalVolume.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.TotalValue.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.CurrentForeignRoom.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.BuyCount.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.SellCount.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.BuyForeignQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.SellForeignQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.BuyForeignValue.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.SellForeignValue.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.BuyQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.SellQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat)
                                        );
                }

                if (dr["FLOOR_CODE"].ToString() == HASTC_FLOOR_CODE && dr["PT_MATCH_QTTY"].ToString() != "0")
                {
                    xmlPT.AppendFormat("<Stock StockID=\"{0}\" PT_Volume=\"{1}\" PT_Price=\"{2}\" PT_TotalVolume=\"{3}\" PT_TotalValue=\"{4}\"></Stock>",
                   "HA" + dr["STOCK_ID"].ToString(),
                   Convert.ToDouble(dr["PT_MATCH_QTTY"].ToString()),
                   Convert.ToDouble(dr["PT_MATCH_PRICE"].ToString()),
                   Convert.ToDouble(dr["PT_TOTAL_TRADED_QTTY"].ToString()),
                   Convert.ToDouble(dr["PT_TOTAL_TRADED_VALUE"].ToString())
                   );
                }
            }

            xml.Append("</ROOT>");
            xmlPT.Append("</ROOT>");

            DBProxyServiceClient client = GetDatabaseClient();

            client.HAGW_UpdateStockPriceInfo(xml.ToString());
            client.HAGW_UpdatePutThrough(xmlPT.ToString());
        }

        private static void ProcessDeList()
        {
            List<DataRow> listRows = _dicStocksRows.Values.Where(r => r.Field<string>("FLOOR_CODE") == HASTC_FLOOR_CODE).ToList();

            StringBuilder sb = new StringBuilder();
            sb.Append(",");

            foreach (DataRow dr in listRows)
            {
                if (Convert.ToInt32(dr["STOCK_TYPE"].ToString()) == HASTC_STOCK_TYPE && dr["FLOOR_CODE"].ToString() == HASTC_FLOOR_CODE)
                {
                    sb.Append(dr["CODE"].ToString()).Append(",");
                    
                }
            }

            DBProxyServiceClient client = GetDatabaseClient();
            client.HAGW_ProcessDeList(sb.ToString());
        }

        public static void UpdateHASTCSecuritiesList()
        {
            //OracleConnection conn = new OracleConnection(_if5Connection);  // C#
            //conn.Open();
            //OracleCommand cmd = new OracleCommand();
            //cmd.Connection = conn;
            //cmd.CommandText = "SELECT STOCK_ID, CODE, FLOOR_CODE, STOCK_TYPE FROM STS_STOCKS_INFO WHERE FLOOR_CODE='02'";
            //cmd.CommandType = CommandType.Text;
            //OracleDataReader dr = cmd.ExecuteReader();

            //DBProxyServiceClient client = GetDatabaseClient();

            //while (dr.Read())
            //{
            //    if (dr["FLOOR_CODE"].ToString() == HASTC_FLOOR_CODE)
            //    {
            //        int t = Convert.ToInt32(dr["STOCK_TYPE"].ToString());
            //        int type;

            //        switch (t)
            //        {
            //            case 1:
            //                type = 2;
            //                break;

            //            case 2:
            //                type = 1;
            //                break;

            //            case 3:
            //                type = 3;
            //                break;

            //            default:
            //                type = t;
            //                break;
            //        }
            //        try
            //        {
            //            client.UpdateNewSymbol("HA" + dr["STOCK_ID"].ToString(), dr["CODE"].ToString(), "HASTC", type);
            //        }
            //        catch (Exception ex)
            //        {
            //            Console.WriteLine("Error while UpdateHASTCSecuritiesList, message=" + ex.Message);
            //        }
            //    }
            //}            

            //conn.Dispose();

            ProcessDeList();
        }

        public static void ProcessTopPriceInfo()
        {
            List<DataRow> listRows = _dicTopPriceRows.Values.Where(r => r.Field<string>("NORP") == "1").ToList();

            DBProxyServiceClient client = GetDatabaseClient();

            if (listRows == null || listRows.Count == 0)
            {
                client.HAGW_ResetStockTop3Price();
            }
            else
            {
                StringBuilder xml = new StringBuilder();
                xml.Append("<ROOT>");
                foreach (DataRow dr in listRows)
                {
                    xml.AppendFormat("<Stock StockID=\"{0}\" PriceBid1=\"{1}\" QuantityBid1=\"{2}\" PriceBid2=\"{3}\" QuantityBid2=\"{4}\" PriceBid3=\"{5}\" QuantityBid3=\"{6}\" " +
                                        "PriceOffer1=\"{7}\" QuantityOffer1=\"{8}\" PriceOffer2=\"{9}\" QuantityOffer2=\"{10}\" PriceOffer3=\"{11}\" QuantityOffer3=\"{12}\" " +
                                        "></Stock>", "HA" + dr["STOCK_ID"].ToString(),
                                        Convert.ToDouble(dr["B_ORD_PRICE_1"].ToString()),
                                        Convert.ToDouble(dr["B_ORD_QTTY_1"].ToString()),
                                        Convert.ToDouble(dr["B_ORD_PRICE_2"].ToString()),
                                        Convert.ToDouble(dr["B_ORD_QTTY_2"].ToString()),
                                        Convert.ToDouble(dr["B_ORD_PRICE_3"].ToString()),
                                        Convert.ToDouble(dr["B_ORD_QTTY_3"].ToString()),
                                        Convert.ToDouble(dr["S_ORD_PRICE_1"].ToString()),
                                        Convert.ToDouble(dr["S_ORD_QTTY_1"].ToString()),
                                        Convert.ToDouble(dr["S_ORD_PRICE_2"].ToString()),
                                        Convert.ToDouble(dr["S_ORD_QTTY_2"].ToString()),
                                        Convert.ToDouble(dr["S_ORD_PRICE_3"].ToString()),
                                        Convert.ToDouble(dr["S_ORD_QTTY_3"].ToString()));


                }
                xml.Append("</ROOT>");
                client.HAGW_UpdateStockTop3Price(xml.ToString());
            }
        }

        public static void ProcessHAPutAdInfo()
        {
            List<DataRow> listRows = _dicTopPriceRows.Values.Where(r => r.Field<string>("NORP") == "2" && (r.Field<string>("B_ORD_QTTY_1") != "0" || r.Field<string>("S_ORD_QTTY_1") != "0")).ToList();

            DBProxyServiceClient client = GetDatabaseClient();

            if (listRows == null || listRows.Count == 0)
            {
                client.HAGW_ResetPutAdInfo();
            }
            else
            {
                StringBuilder xml = new StringBuilder();
                xml.Append("<ROOT>");
                foreach (DataRow dr in listRows)
                {
                    xml.AppendFormat("<Stock StockID=\"{0}\" BuyPrice=\"{1}\" BuyQuantity=\"{2}\" SellPrice=\"{3}\" SellQuantity=\"{4}\" ></Stock>", "HA" + dr["STOCK_ID"].ToString(),
                                       Convert.ToDouble(dr["B_ORD_PRICE_1"].ToString()),
                                       Convert.ToDouble(dr["B_ORD_QTTY_1"].ToString()),
                                       Convert.ToDouble(dr["S_ORD_PRICE_1"].ToString()),
                                       Convert.ToDouble(dr["S_ORD_QTTY_1"].ToString()));
                }
                xml.Append("</ROOT>");
                client.HAGW_InsertPutAd(xml.ToString());
            }
        }
    }
}
