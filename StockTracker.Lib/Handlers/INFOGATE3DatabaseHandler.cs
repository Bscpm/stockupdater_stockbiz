﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StockTracker.Lib.DBProxies;
using System.Configuration;
using System.Data.OracleClient;
using System.Data;
using System.Globalization;

namespace StockTracker.Lib.Handlers
{
    public class INFOGATE3DatabaseHandler
    {
        public const string HASTC_FLOOR_CODE = "02";
        public const int HASTC_STOCK_TYPE = 2;
        private static DBProxyServiceClient _client = null;
        private static string _infogate3Connection = ConfigurationManager.AppSettings["InfoGate3Database"];

        private static DBProxyServiceClient GetDatabaseClient()
        {
            if (_client == null || _client.State != System.ServiceModel.CommunicationState.Opened)
            {
                _client = new DBProxyServiceClient();
            }

            return _client;
        }

        public static void ProcessIndexInfo()
        {
            OracleConnection conn = new OracleConnection(_infogate3Connection);  // C#
            conn.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = "SELECT * FROM INDEX_INFO";
            cmd.CommandType = CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();
            IndexInfo indexInfo = null;

            //double putthroughVolume = 0;
            //double putthroughValue = 0;
            string indexCode = "";

            StringBuilder xml = new StringBuilder();
            xml.Append("<ROOT>");

            while (dr.Read())
            {
                indexInfo = new IndexInfo();
                indexCode = dr["Code"].ToString();
                if (indexCode.Equals("HNXIndex", StringComparison.InvariantCultureIgnoreCase))
                {
                    indexInfo.IndexCode = "HASTC";
                }
                else if (indexCode.Equals("HNXUpcomIndex", StringComparison.InvariantCultureIgnoreCase))
                {
                    indexInfo.IndexCode = "UPCOM";
                }
                else if (indexCode.Equals("HNX30", StringComparison.InvariantCultureIgnoreCase))
                {
                    indexInfo.IndexCode = "HNX30";
                }
                indexInfo.CurrentValue = Convert.ToDouble(dr["Value"].ToString());
                indexInfo.Date = ((DateTime)dr["TradingDate"]).Add(TimeSpan.Parse(dr["CallTime"].ToString()));

                // Change
                // RatioChange
                // CurrentStatus

                // Tong KL giao dich khop lenh (Ko tinh giao dich thoa thuan)
                indexInfo.TotalQtty = Convert.ToDouble(dr["TotalQtty"].ToString());
                // Tong GT giao dich khop lenh (Ko tinh giao dich thoa thuan)
                indexInfo.TotalValue = Convert.ToDouble(dr["TotalValue"].ToString());

                // TotalStock: Tong so CK trong ro

                // InfoGate ko thay tra ve truong TotalTrade
                //market.TotalTrade = Convert.ToDouble(dr["TOTAL_TRADE"].ToString());

                indexInfo.PriorIndexVal = Convert.ToDouble(dr["PriorIndexVal"].ToString());

                indexInfo.HighestIndex = Convert.ToDouble(dr["HighestIndex"].ToString());
                indexInfo.LowestIndex = Convert.ToDouble(dr["LowestIndex"].ToString());
                // CloseIndex
                // TypeIndex
                // Index Name

                // InfoGate ko thay tra ve truong Advances
                //market.Advances = Convert.ToInt32(dr["ADVANCES"].ToString());
                //market.NoChange = Convert.ToInt32(dr["NOCHANGE"].ToString());
                //market.Declines = Convert.ToInt32(dr["DECLINES"].ToString());
                //market.Status = dr["CURRENT_STATUS"].ToString();
                //putthroughVolume = Convert.ToDouble(dr["PT_TOTAL_QTTY"].ToString());
                //putthroughValue = Convert.ToDouble(dr["PT_TOTAL_VALUE"].ToString());


                xml.AppendFormat("<Index IndexCode=\"{0}\" CurrentValue=\"{1}\" Date=\"{2}\" TotalQtty=\"{3}\" TotalValue=\"{4}\" " +
                    "PriorIndexVal=\"{5}\" HighestIndex=\"{6}\" LowestIndex=\"{7}\" ></Index>",
                    indexInfo.IndexCode, indexInfo.CurrentValue, indexInfo .Date.ToString("MM/dd/yyyy HH:mm:ss"), indexInfo.TotalQtty, indexInfo.TotalValue,
                    indexInfo.PriorIndexVal, indexInfo.HighestIndex, indexInfo.LowestIndex
                   );
            }

            conn.Dispose();
            xml.Append("</ROOT>");

            DBProxyServiceClient client = GetDatabaseClient();

            try
            {
                client.IG3_UpdateIndexInfo(xml.ToString());
            }
            catch
            {
                client.IG3_UpdateIndexInfo(xml.ToString());
            }
        }

        public static void ProcessBasketInfo()
        {
            OracleConnection conn = new OracleConnection(_infogate3Connection);  // C#
            conn.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = "SELECT * FROM BASKET_INFO";
            cmd.CommandType = CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();
            BasketInfo basket = null;
            string indexCode = "";

            StringBuilder xml = new StringBuilder();
            xml.Append("<ROOT>");

            while (dr.Read())
            {
                basket = new BasketInfo();
                indexCode = dr["Name"].ToString();
                if (indexCode.Equals("HNXIndex", StringComparison.InvariantCultureIgnoreCase))
                {
                    basket.IndexCode = "HASTC";
                }
                else if (indexCode.Equals("HNXUpcomIndex", StringComparison.InvariantCultureIgnoreCase))
                {
                    basket.IndexCode = "UPCOM";
                }
                else if (indexCode.Equals("HNX30", StringComparison.InvariantCultureIgnoreCase))
                {
                    basket.IndexCode = "HNX30";
                }
                basket.Symbol = dr["Symbol"].ToString();
                basket.TotalQtty = Convert.ToInt64(dr["TotalQtty"].ToString());
                basket.AddDate = DateTime.ParseExact(dr["AddDate"].ToString(), "yyyyMMdd", null);
                basket.Weighted = Convert.ToDouble(dr["Weighted"].ToString());

                xml.AppendFormat("<Basket IndexCode=\"{0}\" Symbol=\"{1}\" TotalQtty=\"{3}\" AddDate=\"{4}\" " +
                    "Weighted=\"{5}\" ></Basket>",
                    basket.IndexCode, basket.Symbol, basket.TotalQtty, basket.AddDate.ToString("MM/dd/yyyy HH:mm:ss"),
                    basket.Weighted);
            }

            conn.Dispose();
            xml.Append("</ROOT>");

            DBProxyServiceClient client = GetDatabaseClient();

            try
            {
                client.IG3_UpdateBasketInfo(xml.ToString());
            }
            catch
            {
                client.IG3_UpdateBasketInfo(xml.ToString());
            }
        }

        public static void ProcessStockInfo(out string _status)
        {
            OracleConnection conn = new OracleConnection(_infogate3Connection);  // C#
            conn.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = "SELECT * FROM STOCKS_INFO WHERE TRADINGDATE='" + DateTime.Today.ToString("yyyyMMdd") + "'";
            cmd.CommandType = CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();

            StringBuilder xmlStockInfo = new StringBuilder();
            xmlStockInfo.Append("<ROOT>");
            StringBuilder xmlSymbolInfo = new StringBuilder();
            xmlSymbolInfo.Append("<ROOT>");
            StringBuilder xmlPT = new StringBuilder();
            xmlPT.Append("<ROOT>");

            _status = null;
            string boardCode = "";
            while (dr.Read())
            {
                boardCode = dr["TradingSessionCode"].ToString();

                StockInfo stock = new StockInfo();
                SymbolInfo symbolInfo = new SymbolInfo();

                if (boardCode.Equals("LIST", StringComparison.InvariantCultureIgnoreCase))
                {
                    stock.StockId = "HA" + dr["SymbolID"].ToString();
                    stock.Exchange = "HASTC";
                }
                else
                {
                    stock.StockId = "UC" + dr["SymbolID"].ToString();
                    stock.Exchange = "UPCOM";
                }
                symbolInfo.SourceID = stock.StockId;
                symbolInfo.BoardCode = stock.Exchange;

                stock.Symbol = dr["Symbol"].ToString();
                symbolInfo.Symbol = stock.Symbol;

                // Ngay GD hien tai
                stock.Date = DateTime.ParseExact(dr["TradingDate"].ToString(), "yyyyMMdd", null).Add(TimeSpan.Parse(dr["Time"].ToString()));

                stock.TradingSessionID = dr["TradingSessionId"] != DBNull.Value ? dr["TradingSessionId"].ToString() : "";
                stock.TradSesStatus = dr["TradingSeStatus"] != DBNull.Value ? dr["TradingSeStatus"].ToString() : "";
                _status = stock.TradSesStatus;
                symbolInfo.SecurityTradingStatus = dr["SecurityTradingStatus"] != DBNull.Value ? dr["SecurityTradingStatus"].ToString() : "";

                symbolInfo.SecurityType = dr["SecurityType"].ToString();
                symbolInfo.IssueDate = DateTime.ParseExact(dr["IssueDate"].ToString(), "yyyyMMdd", null);
                symbolInfo.Issuer = dr["Issuer"] != DBNull.Value ? dr["Issuer"].ToString() : "";
                symbolInfo.SecurityDesc = dr["SecurityDesc"] != DBNull.Value ? dr["SecurityDesc"].ToString() : "";

                // Gia mua tot nhat (chi tinh khop lenh lo chan)
                stock.BestBidPrice = (dr["BestBidPrice"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["BestBidPrice"]);
                // KL mua tot nhat (chi tinh khop lenh lo chan)
                stock.BestBidQtty = (dr["BestBidQtty"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["BestBidQtty"]);
                // Gia ban tot nhat (chi tinh khop lenh lo chan)
                stock.BestOfferPrice = (dr["BestOfferPrice"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["BestOfferPrice"]);
                // KL ban tot nhat (chi tinh khop lenh lo chan)
                stock.BestOfferQtty = (dr["BestOfferQtty"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["BestOfferQtty"]);
                // Tong KL chao mua cong don
                stock.BuyQuantity = (dr["TotalBidQtty"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["TotalBidQtty"]);
                // Tong KL chao ban cong don
                stock.SellQuantity = (dr["TotalOfferQtty"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["TotalOfferQtty"]);
                // Gia tham chieu
                stock.PriceBasic = (dr["BasicPrice"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["BasicPrice"]);
                // Gia tran
                stock.PriceCeiling = (dr["CeilingPrice"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["CeilingPrice"]);
                // Gia san
                stock.PriceFloor = (dr["FloorPrice"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["FloorPrice"]);
                // Menh gia CK
                symbolInfo.ParValue = (dr["ParValue"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["ParValue"]);
                // Gia khop gan nhat
                stock.PriceCurrent = (dr["MatchPrice"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["MatchPrice"]);
                // KL khop gan nhat
                stock.Volume = (dr["MatchQtty"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["MatchQtty"]);
                // Don vi giao dich nho nhat
                symbolInfo.TradingUnit = (dr["TradingUnit"] == DBNull.Value) ?
                                    1 : Convert.ToInt32(dr["TradingUnit"]);
                // KL niem yet
                symbolInfo.TotalListingQtty = (dr["TotalListingQtty"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["TotalListingQtty"]);
                // Phien giao dich thu (Ke tu ngay niem yet)
                stock.DateNo = (dr["DateNo"] == DBNull.Value) ?
                                    0 : Convert.ToInt32(dr["DateNo"]);
                // KL dieu chinh
                stock.AdjustQtty = (dr["AdjustQtty"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["AdjustQtty"]);
                // Trang thai thuc hien quyen
                stock.ReferenceStatus = (dr["ReferenceStatus"] == DBNull.Value) ?
                                    0 : Convert.ToInt32(dr["ReferenceStatus"]);
                // Ty le dieu chinh
                stock.AdjustRate = (dr["AdjustRate"] == DBNull.Value) ?
                                    1 : Convert.ToDouble(dr["AdjustRate"]);
                // Ty le co tuc/co phieu
                stock.DividendRate = (dr["DividentRate"] == DBNull.Value) ?
                                    (double?)null : Convert.ToDouble(dr["DividentRate"]);

                // Gia thuc hien cao nhat (gia khop lenh cua ca 2 hinh thuc)
                stock.PriceHigh = (dr["HighestPice"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["HighestPice"]);
                // Gia thuc hien thap nhat (gia khop lenh cua ca 2 hinh thuc)
                stock.PriceLow = (dr["LowestPrice"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["LowestPrice"]);
                // Gia khop lenh cua phien truoc do
                stock.PriorPrice = (dr["PriorPrice"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["PriorPrice"]);
                // Gia mo cua
                stock.PriceOpen = (dr["OpenPrice"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["OpenPrice"]);
                // Gia mo cua phien GD truoc
                stock.PriorOpenPrice = (dr["PriorOpenPrice"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["PriorOpenPrice"]);
                // Gia dong cua
                stock.PriceClose = (dr["ClosePrice"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["ClosePrice"]);
                // Gia dong cua phien GD truoc
                stock.PriorClosePrice = (dr["PriorClosePrice"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["PriorClosePrice"]);
                // Gia binh quan
                stock.PriceAverage = (dr["MidPx"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["MidPx"]);
                // TotalVolumeTraded: Pending
                // TotalValueTraded: Pending
                stock.MatchValue = (dr["MatchValue"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["MatchValue"]);
                // Tong so lenh dat ban cho khop
                stock.SellCount = (dr["OfferCount"] == DBNull.Value) ?
                                    0 : Convert.ToInt32(dr["OfferCount"]);
                // Tong so lenh dat mua cho khop
                stock.BuyCount = (dr["BidCount"] == DBNull.Value) ?
                                    0 : Convert.ToInt32(dr["BidCount"]);
                // Tong KL giao dich thong thuong
                stock.TotalVolume = (dr["NM_TotalTradedQtty"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["NM_TotalTradedQtty"]);
                // Tong gia tri GD thong thuong
                stock.TotalValue = (dr["NM_TotalTradedValue"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["NM_TotalTradedValue"]);
                // PT_MatchQtty
                // PT_MatchPrice
                // PT_TotalTradedQtty
                // PT_TotalTradedValue

                stock.BoughtQuantity = (dr["TotalBuyTradingQtty"] == DBNull.Value) ?
                                  0 : Convert.ToDouble(dr["TotalBuyTradingQtty"]);
                // Tong so lenh da mua
                stock.BoughtCount = (dr["BuyCount"] == DBNull.Value) ?
                                    0 : Convert.ToInt32(dr["BuyCount"]);
                stock.BoughtValue = (dr["TotalBuyTradingValue"] == DBNull.Value) ?
                                  0 : Convert.ToDouble(dr["TotalBuyTradingValue"]);
                stock.SoldQuantity = (dr["TotalSellTradingQtty"] == DBNull.Value) ?
                                  0 : Convert.ToDouble(dr["TotalSellTradingQtty"]);
                // Tong so lenh da mua
                stock.SoldCount = (dr["SellCount"] == DBNull.Value) ?
                                    0 : Convert.ToInt32(dr["SellCount"]);
                stock.SoldValue = (dr["TotalSellTradingValue"] == DBNull.Value) ?
                                  0 : Convert.ToDouble(dr["TotalSellTradingValue"]);
                stock.BuyForeignQuantity = (dr["BuyForeignQtty"] == DBNull.Value) ?
                                            0 : Convert.ToDouble(dr["BuyForeignQtty"]);
                stock.BuyForeignValue = (dr["BuyForeignValue"] == DBNull.Value) ?
                                            0 : Convert.ToDouble(dr["BuyForeignValue"]);
                stock.SellForeignQuantity = (dr["SellForeignQtty"] == DBNull.Value) ?
                                            0 : Convert.ToDouble(dr["SellForeignQtty"]);

                stock.SellForeignValue = (dr["SellForeignValue"] == DBNull.Value) ?
                                            0 : Convert.ToDouble(dr["SellForeignValue"]);

                // So luong con lai cho phep NDT NN dat lenh mua
                stock.CurrentForeignRoom = (dr["RemainForeignQtty"] == DBNull.Value) ?
                                    0 : Convert.ToDouble(dr["RemainForeignQtty"]);
                stock.MaturityDate = dr["MaturityDate"] != DBNull.Value ? DateTime.ParseExact(dr["MaturityDate"].ToString(), "yyyyMMdd", null) : (DateTime?)null;
                stock.CouponRate = (dr["CouponRate"] == DBNull.Value) ?
                                            0 : Convert.ToDouble(dr["CouponRate"]);

                xmlSymbolInfo.AppendFormat("<Stock StockID=\"{0}\" Symbol=\"{1}\" Date=\"{2}\" PriceBasic=\"{3}\" PriceCeiling=\"{4}\" PriceFloor=\"{5}\" PriceCurrent=\"{6}\" Volume=\"{7}\" " +
                                    "PriceHigh=\"{8}\" PriceLow=\"{9}\" PriceOpen=\"{10}\" PriceClose=\"{11}\" PriceAverage=\"{12}\" TotalVolume=\"{13}\" TotalValue=\"{14}\" " +
                                    "CurrentForeignRoom=\"{15}\" BuyCount=\"{16}\" SellCount=\"{17}\" " +
                                    "BuyForeignQuantity=\"{18}\" SellForeignQuantity=\"{19}\" BuyForeignValue=\"{20}\" SellForeignValue=\"{21}\" BuyQuantity=\"{22}\" SellQuantity=\"{23}\" " +
                                    "></Stock>", symbolInfo.SourceID, symbolInfo.Symbol, symbolInfo.BoardCode,
                                    symbolInfo.SecurityTradingStatus,
                                    symbolInfo.SecurityType,
                                    symbolInfo.IssueDate.ToString("MM/dd/yyyy HH:mm:ss"),
                                    symbolInfo.Issuer,
                                    symbolInfo.SecurityDesc,
                                    symbolInfo.ParValue.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    symbolInfo.TradingUnit.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    symbolInfo.TotalListingQtty.ToString(CultureInfo.InvariantCulture.NumberFormat)
                                    );

                xmlStockInfo.AppendFormat("<Stock StockID=\"{0}\" Symbol=\"{1}\" Date=\"{2}\" PriceBasic=\"{3}\" PriceCeiling=\"{4}\" PriceFloor=\"{5}\" PriceCurrent=\"{6}\" Volume=\"{7}\" " +
                                    "PriceHigh=\"{8}\" PriceLow=\"{9}\" PriceOpen=\"{10}\" PriceClose=\"{11}\" PriceAverage=\"{12}\" TotalVolume=\"{13}\" TotalValue=\"{14}\" " +
                                    "CurrentForeignRoom=\"{15}\" BuyCount=\"{16}\" SellCount=\"{17}\" " +
                                    "BuyForeignQuantity=\"{18}\" SellForeignQuantity=\"{19}\" BuyForeignValue=\"{20}\" SellForeignValue=\"{21}\" BuyQuantity=\"{22}\" SellQuantity=\"{23}\" " +
                                    "></Stock>", stock.StockId, stock.Symbol, stock.Date.ToString("MM/dd/yyyy HH:mm:ss"),
                                    stock.PriceBasic.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.PriceCeiling.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.PriceFloor.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.PriceCurrent.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.Volume.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.PriceHigh.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.PriceLow.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.PriceOpen.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.PriceClose.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.PriceAverage.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.TotalVolume.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.TotalValue.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.CurrentForeignRoom.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.BuyCount.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.SellCount.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.BuyForeignQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.SellForeignQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.BuyForeignValue.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.SellForeignValue.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.BuyQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                    stock.SellQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat)
                                    );


                if (dr["PT_MatchQtty"].ToString() != "0")
                {
                    if (stock.Exchange == "HASTC")
                    {
                        xmlPT.AppendFormat("<Stock StockID=\"{0}\" PT_Volume=\"{1}\" PT_Price=\"{2}\" PT_TotalVolume=\"{3}\" PT_TotalValue=\"{4}\"></Stock>",
                           "HA" + dr["STOCK_ID"].ToString(),
                           Convert.ToDouble(dr["PT_MatchQtty"].ToString()),
                           Convert.ToDouble(dr["PT_MatchPrice"].ToString()),
                           Convert.ToDouble(dr["PT_TotalTradedQtty"].ToString()),
                           Convert.ToDouble(dr["PT_TotalTradedValue"].ToString())
                           );
                    }
                    else
                    {
                        xmlPT.AppendFormat("<Stock StockID=\"{0}\" PT_Volume=\"{1}\" PT_Price=\"{2}\" PT_TotalVolume=\"{3}\" PT_TotalValue=\"{4}\"></Stock>",
                           "UC" + dr["STOCK_ID"].ToString(),
                           Convert.ToDouble(dr["PT_MatchQtty"].ToString()),
                           Convert.ToDouble(dr["PT_MatchPrice"].ToString()),
                           Convert.ToDouble(dr["PT_TotalTradedQtty"].ToString()),
                           Convert.ToDouble(dr["PT_TotalTradedValue"].ToString())
                           );
                    }
                }
            }

            conn.Dispose();

            xmlStockInfo.Append("</ROOT>");
            xmlSymbolInfo.Append("</ROOT>");
            xmlPT.Append("</ROOT>");

            DBProxyServiceClient client = GetDatabaseClient();

            try
            {
                client.IG3_UpdateStockSymbol(xmlSymbolInfo.ToString());
            }
            catch
            {
                client.IG3_UpdateStockSymbol(xmlSymbolInfo.ToString());
            }

            try
            {
                client.IG3_UpdateStockPriceInfo(xmlStockInfo.ToString());
            }
            catch
            {
                client.IG3_UpdateStockPriceInfo(xmlStockInfo.ToString());
            }

            try
            {
                client.IG3_UpdatePutThrough(xmlPT.ToString());
            }
            catch
            {
                client.IG3_UpdatePutThrough(xmlPT.ToString());
            }
        }

        //private static void ProcessDeList()
        //{
        //    OracleConnection conn = new OracleConnection(_infogate3Connection);  // C#
        //    conn.Open();
        //    OracleCommand cmd = new OracleCommand();
        //    cmd.Connection = conn;
        //    cmd.CommandText = "SELECT * FROM STS_STOCKS_INFO WHERE FLOOR_CODE='02'";
        //    cmd.CommandType = CommandType.Text;
        //    OracleDataReader dr = cmd.ExecuteReader();

        //    StringBuilder sb = new StringBuilder();
        //    sb.Append(",");

        //    while (dr.Read())
        //    {
        //        if (Convert.ToInt32(dr["STOCK_TYPE"].ToString()) == HASTC_STOCK_TYPE && dr["FLOOR_CODE"].ToString() == HASTC_FLOOR_CODE)
        //        {
        //            sb.Append(dr["CODE"].ToString()).Append(",");

        //        }
        //    }

        //    conn.Dispose();

        //    DBProxyServiceClient client = GetDatabaseClient();

        //    try
        //    {
        //        client.HAGW_ProcessDeList(sb.ToString());
        //    }
        //    catch
        //    {
        //        client.HAGW_ProcessDeList(sb.ToString());
        //    }
        //}

        //public static void UpdateHASTCSecuritiesList()
        //{
        //    //OracleConnection conn = new OracleConnection(_infogate3Connection);  // C#
        //    //conn.Open();
        //    //OracleCommand cmd = new OracleCommand();
        //    //cmd.Connection = conn;
        //    //cmd.CommandText = "SELECT STOCK_ID, CODE, FLOOR_CODE, STOCK_TYPE FROM STS_STOCKS_INFO WHERE FLOOR_CODE='02'";
        //    //cmd.CommandType = CommandType.Text;
        //    //OracleDataReader dr = cmd.ExecuteReader();

        //    //DBProxyServiceClient client = GetDatabaseClient();

        //    //while (dr.Read())
        //    //{
        //    //    if (dr["FLOOR_CODE"].ToString() == HASTC_FLOOR_CODE)
        //    //    {
        //    //        int t = Convert.ToInt32(dr["STOCK_TYPE"].ToString());
        //    //        int type;

        //    //        switch (t)
        //    //        {
        //    //            case 1:
        //    //                type = 2;
        //    //                break;

        //    //            case 2:
        //    //                type = 1;
        //    //                break;

        //    //            case 3:
        //    //                type = 3;
        //    //                break;

        //    //            default:
        //    //                type = t;
        //    //                break;
        //    //        }
        //    //        try
        //    //        {
        //    //            client.UpdateNewSymbol("HA" + dr["STOCK_ID"].ToString(), dr["CODE"].ToString(), "HASTC", type);
        //    //        }
        //    //        catch (Exception ex)
        //    //        {
        //    //            Console.WriteLine("Error while UpdateHASTCSecuritiesList, message=" + ex.Message);
        //    //        }
        //    //    }
        //    //}            

        //    //conn.Dispose();

        //    ProcessDeList();
        //}

        public static void ProcessTopPriceInfo()
        {
            OracleConnection conn = new OracleConnection(_infogate3Connection);  // C#
            conn.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = string.Format("SELECT * FROM TOPNPRICE WHERE TRUNC(SENDDATE)=TO_DATE('{0}','yyyy-MM-dd') ORDER BY SYMBOL", DateTime.Today.ToString("yyyy-MM-dd"));
            cmd.CommandType = CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();

            DBProxyServiceClient client = GetDatabaseClient();

            if (!dr.HasRows)
            {
                client.IG3_ResetStockTop3Price();
                client.IG3_ResetTopNPrice();
            }
            else
            {
                StringBuilder xml = new StringBuilder();
                StringBuilder xmlTopNPrice = new StringBuilder();

                xml.Append("<ROOT>");
                xmlTopNPrice.Append("<ROOT>");

                string symbol = string.Empty;
                int top = 0;
                double priceBid1 = 0, priceBid2 = 0, priceBid3 = 0, priceOffer1 = 0, priceOffer2 = 0, priceOffer3 = 0;
                double quantityBid1 = 0, quantityBid2 = 0, quantityBid3 = 0, quantityOffer1 = 0, quantityOffer2 = 0, quantityOffer3 = 0;
                while (dr.Read())
                {
                    xmlTopNPrice.AppendFormat("<topnprice Symbol=\"{0}\" Top=\"{1}\" BuyPrice=\"{2}\" BuyQtty=\"{3}\" SellPrice=\"{4}\" SellQtty=\"{5}\" SendDate=\"{6}\" " +
                                        "></topnprice>", symbol,
                                        Convert.ToDouble(dr["Top"].ToString()),
                                        Convert.ToDouble(dr["BuyPrice"].ToString()),
                                       Convert.ToDouble(dr["BuyQtty"].ToString()),
                                        Convert.ToDouble(dr["SellPrice"].ToString()),
                                        Convert.ToDouble(dr["SellQtty"].ToString()),
                                       Convert.ToDateTime(dr["SendDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss"));

                    if (symbol != dr["Symbol"].ToString())
                    {
                        if (!string.IsNullOrEmpty(symbol))
                        {
                            xml.AppendFormat("<Stock Symbol=\"{0}\" PriceBid1=\"{1}\" QuantityBid1=\"{2}\" PriceBid2=\"{3}\" QuantityBid2=\"{4}\" PriceBid3=\"{5}\" QuantityBid3=\"{6}\" " +
                                        "PriceOffer1=\"{7}\" QuantityOffer1=\"{8}\" PriceOffer2=\"{9}\" QuantityOffer2=\"{10}\" PriceOffer3=\"{11}\" QuantityOffer3=\"{12}\" " +
                                        "></Stock>", symbol,
                                        priceBid1,
                                        quantityBid1,
                                        priceBid2,
                                        quantityBid2,
                                        priceBid3,
                                        quantityBid3,
                                        priceOffer1,
                                        quantityOffer1,
                                        priceOffer2,
                                        quantityOffer2,
                                        priceOffer3,
                                        quantityOffer3);
                        }
                        priceBid1 = 0;
                        quantityBid1 = 0;
                        priceBid2 = 0;
                        quantityBid2 = 0;
                        priceBid3 = 0;
                        quantityBid3 = 0;
                        priceOffer1 = 0;
                        quantityOffer1 = 0;
                        priceOffer2 = 0;
                        quantityOffer2 = 0;
                        priceOffer3 = 0;
                        quantityOffer3 = 0;
                    }
                    else
                    {
                        top = Convert.ToInt32(dr["Top"].ToString());
                        if (top == 1)
                        {
                            priceBid1 = Convert.ToDouble(dr["BuyPrice"].ToString());
                            quantityBid1 = Convert.ToDouble(dr["BuyQtty"].ToString());
                            priceOffer1 = Convert.ToDouble(dr["SellPrice"].ToString());
                            quantityOffer1 = Convert.ToDouble(dr["SellQtty"].ToString());
                        }
                        else if (top == 2)
                        {
                            priceBid2 = Convert.ToDouble(dr["BuyPrice"].ToString());
                            quantityBid2 = Convert.ToDouble(dr["BuyQtty"].ToString());
                            priceOffer2 = Convert.ToDouble(dr["SellPrice"].ToString());
                            quantityOffer2 = Convert.ToDouble(dr["SellQtty"].ToString());
                        }
                        else if (top == 3)
                        {
                            priceBid3 = Convert.ToDouble(dr["BuyPrice"].ToString());
                            quantityBid3 = Convert.ToDouble(dr["BuyQtty"].ToString());
                            priceOffer3 = Convert.ToDouble(dr["SellPrice"].ToString());
                            quantityOffer3 = Convert.ToDouble(dr["SellQtty"].ToString());
                        }
                    }
                    symbol = dr["Symbol"].ToString();
                }
                if (priceBid1 + priceBid2 + priceBid3 + priceOffer1 + priceOffer2 + priceOffer3 > 0)
                {
                    xml.AppendFormat("<Stock Symbol=\"{0}\" PriceBid1=\"{1}\" QuantityBid1=\"{2}\" PriceBid2=\"{3}\" QuantityBid2=\"{4}\" PriceBid3=\"{5}\" QuantityBid3=\"{6}\" " +
                                "PriceOffer1=\"{7}\" QuantityOffer1=\"{8}\" PriceOffer2=\"{9}\" QuantityOffer2=\"{10}\" PriceOffer3=\"{11}\" QuantityOffer3=\"{12}\" " +
                                "></Stock>", symbol,
                                priceBid1,
                                quantityBid1,
                                priceBid2,
                                quantityBid2,
                                priceBid3,
                                quantityBid3,
                                priceOffer1,
                                quantityOffer1,
                                priceOffer2,
                                quantityOffer2,
                                priceOffer3,
                                quantityOffer3);
                }
                xmlTopNPrice.Append("</ROOT>");
                xml.Append("</ROOT>");

                try
                {
                    client.IG3_UpdateStockTop3Price(xml.ToString());
                    client.IG3_UpdateTopNPrice(xml.ToString());
                }
                catch
                {
                }
            }

            conn.Dispose();
        }

        public static void ProcessHAPutAdInfo()
        {
            return;
            ///////////////////////////////////
            OracleConnection conn = new OracleConnection(_infogate3Connection);  // C#
            conn.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = "SELECT STOCK_ID, TRADING_DATE, B_ORD_PRICE_1, B_ORD_QTTY_1, S_ORD_PRICE_1, S_ORD_QTTY_1 FROM STS_TOP3_PRICE_A WHERE NORP=2 AND (B_ORD_QTTY_1 <> 0 OR S_ORD_QTTY_1 <> 0)";
            cmd.CommandType = CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();

            DBProxyServiceClient client = GetDatabaseClient();

            if (!dr.HasRows)
            {
                client.HAGW_ResetPutAdInfo();
            }
            else
            {
                StringBuilder xml = new StringBuilder();
                xml.Append("<ROOT>");
                while (dr.Read())
                {
                    xml.AppendFormat("<Stock StockID=\"{0}\" BuyPrice=\"{1}\" BuyQuantity=\"{2}\" SellPrice=\"{3}\" SellQuantity=\"{4}\" ></Stock>", "HA" + dr["STOCK_ID"].ToString(),
                                       Convert.ToDouble(dr["B_ORD_PRICE_1"].ToString()),
                                       Convert.ToDouble(dr["B_ORD_QTTY_1"].ToString()),
                                       Convert.ToDouble(dr["S_ORD_PRICE_1"].ToString()),
                                       Convert.ToDouble(dr["S_ORD_QTTY_1"].ToString()));
                }
                xml.Append("</ROOT>");

                try
                {
                    client.HAGW_InsertPutAd(xml.ToString());
                }
                catch
                {
                    client.HAGW_InsertPutAd(xml.ToString());
                }
            }

            conn.Dispose();
        }
    }
}
