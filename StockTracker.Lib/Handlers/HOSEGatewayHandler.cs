﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockTracker.Lib.Handlers
{
    public class HOSEGatewayHandler
    {
        public void UpdateSecuritiesList()
        {
            GWDataClassesDataContext db = new GWDataClassesDataContext();
            List<SS_View_SecuritiesStatus> securities = db.SS_View_SecuritiesStatus.ToList();            

        }

        public void ProcessMarketInfo()
        {
            GWDataClassesDataContext db = new GWDataClassesDataContext();
            db.SS_UpdateMarketInfo();
            SS_CurrentMarketInfo marketInfo = db.SS_CurrentMarketInfos.SingleOrDefault();

            if (marketInfo != null)
            {
                // Update Market Info here
            }
        }

        public void ProcessStockInfo()
        {
            GWDataClassesDataContext db = new GWDataClassesDataContext();
            db.SS_UpdateStockInfo();
            List<SS_CurrentStockInfo> quotes = db.SS_CurrentStockInfos.ToList();

            if (quotes != null && quotes.Count > 0)
            {
                // Update Stock Info here
            }
        }

        public void ProcessIntraday()
        {
            GWDataClassesDataContext db = new GWDataClassesDataContext();
            db.SS_UpdateIntraday();
            List<SS_SymbolPermIntraday> intradayQuotes = db.SS_SymbolPermIntradays.ToList();

            if (intradayQuotes != null && intradayQuotes.Count > 0)
            {
                // Update Intraday Info here
            }
        }

        public void ProcessPutthrough()
        {
            GWDataClassesDataContext db = new GWDataClassesDataContext();
            db.SS_UpdatePutthrough();
            List<SS_PutAd> putAds = db.SS_PutAds.ToList();

            if (putAds != null && putAds.Count > 0)
            {
                // Update Put Ads here
            }

            List<SS_PutExec> putExecs = db.SS_PutExecs.ToList();

            if (putExecs != null && putExecs.Count > 0)
            {
                // Update Put Execs here
            }
        }
    }
}
