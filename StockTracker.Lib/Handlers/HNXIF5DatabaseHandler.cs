using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Runtime.InteropServices;
using System.IO;
using System.Globalization;
using StockTracker.Lib.DBProxies;
using System.Data.OracleClient;
using System.Configuration;

namespace StockTracker.Lib.Handlers
{
    // First, alter the infoshow 5 database schema
    /* 
    ALTER TABLE STS_STOCKS_INFO
    ADD (SIDE varchar2(1));

    CREATE OR REPLACE TRIGGER TR_STS_STOCK_INFO
    BEFORE UPDATE OR INSERT
    ON STS_STOCKS_INFO 
    FOR EACH ROW   
    BEGIN 
        IF ((:new.NM_TOTAL_TRADED_QTTY <> :old.NM_TOTAL_TRADED_QTTY) OR (:old.NM_TOTAL_TRADED_QTTY = NULL))
        THEN   
	    IF (:new.MATCH_PRICE <= :old.BEST_BID_PRICE) THEN
		    :new.SIDE := 'S';			
	    ELSIF (:new.MATCH_PRICE >= :old.BEST_OFFER_PRICE) THEN
		    :new.SIDE := 'B';			
	    ELSE
		    :new.SIDE := NULL;
	    END IF;
	
	    INSERT INTO STS_INTRADAY_INFO VALUES (
		    :new.TRADING_DATE, 
		    :new.TIME, 
		    :new.CODE,
		    :new.MATCH_PRICE,
		    :new.MATCH_QTTY,
		    :new.NM_TOTAL_TRADED_QTTY,
		    :new.BUY_COUNT,
		    :new.TOTAL_BID_QTTY,
		    :new.SELL_COUNT,
		    :new.TOTAL_OFFER_QTTY,
		    :new.BUY_FOREIGN_QTTY,
		    :new.BUY_FOREIGN_VALUE,
		    :new.SELL_FOREIGN_QTTY,
		    :new.SELL_FOREIGN_VALUE,
		    :new.SIDE
	    );
        END IF;
    END;

    CREATE TABLE STS_INTRADAY_INFO 
    (
	    TRADING_DATE DATE NOT NULL, 
	    TIME VARCHAR2(8), 
	    CODE VARCHAR2(20) NOT NULL, 
	    PRICE NUMBER, 
	    VOLUME NUMBER, 
	    TOTAL_VOLUME NUMBER, 
	    BUY_COUNT NUMBER, 
	    BUY_QTTY NUMBER, 
	    SELL_COUNT NUMBER, 
	    SELL_QTTY NUMBER, 
	    BUY_FOREIGN_QTTY NUMBER,
	    BUY_FOREIGN_VALUE NUMBER,
	    SELL_FOREIGN_QTTY NUMBER,
	    SELL_FOREIGN_VALUE NUMBER,
	    SIDE VARCHAR2(1)
    );

    UPDATE sts_stocks_info set match_price = 23000, nm_total_traded_qtty = nm_total_traded_qtty + 100 where code = 'ACB';

    UPDATE sts_stocks_info set match_price = 23100, nm_total_traded_qtty = nm_total_traded_qtty - 100 where code = 'ACB';
    */
    public class HNXIF5DatabaseHandler
    {
        //public const string HASTC_FLOOR_CODE = "02";
        public const int HASTC_STOCK_TYPE = 2;
        public const int HASTC_ETF_SYMBOL_TYPE = 6;
        private static DBProxyServiceClient _client = null;
        private static string _if5Connection = ConfigurationManager.AppSettings["Infoshow5Database"];

        private static DBProxyServiceClient GetDatabaseClient()
        {
            if (_client == null || _client.State != System.ServiceModel.CommunicationState.Opened)
            {
                _client = new DBProxyServiceClient();
            }

            return _client;
        }

        public static MarketInfo GetHNXInfoForTestingPurpose(out string errMsg)
        {
            try
            {
                OracleConnection conn = new OracleConnection(_if5Connection);  // C#
                conn.Open();
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = conn;
                //cmd.CommandText = "SELECT * FROM STS_MARKET_INFO WHERE MARKET_INFO_ID=2";
                cmd.CommandText = "SELECT * FROM COL_HASTC_MARKET_INFO";
                cmd.CommandType = CommandType.Text;
                OracleDataReader dr = cmd.ExecuteReader();
                MarketInfo market = null;
                double putthroughVolume = 0;
                double putthroughValue = 0;
                if (dr.Read())
                {
                    market = new MarketInfo();
                    market.Symbol = "HASTC";
                    market.TotalTrade = Convert.ToDouble(dr["TOTAL_TRADE"].ToString());

                    market.Date = ((DateTime)dr["TRADING_DATE"]).Add(TimeSpan.Parse(dr["TIME"].ToString()));

                    market.TotalValue = Convert.ToDouble(dr["TOTAL_VALUE"].ToString());
                    market.TotalVolume = Convert.ToDouble(dr["TOTAL_QTTY"].ToString());
                    market.BasicIndex = Convert.ToDouble(dr["PRIOR_MARKET_INDEX"].ToString());
                    market.CurrentIndex = Convert.ToDouble(dr["MARKET_INDEX"].ToString());
                    market.HighestIndex = Convert.ToDouble(dr["HIGHEST_INDEX"].ToString());
                    market.LowestIndex = Convert.ToDouble(dr["LOWEST_INDEX"].ToString());
                    market.Advances = Convert.ToInt32(dr["ADVANCES"].ToString());
                    market.NoChange = Convert.ToInt32(dr["NOCHANGE"].ToString());
                    market.Declines = Convert.ToInt32(dr["DECLINES"].ToString());
                    market.Status = dr["CURRENT_STATUS"].ToString();
                    putthroughVolume = Convert.ToDouble(dr["PT_TOTAL_QTTY"].ToString());
                    putthroughValue = Convert.ToDouble(dr["PT_TOTAL_VALUE"].ToString());
                }

                conn.Dispose();
                errMsg = "";
                return market;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return null;
            }
        }

        public static StockInfo GetStockInfoForTestingPurpose(string symbol)
        {
            OracleConnection conn = new OracleConnection(_if5Connection);  // C#
            conn.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            //cmd.CommandText = "SELECT * FROM STS_STOCKS_INFO WHERE FLOOR_CODE='02'";
            cmd.CommandText = "SELECT * FROM COL_HASTC_STOCKS_INFO WHERE CODE='" + symbol + "'";
            cmd.CommandType = CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();

            StockInfo stock = null;
            while (dr.Read())
            {
                if (Convert.ToInt32(dr["STOCK_TYPE"].ToString()) == HASTC_STOCK_TYPE)
                {
                    stock = new StockInfo();
                    stock.StockId = "HA" + dr["STOCK_ID"].ToString();
                    stock.Symbol = dr["CODE"].ToString();
                    stock.Date = ((DateTime)dr["TRADING_DATE"]).Add(TimeSpan.Parse(dr["TIME"].ToString()));

                    stock.PriceBasic = (dr["BASIC_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["BASIC_PRICE"]);

                    stock.PriceCeiling = (dr["CEILING_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["CEILING_PRICE"]);

                    stock.PriceFloor = (dr["FLOOR_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["FLOOR_PRICE"]);

                    stock.PriceCurrent = (dr["MATCH_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["MATCH_PRICE"]);

                    stock.PriceHigh = (dr["HIGHEST_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["HIGHEST_PRICE"]);

                    stock.PriceLow = (dr["LOWEST_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["LOWEST_PRICE"]);

                    stock.PriceOpen = (dr["OPEN_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["OPEN_PRICE"]);

                    stock.PriceClose = (dr["CLOSE_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["CLOSE_PRICE"]);

                    stock.PriceAverage = (dr["AVERAGE_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["AVERAGE_PRICE"]);

                    stock.Volume = (dr["MATCH_QTTY"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["MATCH_QTTY"]);

                    stock.TotalVolume = (dr["NM_TOTAL_TRADED_QTTY"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["NM_TOTAL_TRADED_QTTY"]);

                    stock.TotalValue = (dr["NM_TOTAL_TRADED_VALUE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["NM_TOTAL_TRADED_VALUE"]);

                    stock.CurrentForeignRoom = (dr["REMAIN_FOREIGN_QTTY"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["REMAIN_FOREIGN_QTTY"]);

                    stock.BuyCount = (dr["BID_COUNT"] == DBNull.Value) ?
                                      0 : Convert.ToDouble(dr["BID_COUNT"]);
                    stock.SellCount = (dr["OFFER_COUNT"] == DBNull.Value) ?
                                      0 : Convert.ToDouble(dr["OFFER_COUNT"]);
                    stock.BuyQuantity = (dr["TOTAL_BID_QTTY"] == DBNull.Value) ?
                                      0 : Convert.ToDouble(dr["TOTAL_BID_QTTY"]);
                    stock.SellQuantity = (dr["TOTAL_OFFER_QTTY"] == DBNull.Value) ?
                                      0 : Convert.ToDouble(dr["TOTAL_OFFER_QTTY"]);
                    stock.BuyForeignQuantity = (dr["BUY_FOREIGN_QTTY"] == DBNull.Value) ?
                                                0 : Convert.ToDouble(dr["BUY_FOREIGN_QTTY"]);
                    stock.SellForeignQuantity = (dr["SELL_FOREIGN_QTTY"] == DBNull.Value) ?
                                                0 : Convert.ToDouble(dr["SELL_FOREIGN_QTTY"]);
                    stock.BuyForeignValue = (dr["BUY_FOREIGN_VALUE"] == DBNull.Value) ?
                                                0 : Convert.ToDouble(dr["BUY_FOREIGN_VALUE"]);
                    stock.SellForeignValue = (dr["SELL_FOREIGN_VALUE"] == DBNull.Value) ?
                                                0 : Convert.ToDouble(dr["SELL_FOREIGN_VALUE"]);

                }
            }

            conn.Dispose();

            return stock;
        }

        public static void ProcessMarketInfo(out string _status)
        {
            OracleConnection conn = new OracleConnection(_if5Connection);  // C#
            conn.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = "SELECT * FROM STS_MARKET_INFO WHERE MARKET_INFO_ID=4383";
            //cmd.CommandText = "SELECT * FROM COL_HASTC_MARKET_INFO"; //Commented by MinhDQ 29/12/2014
            //cmd.CommandText = "SELECT * FROM COL_HASTC_MARKET_INFO WHERE MARKET_INFO_ID=4383"; //Added by MinhDQ 29/12/2014
            cmd.CommandType = CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();
            MarketInfo market = null;
            _status = null;
            double putthroughVolume = 0;
            double putthroughValue = 0;
            if (dr.Read())
            {
                market = new MarketInfo();
                market.Symbol = "HASTC";
                market.TotalTrade = Convert.ToDouble(dr["TOTAL_TRADE"].ToString());

                market.Date = ((DateTime)dr["TRADING_DATE"]).Add(TimeSpan.Parse(dr["TIME"].ToString()));

                market.TotalValue = Convert.ToDouble(dr["TOTAL_VALUE"].ToString());
                market.TotalVolume = Convert.ToDouble(dr["TOTAL_QTTY"].ToString());
                market.BasicIndex = Convert.ToDouble(dr["PRIOR_MARKET_INDEX"].ToString());
                market.CurrentIndex = Convert.ToDouble(dr["MARKET_INDEX"].ToString());
                market.HighestIndex = Convert.ToDouble(dr["HIGHEST_INDEX"].ToString());
                market.LowestIndex = Convert.ToDouble(dr["LOWEST_INDEX"].ToString());
                market.Advances = Convert.ToInt32(dr["ADVANCES"].ToString());
                market.NoChange = Convert.ToInt32(dr["NOCHANGE"].ToString());
                market.Declines = Convert.ToInt32(dr["DECLINES"].ToString());
                market.Status = dr["CURRENT_STATUS"].ToString();
                putthroughVolume = Convert.ToDouble(dr["PT_TOTAL_QTTY"].ToString());
                putthroughValue = Convert.ToDouble(dr["PT_TOTAL_VALUE"].ToString());
            }

            conn.Dispose();

            if (market != null)
            {
                _status = market.Status;

                DBProxyServiceClient client = GetDatabaseClient();

                try
                {
                    client.HAGW_UpdateMarketInfo(market.Date, market.Status, market.BasicIndex, market.CurrentIndex, market.HighestIndex, market.LowestIndex, market.TotalTrade, market.TotalVolume, putthroughVolume, market.TotalValue, putthroughValue, market.Advances, market.NoChange, market.Declines);
                    //Console.WriteLine(string.Format("HNXIF5DatabaseHandler.HAGW_UpdateMarketInfo Date:{0}, Status:{1}, CurrentIndex:{2}, TotalTrade:{3}, TotalVolume:{4}, TotalValue:{5} successfully", market.Date.ToString("dd/MM/yyyy HH:mm:ss"), market.Status, market.CurrentIndex, market.TotalTrade, market.TotalVolume, market.TotalValue));
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HNXIF5DatabaseHandler.HAGW_UpdateMarketInfo, detail:" + ex.Message);
                    Console.WriteLine("Re-trying...");
                    Console.ResetColor();

                    client.HAGW_UpdateMarketInfo(market.Date, market.Status, market.BasicIndex, market.CurrentIndex, market.HighestIndex, market.LowestIndex, market.TotalTrade, market.TotalVolume, putthroughVolume, market.TotalValue, putthroughValue, market.Advances, market.NoChange, market.Declines);
                }
            }
        }

        public static void ProcessStockInfo()
        {
            //Console.ForegroundColor = ConsoleColor.Yellow;
            //Console.WriteLine("Bat dau xu ly du lieu Oracle " + DateTime.Now);
            //Console.ResetColor();
            OracleConnection conn = new OracleConnection(_if5Connection);  // C#
            conn.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = "SELECT * FROM STS_STOCKS_INFO WHERE FLOOR_CODE='02'";
            //cmd.CommandText = "SELECT * FROM COL_HASTC_STOCKS_INFO";
            cmd.CommandType = CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();

            StringBuilder xml = new StringBuilder();
            xml.Append("<ROOT>");
            StringBuilder xmlPT = new StringBuilder();
            xmlPT.Append("<ROOT>");

            int stockCount = 0;
            int putthroughCount = 0;
            while (dr.Read())
            {
                if (Convert.ToInt32(dr["STOCK_TYPE"].ToString()) == HASTC_STOCK_TYPE || Convert.ToInt32(dr["STOCK_TYPE"].ToString()) == HASTC_ETF_SYMBOL_TYPE)
                {
                    StockInfo stock = new StockInfo();
                    stock.StockId = "HA" + dr["STOCK_ID"].ToString();
                    stock.Symbol = dr["CODE"].ToString();
                    stock.Date = ((DateTime)dr["TRADING_DATE"]).Add(TimeSpan.Parse(dr["TIME"].ToString()));

                    stock.PriceBasic = (dr["BASIC_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["BASIC_PRICE"]);

                    stock.PriceCeiling = (dr["CEILING_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["CEILING_PRICE"]);

                    stock.PriceFloor = (dr["FLOOR_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["FLOOR_PRICE"]);

                    stock.PriceCurrent = (dr["MATCH_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["MATCH_PRICE"]);

                    stock.PriceHigh = (dr["HIGHEST_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["HIGHEST_PRICE"]);

                    stock.PriceLow = (dr["LOWEST_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["LOWEST_PRICE"]);

                    stock.PriceOpen = (dr["OPEN_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["OPEN_PRICE"]);

                    stock.PriceClose = (dr["CLOSE_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["CLOSE_PRICE"]);

                    stock.PriceAverage = (dr["AVERAGE_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["AVERAGE_PRICE"]);

                    stock.Volume = (dr["MATCH_QTTY"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["MATCH_QTTY"]);

                    stock.TotalVolume = (dr["NM_TOTAL_TRADED_QTTY"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["NM_TOTAL_TRADED_QTTY"]);

                    stock.TotalValue = (dr["NM_TOTAL_TRADED_VALUE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["NM_TOTAL_TRADED_VALUE"]);

                    stock.CurrentForeignRoom = (dr["REMAIN_FOREIGN_QTTY"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(dr["REMAIN_FOREIGN_QTTY"]);

                    stock.BuyCount = (dr["BID_COUNT"] == DBNull.Value) ?
                                      0 : Convert.ToDouble(dr["BID_COUNT"]);
                    stock.SellCount = (dr["OFFER_COUNT"] == DBNull.Value) ?
                                      0 : Convert.ToDouble(dr["OFFER_COUNT"]);
                    stock.BuyQuantity = (dr["TOTAL_BID_QTTY"] == DBNull.Value) ?
                                      0 : Convert.ToDouble(dr["TOTAL_BID_QTTY"]);
                    stock.SellQuantity = (dr["TOTAL_OFFER_QTTY"] == DBNull.Value) ?
                                      0 : Convert.ToDouble(dr["TOTAL_OFFER_QTTY"]);
                    stock.BuyForeignQuantity = (dr["BUY_FOREIGN_QTTY"] == DBNull.Value) ?
                                                0 : Convert.ToDouble(dr["BUY_FOREIGN_QTTY"]);
                    stock.SellForeignQuantity = (dr["SELL_FOREIGN_QTTY"] == DBNull.Value) ?
                                                0 : Convert.ToDouble(dr["SELL_FOREIGN_QTTY"]);
                    stock.BuyForeignValue = (dr["BUY_FOREIGN_VALUE"] == DBNull.Value) ?
                                                0 : Convert.ToDouble(dr["BUY_FOREIGN_VALUE"]);
                    stock.SellForeignValue = (dr["SELL_FOREIGN_VALUE"] == DBNull.Value) ?
                                                0 : Convert.ToDouble(dr["SELL_FOREIGN_VALUE"]);

                    xml.AppendFormat("<Stock StockID=\"{0}\" Symbol=\"{1}\" Date=\"{2}\" PriceBasic=\"{3}\" PriceCeiling=\"{4}\" PriceFloor=\"{5}\" PriceCurrent=\"{6}\" Volume=\"{7}\" " +
                                        "PriceHigh=\"{8}\" PriceLow=\"{9}\" PriceOpen=\"{10}\" PriceClose=\"{11}\" PriceAverage=\"{12}\" TotalVolume=\"{13}\" TotalValue=\"{14}\" " +
                                        "CurrentForeignRoom=\"{15}\" BuyCount=\"{16}\" SellCount=\"{17}\" " +
                                        "BuyForeignQuantity=\"{18}\" SellForeignQuantity=\"{19}\" BuyForeignValue=\"{20}\" SellForeignValue=\"{21}\" BuyQuantity=\"{22}\" SellQuantity=\"{23}\" " +
                                        "></Stock>", stock.StockId, stock.Symbol, stock.Date.ToString("MM/dd/yyyy HH:mm:ss"),
                                        stock.PriceBasic.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceCeiling.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceFloor.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceCurrent.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.Volume.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceHigh.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceLow.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceOpen.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceClose.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceAverage.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.TotalVolume.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.TotalValue.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.CurrentForeignRoom.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.BuyCount.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.SellCount.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.BuyForeignQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.SellForeignQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.BuyForeignValue.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.SellForeignValue.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.BuyQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.SellQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat)
                                        );
                    stockCount++;
                }

                if (dr["PT_MATCH_QTTY"].ToString() != "0")
                {
                    xmlPT.AppendFormat("<Stock StockID=\"{0}\" PT_Volume=\"{1}\" PT_Price=\"{2}\" PT_TotalVolume=\"{3}\" PT_TotalValue=\"{4}\"></Stock>",
                   "HA" + dr["STOCK_ID"].ToString(),
                   Convert.ToDouble(dr["PT_MATCH_QTTY"].ToString()),
                   Convert.ToDouble(dr["PT_MATCH_PRICE"].ToString()),
                   Convert.ToDouble(dr["PT_TOTAL_TRADED_QTTY"].ToString()),
                   Convert.ToDouble(dr["PT_TOTAL_TRADED_VALUE"].ToString())
                   );
                    putthroughCount++;
                }
            }

            conn.Dispose();

            xml.Append("</ROOT>");
            xmlPT.Append("</ROOT>");
            //Console.ForegroundColor = ConsoleColor.Yellow;
            //Console.WriteLine("Ket thuc xu ly du lieu Oracle " + DateTime.Now);
            //Console.ResetColor();

            DBProxyServiceClient client = GetDatabaseClient();

            //Console.ForegroundColor = ConsoleColor.Magenta;
            //Console.WriteLine("Bat dau goi ham client.HAGW_UpdateStockPriceInfo " + DateTime.Now);
            //Console.ResetColor();
            try
            {
                client.HAGW_UpdateStockPriceInfo(xml.ToString());
                Console.WriteLine(string.Format("HNXIF5DatabaseHandler.HAGW_UpdateStockPriceInfo successfully, Stocks Count:{0}", stockCount));
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error in HNXIF5DatabaseHandler.HAGW_UpdateStockPriceInfo, detail:" + ex.Message);
                Console.WriteLine("Re-trying...");
                Console.ResetColor();

                client.HAGW_UpdateStockPriceInfo(xml.ToString());
            }
            //Console.ForegroundColor = ConsoleColor.Magenta;
            //Console.WriteLine("Ket thuc goi ham client.HAGW_UpdateStockPriceInfo " + DateTime.Now);
            //Console.ResetColor();

            
            try
            {
                client.HAGW_UpdatePutThrough(xmlPT.ToString());
                Console.WriteLine(string.Format("HNXIF5DatabaseHandler.HAGW_UpdatePutThrough successfully, Putthrough Count:{0}", putthroughCount));
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error in HNXIF5DatabaseHandler.HAGW_UpdatePutThrough, detail:" + ex.Message);
                Console.WriteLine("Re-trying...");
                Console.ResetColor();

                client.HAGW_UpdatePutThrough(xmlPT.ToString());
            }
            
        }

        private static void ProcessDeList()
        {
            OracleConnection conn = new OracleConnection(_if5Connection);  // C#
            conn.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = "SELECT * FROM STS_STOCKS_INFO WHERE FLOOR_CODE='02'";
            //cmd.CommandText = "SELECT * FROM COL_HASTC_STOCKS_INFO";
            cmd.CommandType = CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();

            StringBuilder sb = new StringBuilder();
            sb.Append(",");

            int stockCount = 0;
            while (dr.Read())
            {
                if (Convert.ToInt32(dr["STOCK_TYPE"].ToString()) == HASTC_STOCK_TYPE)
                {
                    sb.Append(dr["CODE"].ToString()).Append(",");
                    stockCount++;
                }
            }

            conn.Dispose();

            DBProxyServiceClient client = GetDatabaseClient();

            try
            {
                client.HAGW_ProcessDeList(sb.ToString());
                Console.WriteLine(string.Format("HNXIF5DatabaseHandler.HAGW_ProcessDeList successfully, Stocks Count:{0}", stockCount));
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error in HNXIF5DatabaseHandler.HAGW_ProcessDeList, detail:" + ex.Message);
                Console.WriteLine("Re-trying...");
                Console.ResetColor();

                client.HAGW_ProcessDeList(sb.ToString());
            }
        }

        public static void UpdateHASTCSecuritiesList()
        {
            //OracleConnection conn = new OracleConnection(_if5Connection);  // C#
            //conn.Open();
            //OracleCommand cmd = new OracleCommand();
            //cmd.Connection = conn;
            //cmd.CommandText = "SELECT STOCK_ID, CODE, FLOOR_CODE, STOCK_TYPE FROM STS_STOCKS_INFO WHERE FLOOR_CODE='02'";
            //cmd.CommandType = CommandType.Text;
            //OracleDataReader dr = cmd.ExecuteReader();

            //DBProxyServiceClient client = GetDatabaseClient();

            //while (dr.Read())
            //{
            //    if (dr["FLOOR_CODE"].ToString() == HASTC_FLOOR_CODE)
            //    {
            //        int t = Convert.ToInt32(dr["STOCK_TYPE"].ToString());
            //        int type;

            //        switch (t)
            //        {
            //            case 1:
            //                type = 2;
            //                break;

            //            case 2:
            //                type = 1;
            //                break;

            //            case 3:
            //                type = 3;
            //                break;

            //            default:
            //                type = t;
            //                break;
            //        }
            //        try
            //        {
            //            client.UpdateNewSymbol("HA" + dr["STOCK_ID"].ToString(), dr["CODE"].ToString(), "HASTC", type);
            //        }
            //        catch (Exception ex)
            //        {
            //            Console.WriteLine("Error while UpdateHASTCSecuritiesList, message=" + ex.Message);
            //        }
            //    }
            //}            

            //conn.Dispose();

            ProcessDeList();
        }

        public static void ProcessTopPriceInfo()
        {
            OracleConnection conn = new OracleConnection(_if5Connection);  // C#
            conn.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = "SELECT * FROM STS_TOP3_PRICE_A WHERE NORP=1";
            //cmd.CommandText = "SELECT * FROM COL_HASTC_TOP3_PRICE_A";
            cmd.CommandType = CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();

            DBProxyServiceClient client = GetDatabaseClient();

            if (!dr.HasRows)
            {
                client.HAGW_ResetStockTop3Price();
                Console.WriteLine("HNXIF5DatabaseHandler.HAGW_ResetStockTop3Price successfully");
            }
            else
            {
                int stockCount = 0;
                StringBuilder xml = new StringBuilder();
                xml.Append("<ROOT>");
                while (dr.Read())
                {
                    xml.AppendFormat("<Stock StockID=\"{0}\" PriceBid1=\"{1}\" QuantityBid1=\"{2}\" PriceBid2=\"{3}\" QuantityBid2=\"{4}\" PriceBid3=\"{5}\" QuantityBid3=\"{6}\" " +
                                        "PriceOffer1=\"{7}\" QuantityOffer1=\"{8}\" PriceOffer2=\"{9}\" QuantityOffer2=\"{10}\" PriceOffer3=\"{11}\" QuantityOffer3=\"{12}\" " +
                                        "></Stock>", "HA" + dr["STOCK_ID"].ToString(),
                                        Convert.ToDouble(dr["B_ORD_PRICE_1"].ToString()),
                                        Convert.ToDouble(dr["B_ORD_QTTY_1"].ToString()),
                                        Convert.ToDouble(dr["B_ORD_PRICE_2"].ToString()),
                                        Convert.ToDouble(dr["B_ORD_QTTY_2"].ToString()),
                                        Convert.ToDouble(dr["B_ORD_PRICE_3"].ToString()),
                                        Convert.ToDouble(dr["B_ORD_QTTY_3"].ToString()),
                                        Convert.ToDouble(dr["S_ORD_PRICE_1"].ToString()),
                                        Convert.ToDouble(dr["S_ORD_QTTY_1"].ToString()),
                                        Convert.ToDouble(dr["S_ORD_PRICE_2"].ToString()),
                                        Convert.ToDouble(dr["S_ORD_QTTY_2"].ToString()),
                                        Convert.ToDouble(dr["S_ORD_PRICE_3"].ToString()),
                                        Convert.ToDouble(dr["S_ORD_QTTY_3"].ToString()));
                    stockCount++;
                }
                xml.Append("</ROOT>");

                try
                {
                    client.HAGW_UpdateStockTop3Price(xml.ToString());
                    Console.WriteLine(string.Format("HNXIF5DatabaseHandler.HAGW_UpdateStockTop3Price successfully, Stocks Count:{0}", stockCount));
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HNXIF5DatabaseHandler.HAGW_UpdateStockTop3Price, detail:" + ex.Message);
                    Console.WriteLine("Re-trying...");
                    Console.ResetColor();

                    client.HAGW_UpdateStockTop3Price(xml.ToString());
                }
            }

            conn.Dispose();
        }

        public static void ProcessHAPutAdInfo()
        {
            OracleConnection conn = new OracleConnection(_if5Connection);  // C#
            conn.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = "SELECT STOCK_ID, TRADING_DATE, B_ORD_PRICE_1, B_ORD_QTTY_1, S_ORD_PRICE_1, S_ORD_QTTY_1 FROM STS_TOP3_PRICE_A WHERE NORP=2 AND (B_ORD_QTTY_1 <> 0 OR S_ORD_QTTY_1 <> 0)";
            //cmd.CommandText = "SELECT STOCK_ID, TRADING_DATE, ORDER_PRICE_BUY, ORDER_QTTY_BUY, ORDER_PRICE_SELL, ORDER_QTTY_SELL FROM COL_HASTC_TOP3_PRICE_B WHERE (ORDER_QTTY_BUY <> 0 OR ORDER_QTTY_SELL <> 0)";
            cmd.CommandType = CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();

            DBProxyServiceClient client = GetDatabaseClient();

            if (!dr.HasRows)
            {
                client.HAGW_ResetPutAdInfo();
            }
            else
            {
                int stockCount = 0;
                StringBuilder xml = new StringBuilder();
                xml.Append("<ROOT>");
                while (dr.Read())
                {
                    xml.AppendFormat("<Stock StockID=\"{0}\" BuyPrice=\"{1}\" BuyQuantity=\"{2}\" SellPrice=\"{3}\" SellQuantity=\"{4}\" ></Stock>", "HA" + dr["STOCK_ID"].ToString(),
                                       Convert.ToDouble(dr["B_ORD_PRICE_1"].ToString()),
                                       Convert.ToDouble(dr["B_ORD_QTTY_1"].ToString()),
                                       Convert.ToDouble(dr["S_ORD_PRICE_1"].ToString()),
                                       Convert.ToDouble(dr["S_ORD_QTTY_1"].ToString()));
                    stockCount++;
                }
                xml.Append("</ROOT>");

                try
                {
                    client.HAGW_InsertPutAd(xml.ToString());
                    Console.WriteLine(string.Format("HNXIF5DatabaseHandler.ProcessHAPutAdInfo successfully, Stocks Count:{0}", stockCount));
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in HNXIF5DatabaseHandler.ProcessHAPutAdInfo, detail:" + ex.Message);
                    Console.WriteLine("Re-trying...");
                    Console.ResetColor();

                    client.HAGW_InsertPutAd(xml.ToString());
                }
            }

            conn.Dispose();
        }
    }
}
