﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using StockTracker.Lib.DBProxies;

namespace StockTracker.Lib.Handlers
{
    public class HOSEFileHandler
    {
        public const string HOSTC_SECTYPE_S = "S";
        public const string HOSTC_SECTYPE_U = "U";
        public const string HOSTC_SECTYPE_D = "D";
        public const string HOSTC_SECTYPE_E = "E";
        public const string HOSTC_SECTYPE_W = "W";

        private static DBProxyServiceClient _client = null;

        private static DBProxyServiceClient GetDatabaseClient()
        {
            if (_client == null || _client.State != System.ServiceModel.CommunicationState.Opened)
            {
                _client = new DBProxyServiceClient();
            }

            return _client;
        }

        public static bool IsNewDay = false;

        private static Dictionary<string, double> _openPrices = new Dictionary<string, double>();
        private static int BUFFER_SIZE = 4096;

        private static MemoryStream ReadAllBytes(string filePath)
        {
            MemoryStream ms = new MemoryStream();
            using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                byte[] buffer = new byte[BUFFER_SIZE];

                int read = 0;
                while ((read = fs.Read(buffer, 0, BUFFER_SIZE)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                ms.Seek(0, SeekOrigin.Begin);
            }

            return ms;
        }

        public static MarketInfo ProcessMarketInfo(string hostcDirectory, string status)
        {
            MarketInfo market = new MarketInfo();
            
            using (MemoryStream ms = ReadAllBytes((hostcDirectory.EndsWith("\\") ? hostcDirectory : hostcDirectory + "\\") + "INDEX\\" + DateTime.Today.ToString("yyyyMMdd") + "_" + "VNINDEX.DAT"))
            {
                BinaryReader brdr = new BinaryReader(ms);
                brdr.BaseStream.Seek(brdr.BaseStream.Length - 58, SeekOrigin.Begin);

                int setIndex = brdr.ReadInt32();
                int totalTrade = brdr.ReadInt32();
                double totalShare = brdr.ReadDouble();
                double totalValue = brdr.ReadDouble();
                double upVolume = brdr.ReadDouble();
                double downVolume = brdr.ReadDouble();
                double nochangeVolume = brdr.ReadDouble();
                short advances = brdr.ReadInt16();
                short nochange = brdr.ReadInt16();
                short declines = brdr.ReadInt16();
                
                //int set50Index = brdr.ReadInt32();
                //string marketId = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                //string filler = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                string time = brdr.ReadInt32().ToString();
                if (time.Length == 5) time = "0" + time;
                int seconds = int.Parse(time.Substring(4, 2));
                int minutes = int.Parse(time.Substring(2, 2));
                int hours = int.Parse(time.Substring(0, 2));

                market.Symbol = "HOSTC";
                market.Date = DateTime.Today.Add(new TimeSpan(hours, minutes, seconds));
                market.TotalTrade = totalTrade;
                market.TotalValue = totalValue * 1000000;
                market.TotalVolume = totalShare;
                market.CurrentIndex = ((double)setIndex) / 100;
                market.Advances = advances;
                market.NoChange = nochange;
                market.Declines = declines;
            }
            market.Status = status;

            DBProxyServiceClient client = GetDatabaseClient();

            try
            {
                client.HOGW_UpdateMarketInfo(market.Date, market.Status, market.CurrentIndex, market.TotalTrade, market.TotalVolume, market.TotalValue, market.Advances, market.NoChange, market.Declines);
                Console.ForegroundColor = ConsoleColor.DarkBlue;
                Console.WriteLine("ProcessMarketInfo completed");
                Console.ResetColor();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error in function ProcessMarketInfo, detail:" + ex.Message);
                Console.ResetColor();
                client.HOGW_UpdateMarketInfo(market.Date, market.Status, market.CurrentIndex, market.TotalTrade, market.TotalVolume, market.TotalValue, market.Advances, market.NoChange, market.Declines);
            }

            return market;
        }

        public static void Reset3SessionsInfo()
        {
            DBProxyServiceClient client = GetDatabaseClient();

            try
            {
                client.HOGW_Update3SessionsInfo(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            }
            catch
            {
                client.HOGW_Update3SessionsInfo(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            }
        }

        public static void ResetAllMarketInfo()
        {
            DBProxyServiceClient client = GetDatabaseClient();

            try
            {
                client.HOGW_ResetMarketInfo();
            }
            catch
            {
                client.HOGW_ResetMarketInfo();
            }
        }

        public static void Process3SessionsInfo(string hostcDirectory)
        {
            TimeSpan pts = new TimeSpan(0, 0, 0);
            int totalTrade1 = 0;
            double totalValue1 = 0;
            double totalShare1 = 0;
            double index1 = 0;
            int totalTrade2 = 0;
            double totalValue2 = 0;
            double totalShare2 = 0;
            double index2 = 0;
            int totalTrade3 = 0;
            double totalValue3 = 0;
            double totalShare3 = 0;
            double index3 = 0;
            int totalTrade = 0;
            double totalShare = 0;
            double totalValue = 0;
            double index = 0;
            int p_totalTrade = 0;

            // HSX sua lai thoi gian giao dich tu thang 7/2013
            //TimeSpan t1 = new TimeSpan(9, 15, 0);
            //TimeSpan t2 = new TimeSpan(13, 45, 0);//new TimeSpan(14, 30, 0);
            //TimeSpan t3 = new TimeSpan(14, 0, 0); //new TimeSpan(14, 45, 0);
            //TimeSpan t4 = new TimeSpan(14, 15, 0); // new TimeSpan(15, 00, 0);

            // Update tu ngay 22/07/2013
            TimeSpan t1 = new TimeSpan(9, 15, 0);
            TimeSpan t2 = new TimeSpan(14, 30, 0);//new TimeSpan(14, 30, 0);
            TimeSpan t3 = new TimeSpan(14, 45, 0); //new TimeSpan(14, 45, 0);
            TimeSpan t4 = new TimeSpan(15, 00, 0); // new TimeSpan(15, 00, 0);

            using (MemoryStream ms = ReadAllBytes((hostcDirectory.EndsWith("\\") ? hostcDirectory : hostcDirectory + "\\") + "INDEX\\" + DateTime.Today.ToString("yyyyMMdd") + "_" + "VNINDEX.DAT"))
            {
                BinaryReader brdr = new BinaryReader(ms);
                Boolean b1 = true;
                Boolean b2 = true;
                Boolean b3 = true;

                while (brdr.BaseStream.Position < brdr.BaseStream.Length)
                {
                    int setIndex = brdr.ReadInt32();
                    totalTrade = brdr.ReadInt32();
                    totalShare = brdr.ReadDouble();
                    totalValue = brdr.ReadDouble() * 1000000;
                    double upVolume = brdr.ReadDouble();
                    double nochangeVolume = brdr.ReadDouble();
                    double downVolume = brdr.ReadDouble();
                    short advances = brdr.ReadInt16();
                    short nochange = brdr.ReadInt16();
                    short declines = brdr.ReadInt16();
                    //int set50Index = brdr.ReadInt32();
                    //string marketId = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    //string filler = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    string time = brdr.ReadInt32().ToString();
                    if (time.Length == 5) time = "0" + time;
                    int seconds = int.Parse(time.Substring(4, 2));
                    int minutes = int.Parse(time.Substring(2, 2));
                    int hours = int.Parse(time.Substring(0, 2));
                    index = ((double)setIndex) / 100;



                    TimeSpan ts = new TimeSpan(hours, minutes, seconds);

                    //if (pts < t1 && ts > t1 && ts < t2)
                    if (ts > t1 && ts < t2 && totalShare != 0 && b1)
                    {
                        totalTrade1 = totalTrade;
                        totalValue1 = totalValue;
                        totalShare1 = totalShare;
                        index1 = index;
                        b1 = false;
                    }
                    //if (pts < t2 && ts > t2 && ts < t3)
                    if (ts > t2 && ts < t3 && b2)
                    {
                        totalTrade2 = totalTrade - totalTrade1;
                        totalValue2 = totalValue - totalValue1;
                        totalShare2 = totalShare - totalShare1;
                        index2 = index;
                        b2 = false;
                    }
                    //if (pts < t3 && ts > t3 && ts < t4)
                    if (ts > t3 && ts < t4 && (totalShare - (totalShare1 + totalShare2) != 0) && b3)
                    {
                        totalTrade3 = totalTrade - (totalTrade1 + totalTrade2);
                        totalValue3 = totalValue - (totalValue1 + totalValue2);
                        totalShare3 = totalShare - (totalShare1 + totalShare2);
                        index3 = index;
                        b3 = false;
                    }
                    p_totalTrade = totalTrade;
                    pts = ts;
                }
            }

            DBProxyServiceClient client = GetDatabaseClient();

            try
            {
                client.HOGW_Update3SessionsInfo(totalTrade1, totalShare1, totalValue1, index1, totalTrade2, totalShare2, totalValue2, index2, totalTrade3, totalShare3, totalValue3, index3);
            }
            catch
            {
                client.HOGW_Update3SessionsInfo(totalTrade1, totalShare1, totalValue1, index1, totalTrade2, totalShare2, totalValue2, index2, totalTrade3, totalShare3, totalValue3, index3);
            }
        }

        public static void GetMarketStat(string hostcDirectory, out string status, out TimeSpan time)
        {
            using (MemoryStream ms = ReadAllBytes(hostcDirectory + "MARKET_STAT.DAT"))
            {
                BinaryReader brdr = new BinaryReader(ms);
                brdr.BaseStream.Seek(brdr.BaseStream.Length - 5, SeekOrigin.Begin);

                status = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();

                string t = brdr.ReadInt32().ToString();
                if (t.Length == 5) t = "0" + t;
                int seconds = int.Parse(t.Substring(4, 2));
                int minutes = int.Parse(t.Substring(2, 2));
                int hours = int.Parse(t.Substring(0, 2));

                time = new TimeSpan(hours, minutes, seconds);
            }
        }

        public static void ProcessStockInfo(string hostcDirectory, string status)
        {
            StringBuilder xml = new StringBuilder();
            xml.Append("<ROOT>");

            using (MemoryStream ms = ReadAllBytes(hostcDirectory + "SECURITY.DAT"))
            {
                BinaryReader brdr = new BinaryReader(ms);

                while (brdr.BaseStream.Position < brdr.BaseStream.Length)
                {
                    int stockNo = brdr.ReadInt16();
                    string symbol = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(8)).Trim();
                    string type = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    //if (type == HOSTC_SECTYPE_W) continue;

                    int ceiling = brdr.ReadInt32();
                    int floor = brdr.ReadInt32();
                    double bigLotValue = brdr.ReadDouble();
                    string name = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(25)).Trim();
                    string sectorNo = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    string designated = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    string suspension = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    string delist = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    string haltResumeFlag = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    string split = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    string benefit = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    string meeting = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    string notice = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    string clientIdRequired = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    short couponRate = brdr.ReadInt16();
                    string issueDate = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(6)).Trim();
                    string matureDate = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(6)).Trim();
                    int avgPrice = brdr.ReadInt32();
                    short parValue = brdr.ReadInt16();
                    string SDCFlag = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    int priorClosePrice = brdr.ReadInt32();
                    string priorCloseDate = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(6)).Trim();
                    int projectOpen = brdr.ReadInt32();
                    int openPrice = brdr.ReadInt32();
                    int last = brdr.ReadInt32();
                    int lastVol = brdr.ReadInt32();
                    double lastVal = brdr.ReadDouble();
                    int highest = brdr.ReadInt32();
                    int lowest = brdr.ReadInt32();
                    double totalShares = brdr.ReadDouble();
                    double totalValue = brdr.ReadDouble();
                    short accumulateDeal = brdr.ReadInt16();
                    short bigDeal = brdr.ReadInt16();
                    int bigVolume = brdr.ReadInt32();
                    double bigValue = brdr.ReadDouble();
                    short oddDeal = brdr.ReadInt16();
                    int oddVolume = brdr.ReadInt32();
                    double oddValue = brdr.ReadDouble();
                    int best1Bid = brdr.ReadInt32();
                    int best1BidVolume = brdr.ReadInt32();
                    int best2Bid = brdr.ReadInt32();
                    int best2BidVolume = brdr.ReadInt32();
                    int best3Bid = brdr.ReadInt32();
                    int best3BidVolume = brdr.ReadInt32();
                    int best1Offer = brdr.ReadInt32();
                    int best1OfferVolume = brdr.ReadInt32();
                    int best2Offer = brdr.ReadInt32();
                    int best2OfferVolume = brdr.ReadInt32();
                    int best3Offer = brdr.ReadInt32();
                    int best3OfferVolume = brdr.ReadInt32();
                    short boardLost = brdr.ReadInt16();

                    //if (type == HOSTC_SECTYPE_W)
                    //{
                        string underlyingSymbol = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(8)).Trim();
                        string issuerName = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(25)).Trim();
                        string coveredWarrantType = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                        string maturityDate = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(8)).Trim();
                        string lastTradingDate = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(8)).Trim();
                        int exercisePrice = brdr.ReadInt32();
                        string exerciseRatio = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(11)).Trim();
                        double listedShares = brdr.ReadDouble();
                    //}

                    if (type == HOSTC_SECTYPE_S || type == HOSTC_SECTYPE_U || type == HOSTC_SECTYPE_E || type == HOSTC_SECTYPE_W)
                    {
                        StockInfo stock = new StockInfo();
                        stock.StockId = "HO" + stockNo.ToString();
                        stock.Symbol = symbol;
                        stock.Date = DateTime.Now;
                        stock.PriceBasic = priorClosePrice * 10;
                        stock.PriceCeiling = ceiling * 10;
                        stock.PriceFloor = floor * 10;

                        if (status == "P" || status == "A")
                        {
                            stock.PriceCurrent = projectOpen * 10;
                        }
                        else
                        {
                            stock.PriceCurrent = last * 10;
                        }
                        stock.Volume = lastVol * 10;

                        stock.PriceHigh = highest * 10;
                        stock.PriceLow = lowest * 10;

                        if (status == "J" || status == "P" || status == "O" || status == "A")
                        {
                            stock.PriceOpen = openPrice * 10;
                        }

                        stock.PriceClose = 0;
                        stock.PriceAverage = avgPrice;
                        stock.TotalVolume = lastVol * 10;
                        stock.TotalValue = totalValue * 1000000;
                        stock.Bid1Price = best1Bid * 10;
                        stock.Bid1Volume = best1BidVolume * 10;
                        stock.Bid2Price = best2Bid * 10;
                        stock.Bid2Volume = best2BidVolume * 10;
                        stock.Bid3Price = best3Bid * 10;
                        stock.Bid3Volume = best3BidVolume * 10;
                        stock.Offer1Price = best1Offer * 10;
                        stock.Offer1Volume = best1OfferVolume * 10;
                        stock.Offer2Price = best2Offer * 10;
                        stock.Offer2Volume = best2OfferVolume * 10;
                        stock.Offer3Price = best3Offer * 10;
                        stock.Offer3Volume = best3OfferVolume * 10;

                        xml.AppendFormat("<Stock StockID=\"{0}\" Symbol=\"{1}\" Date=\"{2}\" PriceBasic=\"{3}\" PriceCeiling=\"{4}\" PriceFloor=\"{5}\" PriceCurrent=\"{6}\" Volume=\"{7}\" " +
                                "PriceHigh=\"{8}\" PriceLow=\"{9}\" PriceOpen=\"{10}\" PriceClose=\"{11}\" PriceAverage=\"{12}\" TotalVolume=\"{13}\" TotalValue=\"{14}\" " +
                                "PriceBid1=\"{15}\" QuantityBid1=\"{16}\" PriceBid2=\"{17}\" QuantityBid2=\"{18}\" PriceBid3=\"{19}\" QuantityBid3=\"{20}\" " +
                                "PriceOffer1=\"{21}\" QuantityOffer1=\"{22}\" PriceOffer2=\"{23}\" QuantityOffer2=\"{24}\" PriceOffer3=\"{25}\" QuantityOffer3=\"{26}\" " +
                                "></Stock>", stock.StockId, stock.Symbol, stock.Date, stock.PriceBasic, stock.PriceCeiling, stock.PriceFloor, stock.PriceCurrent, stock.Volume, stock.PriceHigh, stock.PriceLow, stock.PriceOpen, stock.PriceClose, stock.PriceAverage, stock.TotalVolume, stock.TotalValue,
                                stock.Bid1Price, stock.Bid1Volume, stock.Bid2Price, stock.Bid2Volume, stock.Bid3Price, stock.Bid3Volume,
                                stock.Offer1Price, stock.Offer1Volume, stock.Offer2Price, stock.Offer2Volume, stock.Offer3Price, stock.Offer3Volume);
                    }
                }
            }

            xml.Append("</ROOT>");

            DBProxyServiceClient client = GetDatabaseClient();

            try
            {
                client.HOGW_UpdateStockInfo(xml.ToString());
            }
            catch
            {
                client.HOGW_UpdateStockInfo(xml.ToString());
            }
        }

        private static int _lastLEOffset = 0;
        public static void ResetLEInfoOffset()
        {
            _lastLEOffset = 0;
        }
        public static void ProcessLEInfo(string hostcDirectory, string status)
        {
            DBProxyServiceClient client = GetDatabaseClient();

            if (IsNewDay) _lastLEOffset = 0;

            List<int> stockNoList = new List<int>();
            List<double> volumeList = new List<double>();
            List<double> matchedVolumeList = new List<double>();

            bool firstStart = (_lastLEOffset == 0);

            StringBuilder xml = new StringBuilder();
            xml.Append("<ROOT>");

            using (MemoryStream ms = ReadAllBytes(hostcDirectory + "LE.DAT"))
            {
                BinaryReader brdr = new BinaryReader(ms);

                brdr.BaseStream.Seek(_lastLEOffset, SeekOrigin.Begin);
                while (brdr.BaseStream.Position < brdr.BaseStream.Length)
                {
                    int stockNo = brdr.ReadInt32();
                    int price = brdr.ReadInt32() * 10;
                    int vol = brdr.ReadInt32() * 10;
                    double val = brdr.ReadDouble();
                    int highest = brdr.ReadInt32() * 10;
                    int lowest = brdr.ReadInt32() * 10;

                    if (!stockNoList.Contains(stockNo))
                    {
                        stockNoList.Add(stockNo);
                        volumeList.Add(vol);
                        matchedVolumeList.Add(vol);
                    }
                    else
                    {
                        int index = stockNoList.IndexOf(stockNo);
                        if (volumeList[index] != vol)
                        {
                            matchedVolumeList[index] = vol - volumeList[index];
                            volumeList[index] = vol;
                        }
                    }
                    string time = brdr.ReadInt32().ToString();
                    if (time.Length == 5) time = "0" + time;

                    if (time.Length == 6)
                    {
                        int seconds = int.Parse(time.Substring(4, 2));
                        int minutes = int.Parse(time.Substring(2, 2));
                        int hours = int.Parse(time.Substring(0, 2));

                        DateTime date = DateTime.Today.Add(new TimeSpan(hours, minutes, seconds));

                        xml.AppendFormat("<LE StockID=\"{0}\" Date=\"{1}\" Price=\"{2}\" Volume=\"{3}\"></LE>", "HO" + stockNo.ToString(), date.ToString("MM/dd/yyyy HH:mm:ss"), price, vol);
                    }


                    _lastLEOffset += 32;
                }
            }

            xml.Append("</ROOT>");

            if (firstStart)
            {
                try
                {
                    client.HOGW_DeleteLEInfo();
                }
                catch
                {
                    client.HOGW_DeleteLEInfo();
                }
            }

            try
            {
                client.HOGW_UpdateLEInfo(xml.ToString());
            }
            catch (Exception ex)
            {
                client.HOGW_UpdateLEInfo(xml.ToString());
            }

        }

        public static void UpdateSecuritiesList(string hostcDirectory)
        {
            //ProcessSecuritiesList(hostcDirectory);
            ProcessNewList(hostcDirectory);
            ProcessDeList(hostcDirectory);
        }

        // Scan securities list and update if needed
        public static void ProcessSecuritiesList(string hostcDirectory)
        {
            DBProxyServiceClient client = GetDatabaseClient();

            using (MemoryStream ms = ReadAllBytes(hostcDirectory + "SECURITY.DAT"))
            {
                BinaryReader brdr = new BinaryReader(ms);

                while (brdr.BaseStream.Position < brdr.BaseStream.Length)
                {
                    int stockNo = brdr.ReadInt16();
                    string symbol = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(8)).Trim();
                    string t = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    int type = (t == "S" ? 1 : (t == "D" ? 2 : 3));
                    int ceiling = brdr.ReadInt32();
                    int floor = brdr.ReadInt32();
                    double bigLotValue = brdr.ReadDouble();
                    string name = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(25)).Trim();
                    string sectorNo = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    string designated = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    string suspension = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    string delist = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    string haltResumeFlag = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    string split = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    string benefit = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    string meeting = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    string notice = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    string clientIdRequired = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    short couponRate = brdr.ReadInt16();
                    string issueDate = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(6)).Trim();
                    string matureDate = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(6)).Trim();
                    int avgPrice = brdr.ReadInt32();
                    short parValue = brdr.ReadInt16();
                    string SDCFlag = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                    int priorClosePrice = brdr.ReadInt32();
                    string priorCloseDate = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(6)).Trim();
                    int projectOpen = brdr.ReadInt32();
                    int openPrice = brdr.ReadInt32();
                    int last = brdr.ReadInt32();
                    int lastVol = brdr.ReadInt32();
                    double lastVal = brdr.ReadDouble();
                    int highest = brdr.ReadInt32();
                    int lowest = brdr.ReadInt32();
                    double totalShares = brdr.ReadDouble();
                    double totalValue = brdr.ReadDouble();
                    short accumulateDeal = brdr.ReadInt16();
                    short bigDeal = brdr.ReadInt16();
                    int bigVolume = brdr.ReadInt32();
                    double bigValue = brdr.ReadDouble();
                    short oddDeal = brdr.ReadInt16();
                    int oddVolume = brdr.ReadInt32();
                    double oddValue = brdr.ReadDouble();
                    int best1Bid = brdr.ReadInt32();
                    int best1BidVolume = brdr.ReadInt32();
                    int best2Bid = brdr.ReadInt32();
                    int best2BidVolume = brdr.ReadInt32();
                    int best3Bid = brdr.ReadInt32();
                    int best3BidVolume = brdr.ReadInt32();
                    int best1Offer = brdr.ReadInt32();
                    int best1OfferVolume = brdr.ReadInt32();
                    int best2Offer = brdr.ReadInt32();
                    int best2OfferVolume = brdr.ReadInt32();
                    int best3Offer = brdr.ReadInt32();
                    int best3OfferVolume = brdr.ReadInt32();
                    short boardLost = brdr.ReadInt16();

                    try
                    {
                        client.UpdateNewSymbol("HO" + stockNo.ToString(), symbol, "HOSTC", type);
                    }
                    catch
                    {
                        // Do Nothing
                    }
                }
            }
        }

        public static void ProcessNewList(string hostcDirectory)
        {
            if (File.Exists(hostcDirectory + "NEWLIST.DAT"))
            {
                DBProxyServiceClient client = GetDatabaseClient();

                using (MemoryStream ms = ReadAllBytes(hostcDirectory + "NEWLIST.DAT"))
                {
                    BinaryReader brdr = new BinaryReader(ms);

                    while (brdr.BaseStream.Position < brdr.BaseStream.Length)
                    {
                        int stockNo = brdr.ReadInt16();
                        string symbol = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(8)).Trim();
                        string t = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                        int type = (t == "S" ? 1 : (t == "D" ? 2 : 3));
                        string name = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(25)).Trim();
                        int sectorNo = brdr.ReadInt16();

                        try
                        {
                            client.UpdateNewSymbol("HO" + stockNo.ToString(), symbol, "HOSTC", type);
                        }
                        catch
                        {
                            // Do Nothing
                        }
                    }
                }
            }
        }

        public static void ProcessDeList(string hostcDirectory)
        {
            if (File.Exists(hostcDirectory + "DELIST.DAT"))
            {
                DBProxyServiceClient client = GetDatabaseClient();

                using (MemoryStream ms = ReadAllBytes(hostcDirectory + "DELIST.DAT"))
                {
                    BinaryReader brdr = new BinaryReader(ms);

                    while (brdr.BaseStream.Position < brdr.BaseStream.Length)
                    {
                        int stockNo = brdr.ReadInt16();
                        string symbol = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(8)).Trim();
                        string t = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                        int type = (t == "S" ? 1 : (t == "D" ? 2 : 3));
                        string name = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(25)).Trim();
                        int sectorNo = brdr.ReadInt16();

                        try
                        {
                            client.UpdateListingStatus(symbol, false);
                        }
                        catch
                        {
                            // Do Nothing
                        }
                    }
                }
            }
        }

        public static void ProcessPutAd(string hostcDirectory)
        {
            DBProxyServiceClient client = GetDatabaseClient();

            if (!File.Exists(hostcDirectory + "PUT_AD.DAT"))
            {
                try
                {
                    client.HOGW_DeleteAllPutAd();
                }
                catch
                {
                    client.HOGW_DeleteAllPutAd();
                }

                return;
            }
            else
            {
                try
                {
                    client.HOGW_DeleteAllPutAd();
                }
                catch
                {
                    client.HOGW_DeleteAllPutAd();
                }

                StringBuilder xml = new StringBuilder();
                xml.Append("<ROOT>");
                using (MemoryStream ms = ReadAllBytes(hostcDirectory + "PUT_AD.DAT"))
                {
                    BinaryReader brdr = new BinaryReader(ms);

                    while (brdr.BaseStream.Position < brdr.BaseStream.Length)
                    {
                        int tradeID = brdr.ReadInt16();
                        int stockNo = brdr.ReadInt16();
                        int vol = brdr.ReadInt32();
                        double price = brdr.ReadDouble() * 1000;
                        int firmNo = brdr.ReadInt32();
                        string side = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                        string board = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();

                        string t = brdr.ReadInt32().ToString();
                        string flag = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();
                        if (t != "0")
                        {
                            if (t.Length == 5) t = "0" + t;
                            if (t.Length == 0) t = "000000";

                            int seconds = int.Parse(t.Substring(4, 2));
                            int minutes = int.Parse(t.Substring(2, 2));
                            int hours = int.Parse(t.Substring(0, 2));

                            DateTime time = DateTime.Today.Add(new TimeSpan(hours, minutes, seconds));


                            xml.AppendFormat("<PutAd TradeID=\"{0}\" StockID=\"{1}\"  Volume=\"{2}\" Price=\"{3}\" FirmNo=\"{4}\" Side=\"{5}\" Board=\"{6}\" Time=\"{7}\" Flag=\"{8}\"></PutAd>", tradeID, "HO" + stockNo.ToString(), vol, price, firmNo, side, board, time.ToString("MM/dd/yyyy hh:mm:ss"), flag);
                        }
                    }
                }

                xml.Append("</ROOT>");

                try
                {
                    client.HOGW_InsertPutAd(xml.ToString());
                }
                catch
                {
                    client.HOGW_InsertPutAd(xml.ToString());
                }
            }
        }

        public static void ProcessPutExec(string hostcDirectory)
        {
            DBProxyServiceClient client = GetDatabaseClient();

            if (!File.Exists(hostcDirectory + "PUT_EXEC.DAT"))
            {
                try
                {
                    client.HOGW_DeleteAllPutExec();
                }
                catch
                {
                    client.HOGW_DeleteAllPutExec();
                }

                return;
            }
            else
            {
                try
                {
                    client.HOGW_DeleteAllPutExec();
                }
                catch
                {
                    client.HOGW_DeleteAllPutExec();
                }

                StringBuilder xml = new StringBuilder();
                xml.Append("<ROOT>");

                using (MemoryStream ms = ReadAllBytes(hostcDirectory + "PUT_EXEC.DAT"))
                {
                    BinaryReader brdr = new BinaryReader(ms);

                    while (brdr.BaseStream.Position < brdr.BaseStream.Length)
                    {
                        long confirmNo = brdr.ReadInt64();
                        int stockNo = brdr.ReadInt16();
                        int vol = brdr.ReadInt32();
                        int price = brdr.ReadInt32() * 10;
                        string board = System.Text.Encoding.ASCII.GetString(brdr.ReadBytes(1)).Trim();

                        xml.AppendFormat("<PutExec ConfirmNo=\"{0}\" StockID=\"{1}\" Volume=\"{2}\" Price=\"{3}\" Board=\"{4}\"></PutExec>", confirmNo, "HO" + stockNo.ToString(), vol, price, board);
                    }
                }

                xml.Append("</ROOT>");

                try
                {
                    client.HOGW_InsertPutExec(xml.ToString());
                }
                catch
                {
                    client.HOGW_InsertPutExec(xml.ToString());
                }
            }
        }

        public static void ProcessFRoomInfo(string hostcDirectory)
        {
            List<int> stockNoList = new List<int>();
            List<int> stockNoList2 = new List<int>();
            List<double> basicFRoomList = new List<double>();
            List<double> currentFRoomList = new List<double>();
            List<double> buyForeignQuantityList = new List<double>();
            List<double> sellForeignQuantityList = new List<double>();

            int previousNo = -1;
            double previousCurrentRoom = 0;

            DBProxyServiceClient client = GetDatabaseClient();

            StringBuilder xml = new StringBuilder();
            xml.Append("<ROOT>");

            using (MemoryStream ms = ReadAllBytes(hostcDirectory + "FROOM.DAT"))
            {
                BinaryReader brdr = new BinaryReader(ms);
                while (brdr.BaseStream.Position < brdr.BaseStream.Length)
                {
                    int stockNo = brdr.ReadInt32();
                    double totalRoom = brdr.ReadDouble();
                    double currentRoom = brdr.ReadDouble();

                    //Them tu 1/6/2016
                    double buyVolume = brdr.ReadDouble();
                    double sellVolume = brdr.ReadDouble();

                    if (!stockNoList.Contains(stockNo))
                    {
                        stockNoList.Add(stockNo);
                        basicFRoomList.Add(currentRoom);
                    }
                    else if (!stockNoList2.Contains(stockNo))
                    {
                        stockNoList2.Add(stockNo);
                        currentFRoomList.Add(currentRoom);
                        buyForeignQuantityList.Add(buyVolume);
                        sellForeignQuantityList.Add(sellVolume);
                    }
                    else
                    {
                        int index = stockNoList2.IndexOf(stockNo);
                        currentFRoomList[index] = currentRoom;
                        buyForeignQuantityList[index] = buyVolume;
                        sellForeignQuantityList[index] = sellVolume;
                    }

                    //xml.AppendFormat("<FRoom StockID=\"{0}\" CurrentRoom=\"{1}\"></FRoom>", "HO" + stockNo.ToString(), currentRoom);
                    previousNo = stockNo;
                    previousCurrentRoom = currentRoom;
                }
            }
            for (int i = 0; i < stockNoList.Count; i++)
            {
                int stockID = stockNoList[i];
                double basicForeignRoom = basicFRoomList[i];

                

                int index = stockNoList2.IndexOf(stockID);
                if (index > -1)
                {
                    double currentForeignRoom = currentFRoomList[index];
                    double buyForeignQuantity = buyForeignQuantityList[index];
                    double sellForeignQuantity = sellForeignQuantityList[index];
                    //double buyForeignQuantity = basicForeignRoom - currentForeignRoom;
                    //xml.AppendFormat("<FRoom StockID=\"{0}\" CurrentRoom=\"{1}\" BuyForeignQuantity=\"{2}\"></FRoom>", "HO" + stockID.ToString(), currentForeignRoom, buyForeignQuantity);
                    xml.AppendFormat("<FRoom StockID=\"{0}\" CurrentRoom=\"{1}\" BuyForeignQuantity=\"{2}\" SellForeignQuantity=\"{3}\"></FRoom>", "HO" + stockID.ToString(), currentForeignRoom, buyForeignQuantity, sellForeignQuantity);
                }
                else
                {
                    xml.AppendFormat("<FRoom StockID=\"{0}\" CurrentRoom=\"{1}\" BuyForeignQuantity=\"{2}\" SellForeignQuantity=\"{3}\"></FRoom>", "HO" + stockID.ToString(), basicForeignRoom, 0, 0);
                }
            }
            xml.Append("</ROOT>");

            try
            {
                client.HOGW_UpdateCurrentFRoom(xml.ToString());
            }
            catch
            {
                client.HOGW_UpdateCurrentFRoom(xml.ToString());
            }
        }

        public static void ResetStockVolume()
        {
            DBProxyServiceClient client = GetDatabaseClient();

            try
            {
                client.HOGW_ResetStockVolume();
            }
            catch
            {
                client.HOGW_ResetStockVolume();
            }
        }
        /*
        public static void ProcessVN30Info(string vn30Directory)
        {
            MarketInfo market = new MarketInfo();

            using (MemoryStream ms = ReadAllBytes(vn30Directory + DateTime.Now.ToString("yyyyMMdd") + "_VN30.DAT"))
            {
                BinaryReader brdr = new BinaryReader(ms);
                //brdr.BaseStream.Seek(brdr.BaseStream.Length - 30, SeekOrigin.Begin);
                brdr.BaseStream.Seek(brdr.BaseStream.Length - 50, SeekOrigin.Begin);
                int index = brdr.ReadInt32();
                double totalShare = brdr.ReadDouble();
                double totalValue = brdr.ReadDouble();
                double totalSharePT = brdr.ReadDouble();
                double totalValuePT = brdr.ReadDouble();

                short advances = brdr.ReadInt16();
                short declines = brdr.ReadInt16();
                short nochange = brdr.ReadInt16();

                short ceiling = brdr.ReadInt16();
                short floor = brdr.ReadInt16();

                string time = brdr.ReadInt32().ToString();
                if (time.Length == 5) time = "0" + time;
                int seconds = int.Parse(time.Substring(4, 2));
                int minutes = int.Parse(time.Substring(2, 2));
                int hours = int.Parse(time.Substring(0, 2));

                market.Date = DateTime.Today.Add(new TimeSpan(hours, minutes, seconds));
                market.TotalValue = totalValue * 1000000;
                market.TotalVolume = totalShare;
                market.CurrentIndex = ((double)index) / 100;
                market.Advances = advances;
                market.NoChange = nochange;
                market.Declines = declines;
            }

            DBProxyServiceClient client = GetDatabaseClient();

            try
            {
                client.HOGW_UpdateVN30Info(market.Date, market.CurrentIndex, market.TotalVolume, market.TotalValue, market.Advances, market.NoChange, market.Declines);
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("ProcessVN30Info completed");
                Console.ResetColor();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error in ProcessVN30Info, detail:" + ex.Message);
                Console.ResetColor();
                client.HOGW_UpdateVN30Info(market.Date, market.CurrentIndex, market.TotalVolume, market.TotalValue, market.Advances, market.NoChange, market.Declines);
            }
        }
        */

        public static void ProcessHOSEIndicesIndo(string symbol, string hoseIndicesFile)
        {
            MarketInfo market = new MarketInfo();

            if (File.Exists(hoseIndicesFile))
            {
                using (MemoryStream ms = ReadAllBytes(hoseIndicesFile))
                {
                    BinaryReader brdr = new BinaryReader(ms);
                    //brdr.BaseStream.Seek(brdr.BaseStream.Length - 30, SeekOrigin.Begin);
                    brdr.BaseStream.Seek(brdr.BaseStream.Length - 50, SeekOrigin.Begin);
                    int index = brdr.ReadInt32();
                    double totalShare = brdr.ReadDouble();
                    double totalValue = brdr.ReadDouble();
                    double totalSharePT = brdr.ReadDouble();
                    double totalValuePT = brdr.ReadDouble();

                    short advances = brdr.ReadInt16();
                    short declines = brdr.ReadInt16();
                    short nochange = brdr.ReadInt16();

                    short ceiling = brdr.ReadInt16();
                    short floor = brdr.ReadInt16();

                    string time = brdr.ReadInt32().ToString();
                    if (time.Length == 5) time = "0" + time;
                    int seconds = int.Parse(time.Substring(4, 2));
                    int minutes = int.Parse(time.Substring(2, 2));
                    int hours = int.Parse(time.Substring(0, 2));

                    market.Symbol = symbol;
                    market.TotalVolumePT = totalSharePT;
                    market.TotalValuePT = totalValuePT;
                    market.Ceiling = ceiling;
                    market.Floor = floor;

                    market.Date = DateTime.Today.Add(new TimeSpan(hours, minutes, seconds));
                    market.TotalValue = totalValue * 1000000;
                    market.TotalVolume = totalShare;
                    market.CurrentIndex = ((double)index) / 100;
                    market.Advances = advances;
                    market.NoChange = nochange;
                    market.Declines = declines;
                }

                DBProxyServiceClient client = GetDatabaseClient();

                try
                {
                    client.HOGW_UpdateHOSEIndicesInfo(market.Symbol, market.Date, market.CurrentIndex, market.TotalVolume, market.TotalValue, market.TotalVolumePT, market.TotalValuePT, market.Advances, market.NoChange, market.Declines, market.Ceiling, market.Floor);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine("ProcessHOSEIndicesIndo '" + symbol + "' completed");
                    Console.ResetColor();
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in ProcessHOSEIndicesIndo'" + symbol + "', detail:" + ex.Message);
                    Console.ResetColor();
                    client.HOGW_UpdateHOSEIndicesInfo(market.Symbol, market.Date, market.CurrentIndex, market.TotalVolume, market.TotalValue, market.TotalVolumePT, market.TotalValuePT, market.Advances, market.NoChange, market.Declines, market.Ceiling, market.Floor);
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error in ProcessHOSEIndicesIndo, detail:File " + hoseIndicesFile + " does not exists.");
                Console.ResetColor();
            }
        }
    }
}
