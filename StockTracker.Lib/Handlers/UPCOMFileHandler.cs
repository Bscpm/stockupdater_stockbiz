using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Runtime.InteropServices;
using System.IO;
using System.Globalization;
using StockTracker.Lib.DBProxies;

namespace StockTracker.Lib.Handlers
{
    public class UPCOMFileHandler
    {
        public const string UPCOM_FLOOR_CODE = "02";
        public const int UPCOM_STOCK_TYPE = 2;
        private static DBProxyServiceClient _client = null;
        private static DBProxyServiceClient GetDatabaseClient()
        {
            if (_client == null || _client.State != System.ServiceModel.CommunicationState.Opened)
            {
                _client = new DBProxyServiceClient();
            }

            return _client;
        }

        public static void UpdateUPCOMSecuritiesList(string upcomDirectory)
        {
            DataSet ds = new DataSet();
            ds.ReadXml(upcomDirectory + "STSO_StocksInfo.XML", XmlReadMode.ReadSchema);

            DBProxyServiceClient client = GetDatabaseClient();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                if (row["FLOOR_CODE"].ToString() == UPCOM_FLOOR_CODE)
                {
                    int t = Convert.ToInt32(row["STOCK_TYPE"].ToString());
                    int type;

                    switch (t)
                    {
                        case 1:
                            type = 2;
                            break;

                        case 2:
                            type = 1;
                            break;

                        case 3:
                            type = 3;
                            break;

                        default:
                            type = t;
                            break;
                    }
                    try
                    {
                        client.UpdateNewSymbol("UC" + row["STOCK_ID"].ToString(), row["CODE"].ToString(), "HASTC", type);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error while UpdateUPCOMSecuritiesList, message=" + ex.Message);
                    }
                }
            }
        }

        public static void ProcessMarketInfo(string upcomDirectory, out string _status)
        {            
            DataSet ds = new DataSet();
            ds.ReadXml(upcomDirectory + "STSO_MarketInfo.XML", XmlReadMode.ReadSchema);
            MarketInfo market = null;
            double putthroughVolume = 0;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                if (row["FLOOR_CODE"].ToString() == UPCOM_FLOOR_CODE)
                {
                    market = new MarketInfo();
                    market.Symbol = "UPCOM";
                    market.TotalTrade = Convert.ToDouble(row["TOTAL_TRADE"].ToString());

                    market.Date = ((DateTime)row["TRADING_DATE"]).Add(TimeSpan.Parse(row["TIME"].ToString()));

                    market.TotalValue = Convert.ToDouble(row["TOTAL_VALUE"].ToString());
                    market.TotalVolume = Convert.ToDouble(row["TOTAL_QTTY"].ToString());
                    market.BasicIndex = Convert.ToDouble(row["PRIOR_MARKET_INDEX"].ToString());
                    market.CurrentIndex = Convert.ToDouble(row["MARKET_INDEX"].ToString());
                    //market.HighestIndex = Convert.ToDouble(row["HIGHEST_INDEX"].ToString());
                    //market.LowestIndex = Convert.ToDouble(row["LOWEST_INDEX"].ToString());
                    market.Advances = Convert.ToInt32(row["ADVANCES"].ToString());
                    market.NoChange = Convert.ToInt32(row["NOCHANGE"].ToString());
                    market.Declines = Convert.ToInt32(row["DECLINES"].ToString());
                    market.Status = row["CURRENT_STATUS"].ToString();
                    //putthroughVolume = Convert.ToDouble(row["PT_TOTAL_QTTY"].ToString());
                    
                    break;
                }
            }
            _status = market.Status;

            DBProxyServiceClient client = GetDatabaseClient();

            try
            {
                client.UPCOM_UpdateMarketInfo(market.Date, market.Status, market.BasicIndex, market.CurrentIndex, market.TotalTrade, market.TotalVolume, putthroughVolume, market.TotalValue, market.Advances, market.NoChange, market.Declines);
            }
            catch
            {
                client.UPCOM_UpdateMarketInfo(market.Date, market.Status, market.BasicIndex, market.CurrentIndex, market.TotalTrade, market.TotalVolume, putthroughVolume, market.TotalValue, market.Advances, market.NoChange, market.Declines);
            }
        }

        public static void ProcessStockInfo(string upcomDirectory)
        {
            DataSet ds = new DataSet();
            ds.ReadXml(upcomDirectory + "STSO_StocksInfo.XML", XmlReadMode.ReadSchema);

            StringBuilder xml = new StringBuilder();
            xml.Append("<ROOT>");
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                if (Convert.ToInt32(row["STOCK_TYPE"].ToString()) == UPCOM_STOCK_TYPE && row["FLOOR_CODE"].ToString() == UPCOM_FLOOR_CODE)
                {
                    StockInfo stock = new StockInfo();
                    stock.StockId = "UC" + row["STOCK_ID"].ToString();
                    stock.Symbol = row["CODE"].ToString();
                    stock.Date = ((DateTime)row["TRADING_DATE"]).Add(TimeSpan.Parse(row["TIME"].ToString()));

                    stock.PriceBasic = (row["BASIC_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(row["BASIC_PRICE"]);

                    stock.PriceCeiling = (row["CEILING_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(row["CEILING_PRICE"]);

                    stock.PriceFloor = (row["FLOOR_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(row["FLOOR_PRICE"]);

                    stock.PriceCurrent = (row["MATCH_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(row["MATCH_PRICE"]);

                    stock.PriceHigh = (row["HIGHEST_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(row["HIGHEST_PRICE"]);

                    stock.PriceLow = (row["LOWEST_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(row["LOWEST_PRICE"]);

                    stock.PriceOpen = (row["OPEN_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(row["OPEN_PRICE"]);

                    stock.PriceClose = (row["CLOSE_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(row["CLOSE_PRICE"]);

                    stock.PriceAverage = (row["AVERAGE_PRICE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(row["AVERAGE_PRICE"]);

                    stock.Volume = (row["MATCH_QTTY"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(row["MATCH_QTTY"]);

                    stock.TotalVolume = (row["AU_TOTAL_TRADED_QTTY"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(row["AU_TOTAL_TRADED_QTTY"]);

                    stock.TotalValue = (row["AU_TOTAL_TRADED_VALUE"] == DBNull.Value) ?
                                        0 : Convert.ToDouble(row["AU_TOTAL_TRADED_VALUE"]);

                    //stock.CurrentForeignRoom = Convert.ToDouble(row["REMAIN_FOREIGN_QTTY"].ToString(), CultureInfo.CurrentCulture.NumberFormat);
                    stock.CurrentForeignRoom = (row["REMAIN_FOREIGN_QTTY"] == DBNull.Value) ? 0 : Convert.ToDouble(row["REMAIN_FOREIGN_QTTY"]);

                    stock.BuyCount = (row["BID_COUNT"] == DBNull.Value) ?
                                      0 : Convert.ToDouble(row["BID_COUNT"]);
                    stock.SellCount = (row["OFFER_COUNT"] == DBNull.Value) ?
                                      0 : Convert.ToDouble(row["OFFER_COUNT"]);
                    stock.BuyQuantity = (row["TOTAL_BID_QTTY"] == DBNull.Value) ?
                                      0 : Convert.ToDouble(row["TOTAL_BID_QTTY"]);
                    stock.SellQuantity = (row["TOTAL_OFFER_QTTY"] == DBNull.Value) ?
                                      0 : Convert.ToDouble(row["TOTAL_OFFER_QTTY"]);
                    stock.BuyForeignQuantity = (row["BUY_FOREIGN_QTTY"] == DBNull.Value) ? 0 : Convert.ToDouble(row["BUY_FOREIGN_QTTY"]);
                    stock.SellForeignQuantity = (row["SELL_FOREIGN_QTTY"] == DBNull.Value) ? 0 : Convert.ToDouble(row["SELL_FOREIGN_QTTY"]);
                    stock.BuyForeignValue = (row["BUY_FOREIGN_VALUE"] == DBNull.Value) ? 0 : Convert.ToDouble(row["BUY_FOREIGN_VALUE"]);
                    stock.SellForeignValue = (row["SELL_FOREIGN_VALUE"] == DBNull.Value) ? 0 : Convert.ToDouble(row["SELL_FOREIGN_VALUE"]);

                    xml.AppendFormat("<Stock StockID=\"{0}\" Symbol=\"{1}\" Date=\"{2}\" PriceBasic=\"{3}\" PriceCeiling=\"{4}\" PriceFloor=\"{5}\" PriceCurrent=\"{6}\" Volume=\"{7}\" " +
                                        "PriceHigh=\"{8}\" PriceLow=\"{9}\" PriceOpen=\"{10}\" PriceClose=\"{11}\" PriceAverage=\"{12}\" TotalVolume=\"{13}\" TotalValue=\"{14}\" " +
                                        "CurrentForeignRoom=\"{15}\" BuyCount=\"{16}\" SellCount=\"{17}\" " +
                                        "BuyForeignQuantity=\"{18}\" SellForeignQuantity=\"{19}\" BuyForeignValue=\"{20}\" SellForeignValue=\"{21}\" BuyQuantity=\"{22}\" SellQuantity=\"{23}\" " +
                                        "></Stock>", stock.StockId, stock.Symbol, stock.Date.ToString("MM/dd/yyyy HH:mm:ss"),
                                        stock.PriceBasic.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceCeiling.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceFloor.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceCurrent.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.Volume.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceHigh.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceLow.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceOpen.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceClose.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.PriceAverage.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.TotalVolume.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.TotalValue.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.CurrentForeignRoom.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.BuyCount.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.SellCount.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.BuyForeignQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.SellForeignQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.BuyForeignValue.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.SellForeignValue.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.BuyQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat),
                                        stock.SellQuantity.ToString(CultureInfo.InvariantCulture.NumberFormat)
                                        );
                }
            }

            xml.Append("</ROOT>");

            DBProxyServiceClient client = GetDatabaseClient();

            try
            {
                client.UPCOM_UpdateStockPriceInfo(xml.ToString());
            }
            catch
            {
                client.UPCOM_UpdateStockPriceInfo(xml.ToString());
            }
        }

        public static void ProcessTopPriceInfo(string hastcDirectory)
        {
            DataSet ds = new DataSet();
            ds.ReadXml(hastcDirectory + "STSO_BESTPRICE.XML", XmlReadMode.ReadSchema);

            DBProxyServiceClient client = GetDatabaseClient();

            if (ds.Tables[0].Rows.Count == 0)
            {
                client.UPCOM_ResetStockTop3Price();
            }
            else
            {
                StringBuilder xml = new StringBuilder();
                xml.Append("<ROOT>");
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    xml.AppendFormat("<Stock StockID=\"{0}\" PriceBid1=\"{1}\" QuantityBid1=\"{2}\" PriceBid2=\"{3}\" QuantityBid2=\"{4}\" PriceBid3=\"{5}\" QuantityBid3=\"{6}\" " +
                                        "PriceOffer1=\"{7}\" QuantityOffer1=\"{8}\" PriceOffer2=\"{9}\" QuantityOffer2=\"{10}\" PriceOffer3=\"{11}\" QuantityOffer3=\"{12}\" " +
                                        "></Stock>", "UC" + row["STOCK_ID"].ToString(),
                                        Convert.ToDouble(row["B_ORD_PRICE_1"].ToString()),
                                        Convert.ToDouble(row["B_ORD_QTTY_1"].ToString()),
                                        Convert.ToDouble(row["B_ORD_PRICE_2"].ToString()),
                                        Convert.ToDouble(row["B_ORD_QTTY_2"].ToString()),
                                        Convert.ToDouble(row["B_ORD_PRICE_3"].ToString()),
                                        Convert.ToDouble(row["B_ORD_QTTY_3"].ToString()),
                                        Convert.ToDouble(row["S_ORD_PRICE_1"].ToString()),
                                        Convert.ToDouble(row["S_ORD_QTTY_1"].ToString()),
                                        Convert.ToDouble(row["S_ORD_PRICE_2"].ToString()),
                                        Convert.ToDouble(row["S_ORD_QTTY_2"].ToString()),
                                        Convert.ToDouble(row["S_ORD_PRICE_3"].ToString()),
                                        Convert.ToDouble(row["S_ORD_QTTY_3"].ToString()));


                }
                xml.Append("</ROOT>");

                try
                {
                    client.UPCOM_UpdateStockTop3Price(xml.ToString());
                }
                catch
                {
                    client.UPCOM_UpdateStockTop3Price(xml.ToString());
                }
            }
        }
    }
}
