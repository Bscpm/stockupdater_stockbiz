using StockTracker.Lib.DBProxies;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.ServiceModel;

namespace StockTracker.Lib.Handlers
{
	public class CommonHandler
	{
		private static List<DBProxyServiceClient> _clients;

		static CommonHandler()
		{
			CommonHandler._clients = new List<DBProxyServiceClient>();
		}

		public CommonHandler()
		{
		}

		protected static List<DBProxyServiceClient> GetDatabaseClients()
		{
			string item = ConfigurationManager.AppSettings["Server2"];
			lock (CommonHandler._clients)
			{
				if (CommonHandler._clients.Count != 0)
				{
					if (CommonHandler._clients[0].State != CommunicationState.Opened)
					{
						DBProxyServiceClient dBProxyServiceClient = new DBProxyServiceClient("CustomBinding_IDBProxyService", item);
						CommonHandler._clients.RemoveAt(0);
						CommonHandler._clients.Insert(0, dBProxyServiceClient);
					}
					if (CommonHandler._clients[1].State != CommunicationState.Opened)
					{
						DBProxyServiceClient dBProxyServiceClient1 = new DBProxyServiceClient();
						CommonHandler._clients.RemoveAt(1);
						CommonHandler._clients.Add(dBProxyServiceClient1);
					}
				}
				else
				{
					DBProxyServiceClient dBProxyServiceClient2 = new DBProxyServiceClient("CustomBinding_IDBProxyService", item);
					DBProxyServiceClient dBProxyServiceClient3 = new DBProxyServiceClient();
					CommonHandler._clients.Add(dBProxyServiceClient2);
					CommonHandler._clients.Add(dBProxyServiceClient3);
				}
			}
			return CommonHandler._clients;
		}
	}
}