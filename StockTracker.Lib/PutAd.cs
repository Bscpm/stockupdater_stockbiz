using System;
using System.Collections.Generic;
using System.Text;

namespace StockTracker.Lib
{
    public class PutAd
    {
        private string _stockId;

        public string StockId
        {
            get { return _stockId; }
            set { _stockId = value; }
        }

        private int _tradeId;

        public int TradeId
        {
            get { return _tradeId; }
            set { _tradeId = value; }
        }

        private double _volume;

        public double Volume
        {
            get { return _volume; }
            set { _volume = value; }
        }

        private double _price;

        public double Price
        {
            get { return _price; }
            set { _price = value; }
        }

        private int _firmNo;

        public int FirmNo
        {
            get { return _firmNo; }
            set { _firmNo = value; }
        }

        private string _side;

        public string Side
        {
            get { return _side; }
            set { _side = value; }
        }

        private string _board;

        public string Board
        {
            get { return _board; }
            set { _board = value; }
        }

        private DateTime _time;

        public DateTime Time
        {
            get { return _time; }
            set { _time = value; }
        }

        private string _flag;

        public string Flag
        {
            get { return _flag; }
            set { _flag = value; }
        }
    }
}
