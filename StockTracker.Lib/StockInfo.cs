using System;
using System.Collections.Generic;
using System.Text;

namespace StockTracker.Lib
{
    public class StockInfo
    {
        private string _stockId;

        public string StockId
        {
            get { return _stockId; }
            set { _stockId = value; }
        }

        public string Exchange { get; set; }

        private string _symbol;

        public string Symbol
        {
            get { return _symbol; }
            set { _symbol = value; }
        }

        public string TradingSessionID { get; set; }
        public string TradSesStatus { get; set; }
        

        private DateTime _date;

        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        private double _priceBasic;

        public double PriceBasic
        {
            get { return _priceBasic; }
            set { _priceBasic = value; }
        }

        private double _priceCeiling;

        public double PriceCeiling
        {
            get { return _priceCeiling; }
            set { _priceCeiling = value; }
        }

        private double _priceFloor;

        public double PriceFloor
        {
            get { return _priceFloor; }
            set { _priceFloor = value; }
        }

        private double _priceCurrent;

        public double PriceCurrent
        {
            get { return _priceCurrent; }
            set { _priceCurrent = value; }
        }

        private double _priceHigh;

        public double PriceHigh
        {
            get { return _priceHigh; }
            set { _priceHigh = value; }
        }

        private double _priceLow;

        public double PriceLow
        {
            get { return _priceLow; }
            set { _priceLow = value; }
        }

        private double _priceOpen;

        public double PriceOpen
        {
            get { return _priceOpen; }
            set { _priceOpen = value; }
        }

        private double _priceClose;

        public double PriceClose
        {
            get { return _priceClose; }
            set { _priceClose = value; }
        }

        private double _priceAverage;

        public double PriceAverage
        {
            get { return _priceAverage; }
            set { _priceAverage = value; }
        }

        private double _volume;

        public double Volume
        {
            get { return _volume; }
            set { _volume = value; }
        }

        private double _totalVolume;

        public double TotalVolume
        {
            get { return _totalVolume; }
            set { _totalVolume = value; }
        }

        private double _totalValue;

        public double TotalValue
        {
            get { return _totalValue; }
            set { _totalValue = value; }
        }

        private double _bid1Price;

        public double Bid1Price
        {
            get { return _bid1Price; }
            set { _bid1Price = value; }
        }

        private double _bid1Volume;

        public double Bid1Volume
        {
            get { return _bid1Volume; }
            set { _bid1Volume = value; }
        }

        private double _bid2Price;

        public double Bid2Price
        {
            get { return _bid2Price; }
            set { _bid2Price = value; }
        }

        private double _bid2Volume;

        public double Bid2Volume
        {
            get { return _bid2Volume; }
            set { _bid2Volume = value; }
        }

        private double _bid3Price;

        public double Bid3Price
        {
            get { return _bid3Price; }
            set { _bid3Price = value; }
        }

        private double _bid3Volume;

        public double Bid3Volume
        {
            get { return _bid3Volume; }
            set { _bid3Volume = value; }
        }

        private double _offer1Price;

        public double Offer1Price
        {
            get { return _offer1Price; }
            set { _offer1Price = value; }
        }

        private double _offer1Volume;

        public double Offer1Volume
        {
            get { return _offer1Volume; }
            set { _offer1Volume = value; }
        }

        private double _offer2Price;

        public double Offer2Price
        {
            get { return _offer2Price; }
            set { _offer2Price = value; }
        }

        private double _offer2Volume;

        public double Offer2Volume
        {
            get { return _offer2Volume; }
            set { _offer2Volume = value; }
        }

        private double _offer3Price;

        public double Offer3Price
        {
            get { return _offer3Price; }
            set { _offer3Price = value; }
        }

        private double _offer3Volume;

        public double Offer3Volume
        {
            get { return _offer3Volume; }
            set { _offer3Volume = value; }
        }

        private double _buyCount;

        public double BuyCount
        {
            get { return _buyCount; }
            set { _buyCount = value; }
        }

        private double _buyQuantity;

        public double BuyQuantity
        {
            get { return _buyQuantity; }
            set { _buyQuantity = value; }
        }

        private double _sellQuantity;

        public double SellQuantity
        {
            get { return _sellQuantity; }
            set { _sellQuantity = value; }
        }

        private double _sellCount;

        public double SellCount
        {
            get { return _sellCount; }
            set { _sellCount = value; }
        }

        private double _buyForeignQuantity;

        public double BuyForeignQuantity
        {
            get { return _buyForeignQuantity; }
            set { _buyForeignQuantity = value; }
        }

        private double _sellForeignQuantity;

        public double SellForeignQuantity
        {
            get { return _sellForeignQuantity; }
            set { _sellForeignQuantity = value; }
        }

        private double _buyForeignValue;

        public double BuyForeignValue
        {
            get { return _buyForeignValue; }
            set { _buyForeignValue = value; }
        }

        private double _sellForeignValue;

        public double SellForeignValue
        {
            get { return _sellForeignValue; }
            set { _sellForeignValue = value; }
        }

        private double _currentForeignRoom;

        public double CurrentForeignRoom
        {
            get { return _currentForeignRoom; }
            set { _currentForeignRoom = value; }
        }


        //
        // Gia mua tot nhat chi tinh voi khop lenh lo chan
        //
        public double BestBidPrice { get; set; }
        //
        // KL mua tot nhat
        //
        public double BestBidQtty { get; set; }
        //
        // Gia ban tot nhat
        //
        public double BestOfferPrice { get; set; }
        /// <summary>
        /// KL ban tot nhat
        /// </summary>
        public double BestOfferQtty { get; set; }
        /// <summary>
        /// Tong KL chao mua cong don
        /// </summary>
        //public double TotalBidQtty { get; set; }
        /// <summary>
        /// Tong KL chao ban cong don
        /// </summary>
        //public double TotalOfferQtty { get; set; }
        /// <summary>
        /// Gia mo cua phien giao dich truoc
        /// </summary>
        public double PriorOpenPrice { get; set; }
        /// <summary>
        /// Gia dong cua phien GD truoc
        /// </summary>
        public double PriorClosePrice { get; set; }
        /// <summary>
        /// Phien giao dich thu (ke tu ngay niem yet)
        /// </summary>
        public int DateNo { get; set; }
        /// <summary>
        /// KL dieu chinh
        /// </summary>
        public double AdjustQtty { get; set; }
        /// <summary>
        /// Trang thai thuc hien quyen anh huong toi gia CK
        /// 0: Khong xay ra
        /// 1: Tra CT bang tien
        /// 2: Tra co tuc bang CP/CP thuong
        /// 3: Phat hanh CP cho co dong hien huu
        /// 4: Tra co tuc bang CP/CP thuong, phat hanh CP cho co dong hien huu
        /// 5: Tra co tuc bang tien, bang CP/CP thuong, phat hanh CP cho co dong hien huu
        /// 6: Niem yet bo sung
        /// 7: Giam von
        /// 8: Tra co tuc bang tien, tra co tuc bang CP/CP thuong
        /// 9: Tra co tuc bang tien, phat hanh CP cho co dong hien huu
        /// 10: Thay doi ty le Free Float
        /// 11: Hop dai hoi co dong
        /// </summary>
        public int ReferenceStatus { get; set; }
        /// <summary>
        /// Ty le dieu chinh khi ReferenceStatus!=0
        /// </summary>
        public double AdjustRate { get; set; }
        /// <summary>
        /// Ty le co tuc/CP
        /// </summary>
        public double? DividendRate { get; set; }
        /// <summary>
        /// Gia thuc hien cao nhat (gia khop lenh cua ca 2 hinh thuc)
        /// </summary>
        public double HighestPrice { get; set; }
        /// <summary>
        /// Gia thuc hien thap nhat (gia khop lenh cua ca 2 hinh thuc)
        /// </summary>
        public double LowestPrice { get; set; }
        /// <summary>
        /// Gia khop lenh cua phien truoc do (chi tinh voi khop lenh thong thuong)
        /// </summary>
        public double PriorPrice { get; set; }
        /// <summary>
        /// Gia tri khop
        /// </summary>
        public double MatchValue { get; set; }
        /// <summary>
        /// Tong so lenh dat ban cho khop
        /// </summary>
        public int OfferCount { get; set; }
        /// <summary>
        /// Tong so lenh dat mua cho khop
        /// </summary>
        public int BidCount { get; set; }
        /// <summary>
        /// Tong KL giao dich thong thuong
        /// </summary>
        public double NM_TotalTradedQtty { get; set; }
        /// <summary>
        /// Tong GT giao dich thong thuong
        /// </summary>
        public double NM_TotalTradedValue { get; set; }
        /// <summary>
        /// KL thuc hien cua GD thoa thuan
        /// </summary>
        public double PT_MatchQtty { get; set; }
        /// <summary>
        /// Gia thuc hien cua GD thoa thuan
        /// </summary>
        public double PT_MatchPrice { get; set; }
        /// <summary>
        /// Tong KL cua GDTT
        /// </summary>
        public double PT_TotalTradedQtty { get; set; }
        /// <summary>
        /// Tong GT cua GDTT
        /// </summary>
        public double PT_TotalTradedValue { get; set; }
        /// <summary>
        /// Ngay dao han cua trai phieu
        /// </summary>
        public DateTime? MaturityDate { get; set; }
        /// <summary>
        /// Lai suat cua trai phie
        /// </summary>
        public double? CouponRate { get; set; }
        /// <summary>
        /// Tong so luong chung khoan da mua cua ca 2 hinh thuc giao dich
        /// </summary>
        public double BoughtQuantity { get; set; }
        /// <summary>
        /// Tong so lenh da mua
        /// </summary>
        public double BoughtCount { get; set; }
        /// <summary>
        /// Tong gia tri mua cua ca 2 hinh thuc giao dich
        /// </summary>
        public double BoughtValue { get; set; }
        /// <summary>
        /// Tong so luong chung khoan da ban cua ca 2 hinh thuc giao dich
        /// </summary>
        public double SoldQuantity { get; set; }
        /// <summary>
        /// Tong so lenh da ban
        /// </summary>
        public double SoldCount { get; set; }
        /// <summary>
        /// Tong gia tri ban cua ca 2 hinh thuc giao dich
        /// </summary>
        public double SoldValue { get; set; }
    }
}
