using System;
using System.Collections.Generic;
using System.Text;

namespace StockTracker.Lib
{
    public class PutExec
    {
        private string _stockId;

        public string StockId
        {
            get { return _stockId; }
            set { _stockId = value; }
        }

        private int _confirmNo;

        public int ConfirmNo
        {
            get { return _confirmNo; }
            set { _confirmNo = value; }
        }

        private double _volume;

        public double Volume
        {
            get { return _volume; }
            set { _volume = value; }
        }

        private double _price;

        public double Price
        {
            get { return _price; }
            set { _price = value; }
        }

        private string _board;

        public string Board
        {
            get { return _board; }
            set { _board = value; }
        }
    }
}
