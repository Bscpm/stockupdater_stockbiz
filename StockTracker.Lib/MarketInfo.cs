using System;
using System.Collections.Generic;
using System.Text;

namespace StockTracker.Lib
{
    public class MarketInfo
    {
        private string  _symbol;

        public string  Symbol
        {
            get { return _symbol; }
            set { _symbol = value; }
        }

        private DateTime _date;

        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        private double _totalTrade;

        public double TotalTrade
        {
            get { return _totalTrade; }
            set { _totalTrade = value; }
        }

        private double _totalValue;

        public double TotalValue
        {
            get { return _totalValue; }
            set { _totalValue = value; }
        }

        private double _totalVolume;

        public double TotalVolume
        {
            get { return _totalVolume; }
            set { _totalVolume = value; }
        }

        private double _currentIndex;

        public double CurrentIndex
        {
            get { return _currentIndex; }
            set { _currentIndex = value; }
        }

        private double _highestIndex;

        public double HighestIndex
        {
            get { return _highestIndex; }
            set { _highestIndex = value; }
        }

        private double _lowestIndex;

        public double LowestIndex
        {
            get { return _lowestIndex; }
            set { _lowestIndex = value; }
        }

        private double _basicIndex;

        public double BasicIndex
        {
            get { return _basicIndex; }
            set { _basicIndex = value; }
        }

        private int _advances;

        public int Advances
        {
            get { return _advances; }
            set { _advances = value; }
        }

        private int _nochange;

        public int NoChange
        {
            get { return _nochange; }
            set { _nochange = value; }
        }

        private int _declines;

        public int Declines
        {
            get { return _declines; }
            set { _declines = value; }
        }

        private string _status;

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public double TotalVolumePT { get; set; }
        public double TotalValuePT { get; set; }
        public int Ceiling { get; set; }
        public int Floor { get; set; }
    }
}
