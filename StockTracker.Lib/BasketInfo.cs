﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockTracker.Lib
{
    public class BasketInfo
    {
        public string IndexCode { get; set; }
        public string Symbol { get; set; }
        public long TotalQtty { get; set; }
        public double Weighted { get; set; }
        public DateTime AddDate { get; set; }
    }
}
