﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockTracker.Lib
{
    public class SymbolInfo
    {
        public string SourceID { get; set; }
        public string Symbol { get; set; }
        public string BoardCode { get; set; }
        public string SecurityTradingStatus { get; set; }
        /// <summary>
        /// Loai CK
        /// ST: Co phieu
        /// BO: Trai phieu
        /// MF: Chung chi quy
        /// EF: ETF
        /// FU: Future
        /// OP: Option
        /// </summary>
        public string SecurityType { get; set; }
        /// <summary>
        /// Ngay phat hanh
        /// </summary>
        public DateTime IssueDate { get; set; }
        /// <summary>
        /// To chuc phat hanh
        /// </summary>
        public string Issuer { get; set; }
        /// <summary>
        /// Mo ta them ve CK
        /// </summary>
        public string SecurityDesc { get; set; }
        public double ParValue { get; set; }
        public int TradingUnit { get; set; }
        /// <summary>
        /// KL niem yet
        /// </summary>
        public double TotalListingQtty { get; set; }
    }
}
