using System;
using System.Collections.Generic;
using System.Text;

namespace StockTracker.Lib
{
    public class OrderInfo
    {
        private string _stockId;

        public string StockId
        {
            get { return _stockId; }
            set { _stockId = value; }
        }

        private string _orderNo;

        public string OrderNo
        {
            get { return _orderNo; }
            set { _orderNo = value; }
        }

        private DateTime _date;

        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        private int _orderType;

        public int OrderType
        {
            get { return _orderType; }
            set { _orderType = value; }
        }

        private int _transactionType;

        public int TransactionType
        {
            get { return _transactionType; }
            set { _transactionType = value; }
        }

        private bool _isForeignOrder;

        public bool IsForeignOrder
        {
            get { return _isForeignOrder; }
            set { _isForeignOrder = value; }
        }
	

        private double _price;

        public double Price
        {
            get { return _price; }
            set { _price = value; }
        }

        private double _quantity;

        public double Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }					
    }
}
