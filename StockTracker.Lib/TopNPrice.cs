﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockTracker.Lib
{
    public class TopNPrice
    {
        public string Symbol { get; set; }
        public int Top { get; set; }
        public int NumTopPrice { get; set; }
        public double BuyPrice { get; set; }
        public double BuyQtty{ get; set; }
        public double SellPrice { get; set; }
        public double SellQtty { get; set; }
        
    }
}
