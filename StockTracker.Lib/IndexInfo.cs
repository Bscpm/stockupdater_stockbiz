using System;
using System.Collections.Generic;
using System.Text;

namespace StockTracker.Lib
{
    public class IndexInfo
    {
        public string IndexCode { get; set; }
        public double CurrentValue { get; set; }
        public double Change { get; set; }
        public double RatioChange { get; set; }
        public double TotalQtty { get; set; }
        public double TotalValue { get; set; }
        public string CurrentStatus { get; set; }
        public int TotalStock { get; set; }
        public double PriorIndexVal { get; set; }
        public double HighestIndex { get; set; }
        public double LowestIndex { get; set; }
        public double CloseIndex { get; set; }
        public double TypeIndex { get; set; }
        public string IndexName { get; set; }
        public DateTime Date { get; set; }
    }
}
