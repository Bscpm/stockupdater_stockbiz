using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using StockTracker.Lib;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using StockTracker.Lib.Handlers;

namespace StockTracker
{
    class Program
    {
        #region Declare

        private static DateTime _lastTimeUpdateNewStockHA = DateTime.Now;
        static bool b_updatedNewStockHO = false;
        static bool b_updatedNewStockHA = false;
        static bool b_updatedNewStockUC = false;


        private static TimeSpan T1 = new TimeSpan(9, 15, 0);
        private static TimeSpan T2 = new TimeSpan(13, 45, 0);
        private static TimeSpan T3 = new TimeSpan(14, 0, 0);
        private static TimeSpan T4 = new TimeSpan(14, 15, 0);

        private static string _hastcDirectory;
        private static string _hostcDirectory;
        private static string _hoseIndicesDirectory;
        private static string _upcomDirectory;
        private static int _hastcPolling;
        private static int _hostcPolling;
        private static int _upcomPolling;

        private static string _lastHOMarketStat = null;


        #endregion

        #region Main

        static void Main(string[] args)
        {
            InitializeMonitoringService();
            Console.Read();
        }

        /// <summary>
        /// InitializeMonitoringService
        /// </summary>
        private static void InitializeMonitoringService()
        {
            _hastcDirectory = ConfigurationManager.AppSettings["HASTCDirectory"].EndsWith("/") ? ConfigurationManager.AppSettings["HASTCDirectory"] : ConfigurationManager.AppSettings["HASTCDirectory"] + "/";
            _upcomDirectory = ConfigurationManager.AppSettings["UPCOMDirectory"].EndsWith("/") ? ConfigurationManager.AppSettings["UPCOMDirectory"] : ConfigurationManager.AppSettings["UPCOMDirectory"] + "/";
            _hoseIndicesDirectory = ConfigurationManager.AppSettings["HOSEIndicesDirectory"].EndsWith("/") ? ConfigurationManager.AppSettings["HOSEIndicesDirectory"] : ConfigurationManager.AppSettings["HOSEIndicesDirectory"] + "/";
            _hastcPolling = int.Parse(ConfigurationManager.AppSettings["HASTCPollingFrequency"]);
            _hostcPolling = int.Parse(ConfigurationManager.AppSettings["HOSTCPollingFrequency"]);
            _upcomPolling = int.Parse(ConfigurationManager.AppSettings["UPCOMPollingFrequency"]);

            string infoshowVersion = ConfigurationManager.AppSettings["HASTCInfoshowVersion"];
            string upcomInfoshowVersion = ConfigurationManager.AppSettings["UPCOMInfoshowVersion"];

            System.Threading.Thread hastcThread = null;

            if (infoshowVersion == "3")
            {
                Console.WriteLine("HAHandler connect to Infoshow 3");
                hastcThread = new System.Threading.Thread(new System.Threading.ThreadStart(MonitorHASTCData));
            }
            else if (infoshowVersion == "5DB")
            {
                Console.WriteLine("HAHandler connect to Infoshow 5 DB");
                hastcThread = new System.Threading.Thread(new System.Threading.ThreadStart(MonitorHASTCData_IF5DB));
            }
            else if (infoshowVersion == "5FILE")
            {
                Console.WriteLine("HAHandler connect to Infoshow 5 File");
                hastcThread = new System.Threading.Thread(new System.Threading.ThreadStart(MonitorHASTCData_IF5File));
            }

            System.Threading.Thread hostcThread = new System.Threading.Thread(new System.Threading.ThreadStart(MonitorHOSTCData));

            System.Threading.Thread upcomThread = null;
            if (upcomInfoshowVersion == "5DB")
            {
                Console.WriteLine("UPCOMHandler connect to Infoshow 5 DB");
                upcomThread = new System.Threading.Thread(new System.Threading.ThreadStart(MonitorUPCOMData_IF5DB));
            }
            else
            {
                Console.WriteLine("UPCOMHandler connect to file");
                upcomThread = new System.Threading.Thread(new System.Threading.ThreadStart(MonitorUPCOMData));
            }

            hostcThread.Start();
            hastcThread.Start();
            upcomThread.Start();
        }

        #endregion

        #region HOSTC

        private static DateTime _lastProcessHOMarketStatTime = DateTime.MinValue;

        /// <summary>
        /// GetHOMarketStat
        /// </summary>
        /// <returns>Market Status</returns>
        private static string GetHOMarketStat()
        {
            try
            {
                FileInfo file = new FileInfo(_hostcDirectory + "MARKET_STAT.DAT");
                if (file.LastWriteTime > _lastProcessHOMarketStatTime && file.LastWriteTime.Date == DateTime.Today.Date && file.LastWriteTime.Month == DateTime.Today.Month && file.LastWriteTime.Year == DateTime.Today.Year)
                {
                    string status;
                    TimeSpan time;
                    HOSEFileHandler.GetMarketStat(_hostcDirectory, out status, out time);

                    _lastProcessHOMarketStatTime = file.LastWriteTime;
                    _lastHOMarketStat = status;

                    return _lastHOMarketStat;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("Error Processing HOSTC Market Stat File: {0}", ex.Message));
            }
            return _lastHOMarketStat;
        }

        /// <summary>
        /// Update NewStockHO
        /// </summary>
        /// <param name="status">MarketStatus</param>
        private static void UpdateNewStockHO(string status)
        {
            // Update new stock
            try
            {
                if (status == "P" || status == "O")
                {
                    if (!b_updatedNewStockHO)
                    {
                        HOSEFileHandler.UpdateSecuritiesList((ConfigurationManager.AppSettings["HOSTCDirectory"].EndsWith("/") ? ConfigurationManager.AppSettings["HOSTCDirectory"] : ConfigurationManager.AppSettings["HOSTCDirectory"] + "/")
                                + "BACKUP" + DateTime.Today.Day.ToString("00") + "/");
                        b_updatedNewStockHO = true;
                    }
                }
                else if (status == "C" || status == "K" || status == "J")
                {
                    b_updatedNewStockHO = false;
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error Processing Securities File, message=" + ex.Message);
                Console.ResetColor();
            }
        }

        /// <summary>
        /// MonitorHOSTCData
        /// </summary>
        private static void MonitorHOSTCData()
        {
            DateTime lastDay = DateTime.MinValue;

            while (true)
            {
                try
                {
                    HOSEFileHandler.IsNewDay = false;

                    _hostcDirectory = (ConfigurationManager.AppSettings["HOSTCDirectory"].EndsWith("\\") ? ConfigurationManager.AppSettings["HOSTCDirectory"] : ConfigurationManager.AppSettings["HOSTCDirectory"] + "\\")
                        + "BACKUP" + DateTime.Today.Day.ToString("00") + "\\";

                    bool runAnytime = ConfigurationManager.AppSettings["RunAnytime"] == "true" ? true : false;

                    string status = GetHOMarketStat();

                    // Update new stocks
                    UpdateNewStockHO(status);

                    if (runAnytime || ((DateTime.Now.Hour >= 8 && DateTime.Now.Hour <= 10) || status == "P" || status == "O" || status == "A" || status == "C" || status == "K" || status == "I" || status == "F"))
                    {
                        if (ProcessHOMarketInfo(status))
                        {
                            // Update is new day status
                            if (DateTime.Today.Date != lastDay)
                            {
                                HOSEFileHandler.IsNewDay = true;
                                lastDay = DateTime.Today.Date;
                            }
                            ProcessHOStockInfo(status);
                            ProcessHOFRoomInfo();
                            ProcessHOLEInfo(status);
                            ProcessHOPutInfo();
                            ProcessHOSEIndicesIndo();
                        }
                    }

                    ProcessHONewListDeList();

                    System.Threading.Thread.Sleep(_hostcPolling);
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error while MonitorHOSTCData, message=" + ex.Message);
                    Console.ResetColor();
                }
            }
        }

        private static DateTime _lastProcessHOMarketInfoTime = DateTime.MinValue;

        /// <summary>
        /// Process Market information file
        /// </summary>
        /// <returns>Market status</returns>
        private static bool ProcessHOMarketInfo(string status)
        {
            try
            {
                //FileInfo file = new FileInfo(_hostcDirectory + "TOTALMKT.DAT");
                FileInfo file = new FileInfo((ConfigurationManager.AppSettings["HOSTCDirectory"].EndsWith("\\") ? ConfigurationManager.AppSettings["HOSTCDirectory"] : ConfigurationManager.AppSettings["HOSTCDirectory"] + "\\") + "INDEX\\" + DateTime.Today.ToString("yyyyMMdd") + "_" + "VNINDEX.DAT");

                if (status == null || status == "J")
                {
                    HOSEFileHandler.ResetAllMarketInfo();
                    return true;
                }

                if (file.LastWriteTime > _lastProcessHOMarketInfoTime)
                {
                    MarketInfo marketInfo = HOSEFileHandler.ProcessMarketInfo(ConfigurationManager.AppSettings["HOSTCDirectory"], status);
                    if (marketInfo == null)
                    {
                        return true;
                    }
                    TimeSpan ts = marketInfo.Date.TimeOfDay;

                    if (HOSEFileHandler.IsNewDay)
                    {
                        ResetHO3SessionsInfo();
                    }

                    ProcessHO3SessionsInfo();

                    _lastProcessHOMarketInfoTime = file.LastWriteTime;

                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("Error Processing HOSTC Market Information File: {0}", ex.Message));
                Console.ResetColor();
                return false;
            }
            return true;
        }

        private static DateTime _lastProcessVN30InfoTime = DateTime.MinValue;
        private static DateTime _lastProcessVN100InfoTime = DateTime.MinValue;
        private static DateTime _lastProcessVNMIDInfoTime = DateTime.MinValue;
        private static DateTime _lastProcessVNSMLInfoTime = DateTime.MinValue;
        private static DateTime _lastProcessVNALLInfoTime = DateTime.MinValue;
        private static DateTime _lastProcessVNXALLInfoTime = DateTime.MinValue;
        

        /// <summary>
        /// Process VN30 information file
        /// </summary>
        /// <returns>Market status</returns>
        private static bool ProcessHOSEIndicesIndo()
        {
            try
            {
                #region VN30
                FileInfo file = new FileInfo(_hoseIndicesDirectory + DateTime.Now.ToString("yyyyMMdd") + "_VN30.DAT");

                if (file.LastWriteTime > _lastProcessVN30InfoTime)
                {
                    HOSEFileHandler.ProcessHOSEIndicesIndo("VN30", file.FullName);
                    _lastProcessVN30InfoTime = file.LastWriteTime;

                }
                #endregion

                #region VN100
                file = new FileInfo(_hoseIndicesDirectory + DateTime.Now.ToString("yyyyMMdd") + "_VN100.DAT");

                if (file.LastWriteTime > _lastProcessVN100InfoTime)
                {
                    HOSEFileHandler.ProcessHOSEIndicesIndo("VN100", file.FullName);
                    _lastProcessVN100InfoTime = file.LastWriteTime;

                }
                #endregion

                #region VNMID
                file = new FileInfo(_hoseIndicesDirectory + DateTime.Now.ToString("yyyyMMdd") + "_VNMID.DAT");

                if (file.LastWriteTime > _lastProcessVNMIDInfoTime)
                {
                    HOSEFileHandler.ProcessHOSEIndicesIndo("VNMID", file.FullName);
                    _lastProcessVNMIDInfoTime = file.LastWriteTime;

                }
                #endregion

                #region VNSML
                file = new FileInfo(_hoseIndicesDirectory + DateTime.Now.ToString("yyyyMMdd") + "_VNSML.DAT");

                if (file.LastWriteTime > _lastProcessVNSMLInfoTime)
                {
                    HOSEFileHandler.ProcessHOSEIndicesIndo("VNSML", file.FullName);
                    _lastProcessVNSMLInfoTime = file.LastWriteTime;

                }
                #endregion

                #region VNALL
                file = new FileInfo(_hoseIndicesDirectory + DateTime.Now.ToString("yyyyMMdd") + "_VNALL.DAT");

                if (file.LastWriteTime > _lastProcessVNALLInfoTime)
                {
                    HOSEFileHandler.ProcessHOSEIndicesIndo("VNALL", file.FullName);
                    _lastProcessVNALLInfoTime = file.LastWriteTime;

                }
                #endregion

                #region VNALLShare (VNXALL)
                file = new FileInfo(_hoseIndicesDirectory + DateTime.Now.ToString("yyyyMMdd") + "_VNXALL.DAT");

                if (file.LastWriteTime > _lastProcessVNXALLInfoTime)
                {
                    HOSEFileHandler.ProcessHOSEIndicesIndo("VNXALL", file.FullName);
                    _lastProcessVNXALLInfoTime = file.LastWriteTime;

                }
                #endregion
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("Error Processing HOSE Indices Files: {0}", ex.Message));
                Console.ResetColor();
                return false;
            }
            return true;
        }

        private static bool ProcessHO3SessionsInfo()
        {
            try
            {
                HOSEFileHandler.Process3SessionsInfo(ConfigurationManager.AppSettings["HOSTCDirectory"].EndsWith("\\") ? ConfigurationManager.AppSettings["HOSTCDirectory"] : ConfigurationManager.AppSettings["HOSTCDirectory"] + "\\");
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("Error Processing 3 Sessions Information: {0}", ex.Message));
                Console.ResetColor();

                return false;
            }

            return true;
        }

        private static bool ResetHO3SessionsInfo()
        {
            try
            {
                HOSEFileHandler.Reset3SessionsInfo();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("Error Reseting 3 Sessions Information: {0}", ex.Message));
                Console.ResetColor();
                return false;
            }

            return true;
        }

        private static DateTime _lastProcessHOStockInfoTime = DateTime.MinValue;

        private static void ProcessHOStockInfo(string status)
        {
            FileInfo file = new FileInfo(_hostcDirectory + "SECURITY.DAT");

            if (file.LastWriteTime > _lastProcessHOStockInfoTime)
            {
                try
                {
                    HOSEFileHandler.ProcessStockInfo(_hostcDirectory, status);

                    _lastProcessHOStockInfoTime = file.LastWriteTime;
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(string.Format("Error Processing HOSTC Stocks Information File: {0}", ex.Message));
                    Console.ResetColor();
                }
            }
        }

        private static DateTime _lastProcessHOLEInfoTime = DateTime.MinValue;

        private static void ProcessHOLEInfo(string status)
        {
            try
            {
                FileInfo file = new FileInfo(_hostcDirectory + "LE.DAT");
                if (!file.Exists)
                {
                    HOSEFileHandler.ResetLEInfoOffset();
                }
                else
                {
                    if (file.LastWriteTime > _lastProcessHOLEInfoTime)
                    {
                        HOSEFileHandler.ProcessLEInfo(_hostcDirectory, status);
                        _lastProcessHOLEInfoTime = file.LastWriteTime;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("Error Processing HOSTC LE Information File: {0}", ex.Message));
                Console.ResetColor();
            }
        }

        private static DateTime _lastProcessHOFRoomInfoTime = DateTime.MinValue;

        private static void ProcessHOFRoomInfo()
        {
            try
            {
                FileInfo file = new FileInfo(_hostcDirectory + "FROOM.DAT");

                if (file.LastWriteTime > _lastProcessHOFRoomInfoTime)
                {
                    HOSEFileHandler.ProcessFRoomInfo(_hostcDirectory);
                    _lastProcessHOFRoomInfoTime = file.LastWriteTime;
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("Error Processing HOSTC Foreign Room Information File: {0}", ex.Message));
                Console.ResetColor();
            }
        }

        private static DateTime _lastProcessHONewListTime = DateTime.MinValue;
        private static DateTime _lastProcessHODeListTime = DateTime.MinValue;
        private static void ProcessHONewListDeList()
        {
            FileInfo file = new FileInfo(_hostcDirectory + "NEWLIST.DAT");

            if (file.Exists)
            {
                if (file.LastWriteTime > _lastProcessHONewListTime)
                {
                    try
                    {
                        HOSEFileHandler.ProcessNewList(_hostcDirectory);
                        _lastProcessHONewListTime = file.LastWriteTime;
                    }
                    catch (Exception ex)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(string.Format("Error Processing HOSTC New List File: {0}", ex.Message));
                        Console.ResetColor();
                    }
                }
            }

            file = new FileInfo(_hostcDirectory + "DELIST.DAT");
            if (file.Exists)
            {
                if (file.LastWriteTime > _lastProcessHODeListTime)
                {
                    try
                    {
                        HOSEFileHandler.ProcessDeList(_hostcDirectory);
                        _lastProcessHODeListTime = file.LastWriteTime;
                    }
                    catch (Exception ex)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(string.Format("Error Processing HOSTC DeList File: {0}", ex.Message));
                        Console.ResetColor();
                    }
                }
            }
        }

        private static DateTime _lastProcessHOPutAdTime = DateTime.MinValue;
        private static DateTime _lastProcessHOPutExecTime = DateTime.MinValue;

        private static void ProcessHOPutInfo()
        {
            FileInfo file = new FileInfo(_hostcDirectory + "PUT_AD.DAT");

            if (!file.Exists)
            {
                try
                {
                    HOSEFileHandler.ProcessPutAd(_hostcDirectory);
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(string.Format("Error Processing HOSTC Put Ad File: {0}", ex.Message));
                    Console.ResetColor();
                }
            }
            else
            {
                if (file.LastWriteTime > _lastProcessHOPutAdTime)
                {
                    try
                    {
                        HOSEFileHandler.ProcessPutAd(_hostcDirectory);
                        _lastProcessHOPutAdTime = file.LastWriteTime;
                    }
                    catch (Exception ex)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(string.Format("Error Processing HOSTC Put Ad File: {0}", ex.Message));
                        Console.ResetColor();
                    }
                }
            }

            file = new FileInfo(_hostcDirectory + "PUT_EXEC.DAT");

            if (!file.Exists)
            {
                try
                {
                    HOSEFileHandler.ProcessPutExec(_hostcDirectory);
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(string.Format("Error Processing HOSTC Put Exec File: {0}", ex.Message));
                    Console.ResetColor();
                }
            }
            else
            {
                if (file.LastWriteTime > _lastProcessHOPutExecTime)
                {
                    try
                    {
                        HOSEFileHandler.ProcessPutExec(_hostcDirectory);
                        _lastProcessHOPutExecTime = file.LastWriteTime;
                    }
                    catch (Exception ex)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(string.Format("Error Processing HOSTC Put Exec File: {0}", ex.Message));
                        Console.ResetColor();
                    }
                }
            }
        }

        #endregion

        #region HOSTC Gateway



        #endregion

        #region HASTC Infoshow 5 Database

        private static void UpdateNewStockHA_IF5DB(string status)
        {
            // Update new stock
            try
            {
                if (status != "13")
                {
                    if (DateTime.Now.Hour == 8 && _lastTimeUpdateNewStockHA.AddMinutes(7) < DateTime.Now)
                    {
                        // Call UpdateHASTCSecuritiesList every 7 minutes
                        HNXIF5DatabaseHandler.UpdateHASTCSecuritiesList();
                        _lastTimeUpdateNewStockHA = DateTime.Now;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error in UpdateNewStockHA_IF5DB, message=" + ex.Message);
                Console.ResetColor();
            }
        }

        private static void MonitorHASTCData_IF5DB()
        {
            while (true)
            {
                DateTime startDate = DateTime.Now;

                bool runAnytime = ConfigurationManager.AppSettings["RunAnytime"] == "true" ? true : false;
                
                string status = null;

                if (runAnytime || (DateTime.Now.Hour >= 8 && DateTime.Now.Hour <= 15 && DateTime.Now.DayOfWeek != DayOfWeek.Saturday && DateTime.Now.DayOfWeek != DayOfWeek.Sunday))
                {
                    status = ProcessHAMarketInfo_IF5DB();
                }

                if (runAnytime || (status != null && status != "13") || (DateTime.Now.Hour >= 8 && DateTime.Now.Hour <= 15 && DateTime.Now.DayOfWeek != DayOfWeek.Saturday && DateTime.Now.DayOfWeek != DayOfWeek.Sunday))
                {
                    UpdateNewStockHA_IF5DB(status);
                    ProcessHAStockInfo_IF5DB();
                    ProcessHATopPriceInfo_IF5DB();
                    ProcessHAPutAdInfo_IF5DB();
                }

                DateTime endDate = DateTime.Now;

                //Console.WriteLine("Processed Time: {0}", endDate.Subtract(startDate));

                System.Threading.Thread.Sleep(_hastcPolling);
            }
        }

        private static string ProcessHAMarketInfo_IF5DB()
        {
            try
            {
                string status;
                HNXIF5DatabaseHandler.ProcessMarketInfo(out status);
                return status;
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("Error in ProcessHAMarketInfo_IF5DB, detail: {0}", ex.Message));
                Console.ResetColor();
                return null;
            }
        }

        private static void ProcessHAStockInfo_IF5DB()
        {
            try
            {
                HNXIF5DatabaseHandler.ProcessStockInfo();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("Error in ProcessHAStockInfo_IF5DB, detail: {0}", ex.Message));
                Console.ResetColor();
            }
        }

        private static void ProcessHATopPriceInfo_IF5DB()
        {
            try
            {
                HNXIF5DatabaseHandler.ProcessTopPriceInfo();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("Error int ProcessHATopPriceInfo_IF5DB, detail: {0}", ex.Message));
                Console.ResetColor();
            }
        }

        private static void ProcessHAPutAdInfo_IF5DB()
        {
            try
            {
                HNXIF5DatabaseHandler.ProcessHAPutAdInfo();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("Error in ProcessHAPutAdInfo_IF5DB, detail: {0}", ex.Message));
                Console.ResetColor();
            }
        }

        #endregion

        #region HASTC Infoshow 5 File

        private static void UpdateNewStockHA_IF5File(string status)
        {
            // Update new stock
            try
            {
                if (status != "13")
                {
                    if (!b_updatedNewStockHA)
                    {
                        HNXIF5FileHandler.UpdateHASTCSecuritiesList();
                        b_updatedNewStockHA = true;
                    }
                }
                else
                {
                    b_updatedNewStockHA = false;
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error in UpdateNewStockHA_IF5File, message=" + ex.Message);
                Console.ResetColor();
            }
        }

        private static void MonitorHASTCData_IF5File()
        {
            while (true)
            {
                bool runAnytime = ConfigurationManager.AppSettings["RunAnytime"] == "true" ? true : false;

                string status = null;

                if (runAnytime || (DateTime.Now.Hour >= 8 && DateTime.Now.Hour <= 15))
                {
                    // Read data from file and store in memory
                    HNXIF5FileHandler.ProcessData();

                    status = ProcessHAMarketInfo_IF5File();
                }

                if (runAnytime || (status != null && status != "13"))
                {
                    UpdateNewStockHA_IF5File(status);
                    ProcessHAStockInfo_IF5File();
                    ProcessHATopPriceInfo_IF5File();
                    ProcessHAPutAdInfo_IF5File();
                }

                System.Threading.Thread.Sleep(_hastcPolling);
            }
        }

        private static string ProcessHAMarketInfo_IF5File()
        {
            try
            {
                string status;
                HNXIF5FileHandler.ProcessMarketInfo(out status);
                return status;
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("Error in ProcessHAMarketInfo_IF5File, detail: {0}", ex.Message));
                Console.ResetColor();
                return null;
            }
        }

        private static void ProcessHAStockInfo_IF5File()
        {
            try
            {
                HNXIF5FileHandler.ProcessStockInfo();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("Error in ProcessHAStockInfo_IF5File, detail: {0}", ex.Message));
                Console.ResetColor();
            }
        }

        private static void ProcessHATopPriceInfo_IF5File()
        {
            try
            {
                HNXIF5FileHandler.ProcessTopPriceInfo();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("Error in ProcessHATopPriceInfo_IF5File, detail: {0}", ex.Message));
                Console.ResetColor();
            }
        }

        private static void ProcessHAPutAdInfo_IF5File()
        {
            try
            {
                HNXIF5FileHandler.ProcessHAPutAdInfo();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("Error in ProcessHAPutAdInfo_IF5File, detail: {0}", ex.Message));
                Console.ResetColor();
            }
        }

        #endregion

        #region HASTC

        /// <summary>
        /// UpdateNewStockHA
        /// </summary>
        /// <param name="status">Market Status</param>
        private static void UpdateNewStockHA(string status)
        {
            // Update new stock
            try
            {
                if (status == "5" || status == "6")
                {
                    if (!b_updatedNewStockHA)
                    {
                        HNXFileHandler.UpdateHASTCSecuritiesList(ConfigurationManager.AppSettings["HASTCDirectory"].EndsWith("/") ? ConfigurationManager.AppSettings["HASTCDirectory"] : ConfigurationManager.AppSettings["HASTCDirectory"] + "/");
                        b_updatedNewStockHA = true;
                    }
                }
                else if (status == "13")
                {
                    b_updatedNewStockHA = false;
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error in UpdateNewStockHA, message=" + ex.Message);
                Console.ResetColor();
            }
        }


        /// <summary>
        /// MonitorHASTCData
        /// </summary>
        private static void MonitorHASTCData()
        {
            while (true)
            {
                bool runAnytime = ConfigurationManager.AppSettings["RunAnytime"] == "true" ? true : false;

                string status;
                status = ProcessHAMarketInfo();
                if (runAnytime || status != "13")
                {
                    UpdateNewStockHA(status);
                    ProcessHAStockInfo();
                    ProcessHATopPriceInfo();
                    ProcessHAPutAdInfo();
                }

                System.Threading.Thread.Sleep(_hastcPolling);
            }
        }

        private static DateTime _lastProcessHAMarketInfoTime = DateTime.MinValue;

        private static string ProcessHAMarketInfo()
        {
            FileInfo file = new FileInfo(_hastcDirectory + "STS_MarketInfo.XML");

            if (file.LastWriteTime > _lastProcessHAMarketInfoTime)
            {
                try
                {
                    string status;
                    HNXFileHandler.ProcessMarketInfo(_hastcDirectory, out status);
                    _lastProcessHAMarketInfoTime = file.LastWriteTime;
                    return status;
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(string.Format("Error in ProcessHAMarketInfo, detail: {0}", ex.Message));
                    Console.ResetColor();
                    return null;
                }
            }

            return null;
        }

        private static DateTime _lastProcessHAStockInfoTime = DateTime.MinValue;
        private static void ProcessHAStockInfo()
        {
            FileInfo file = new FileInfo(_hastcDirectory + "STS_StocksInfo.XML");

            if (file.LastWriteTime > _lastProcessHAStockInfoTime)
            {
                try
                {
                    HNXFileHandler.ProcessStockInfo(_hastcDirectory);
                    _lastProcessHAStockInfoTime = file.LastWriteTime;
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(string.Format("Error in ProcessHAStockInfo, detail: {0}", ex.Message));
                    Console.ResetColor();
                }
            }
        }

        private static DateTime _lastProcessHATopPriceInfoTime = DateTime.MinValue;
        private static void ProcessHATopPriceInfo()
        {
            FileInfo file = new FileInfo(_hastcDirectory + "STS_TOP3_PRICE_A.XML");

            if (file.LastWriteTime > _lastProcessHATopPriceInfoTime)
            {
                try
                {
                    HNXFileHandler.ProcessTopPriceInfo(_hastcDirectory);
                    _lastProcessHATopPriceInfoTime = file.LastWriteTime;
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(string.Format("Error in ProcessHATopPriceInfo, detail: {0}", ex.Message));
                    Console.ResetColor();
                }
            }
        }

        private static DateTime _lastProcessHAPutAdInfoTime = DateTime.MinValue;
        private static void ProcessHAPutAdInfo()
        {
            FileInfo file = new FileInfo(_hastcDirectory + "STS_TOP3_PRICE_B.XML");

            if (file.LastWriteTime > _lastProcessHAPutAdInfoTime)
            {
                try
                {
                    HNXFileHandler.ProcessHAPutAdInfo(_hastcDirectory);
                    _lastProcessHAPutAdInfoTime = file.LastWriteTime;
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(string.Format("Error in ProcessHAPutAdInfo, detail: {0}", ex.Message));
                    Console.ResetColor();
                }
            }
        }
        #endregion

        #region UPCOM Infoshow 5 DB
        /// <summary>
        /// UpdateNewStockUC
        /// </summary>
        /// <param name="status">Market Status</param>
        private static void UpdateNewStockUC_IF5DB(string status)
        {
            // Update new stock
            try
            {
                if (status == "5" || status == "6")
                {
                    if (!b_updatedNewStockUC)
                    {
                        UPCOMIF5DatabaseHandler.UpdateUPCOMSecuritiesList();
                        b_updatedNewStockUC = true;
                    }
                }
                else if (status == "13")
                {
                    b_updatedNewStockUC = false;
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error in UpdateNewStockUC_IF5DB, message=" + ex.Message);
                Console.ResetColor();
            }
        }

        /// <summary>
        /// Monitor UPCOM Data
        /// </summary>
        private static void MonitorUPCOMData_IF5DB()
        {
            while (true)
            {
                try
                {
                    bool runAnytime = ConfigurationManager.AppSettings["RunAnytime"] == "true" ? true : false;

                    string status = null;

                    if (runAnytime || (DateTime.Now.Hour >= 8 && DateTime.Now.Hour <= 15))
                    {
                        status = ProcessUPCOMMarketInfo_IF5DB();
                    }

                    if (runAnytime || (status != null && status != "13"))
                    {
                        //UpdateNewStockUC_IF5DB(status);
                        ProcessUPCOMStockInfo_IF5DB();
                        ProcessUPCOMTopPriceInfo_IF5DB();
                    }
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in MonitorUPCOMData_IF5DB, detail:" + e.Message);
                    Console.ResetColor();
                }

                System.Threading.Thread.Sleep(_upcomPolling);
            }
        }
        //private static DateTime _lastProcessUPCOMMarketInfoTime = DateTime.MinValue;

        private static string ProcessUPCOMMarketInfo_IF5DB()
        {
            try
            {
                string status;
                UPCOMIF5DatabaseHandler.ProcessMarketInfo(out status);
                return status;
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error in ProcessUPCOMMarketInfo_IF5DB, detail:" + ex.Message);
                Console.ResetColor();
                return null;
            }
        }

        //private static DateTime _lastProcessUPCOMStockInfoTime = DateTime.MinValue;
        private static void ProcessUPCOMStockInfo_IF5DB()
        {
            try
            {
                UPCOMIF5DatabaseHandler.ProcessStockInfo();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error in ProcessUPCOMStockInfo_IF5DB, detail:" + ex.Message);
                Console.ResetColor();
            }
        }

        //private static DateTime _lastProcessUPCOMTopPriceInfoTime = DateTime.MinValue;
        private static void ProcessUPCOMTopPriceInfo_IF5DB()
        {
            try
            {
                UPCOMIF5DatabaseHandler.ProcessTopPriceInfo();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error in ProcessUPCOMTopPriceInfo_IF5DB, detail:" + ex.Message);
                Console.ResetColor();
            }
        }
        #endregion

        #region UPCOM

        /// <summary>
        /// UpdateNewStockUC
        /// </summary>
        /// <param name="status">Market Status</param>
        private static void UpdateNewStockUC(string status)
        {
            // Update new stock
            try
            {
                if (status == "5" || status == "6")
                {
                    if (!b_updatedNewStockUC)
                    {
                        UPCOMFileHandler.UpdateUPCOMSecuritiesList(ConfigurationManager.AppSettings["UPCOMDirectory"].EndsWith("/") ? ConfigurationManager.AppSettings["UPCOMDirectory"] : ConfigurationManager.AppSettings["UPCOMDirectory"] + "/");
                        b_updatedNewStockUC = true;
                    }
                }
                else if (status == "13")
                {
                    b_updatedNewStockUC = false;
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error in UpdateNewStockUC, message=" + ex.Message);
                Console.ResetColor();
            }
        }

        /// <summary>
        /// Monitor UPCOM Data
        /// </summary>
        private static void MonitorUPCOMData()
        {
            while (true)
            {
                try
                {
                    bool runAnytime = ConfigurationManager.AppSettings["RunAnytime"] == "true" ? true : false;

                    string status = null;

                    if (runAnytime || (DateTime.Now.Hour >= 8 && DateTime.Now.Hour <= 16))
                    {
                        status = ProcessUPCOMMarketInfo();
                    }

                    if (runAnytime || status != "13")
                    {
                        //UpdateNewStockHA(status);
                        ProcessUPCOMStockInfo();
                        ProcessUPCOMTopPriceInfo();
                    }
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in MonitorUPCOMData, detail:" + e.Message);
                    Console.ResetColor();
                }

                System.Threading.Thread.Sleep(_upcomPolling);
            }
        }
        private static DateTime _lastProcessUPCOMMarketInfoTime = DateTime.MinValue;

        private static string ProcessUPCOMMarketInfo()
        {
            FileInfo file = new FileInfo(_upcomDirectory + "STSO_MarketInfo.XML");

            if (file.LastWriteTime > _lastProcessUPCOMMarketInfoTime)
            {
                try
                {
                    string status;
                    UPCOMFileHandler.ProcessMarketInfo(_upcomDirectory, out status);
                    _lastProcessUPCOMMarketInfoTime = file.LastWriteTime;
                    return status;
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(string.Format("Error in ProcessUPCOMMarketInfo, detail: {0}", ex.Message));
                    Console.ResetColor();
                    return null;
                }
            }
            return null;
        }

        private static DateTime _lastProcessUPCOMStockInfoTime = DateTime.MinValue;
        private static void ProcessUPCOMStockInfo()
        {
            FileInfo file = new FileInfo(_upcomDirectory + "STSO_StocksInfo.XML");

            if (file.LastWriteTime > _lastProcessUPCOMStockInfoTime)
            {
                try
                {
                    UPCOMFileHandler.ProcessStockInfo(_upcomDirectory);
                    _lastProcessUPCOMStockInfoTime = file.LastWriteTime;
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(string.Format("Error in ProcessUPCOMStockInfo, detail: {0}", ex.Message));
                    Console.ResetColor();
                }
            }
        }

        private static DateTime _lastProcessUPCOMTopPriceInfoTime = DateTime.MinValue;
        private static void ProcessUPCOMTopPriceInfo()
        {
            FileInfo file = new FileInfo(_upcomDirectory + "STSO_BESTPRICE.XML");

            if (file.LastWriteTime > _lastProcessUPCOMTopPriceInfoTime)
            {
                try
                {
                    UPCOMFileHandler.ProcessTopPriceInfo(_upcomDirectory);
                    _lastProcessUPCOMTopPriceInfoTime = file.LastWriteTime;
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(string.Format("Error Processing Top 3 Price Information File: {0}", ex.Message));
                    Console.ResetColor();
                }
            }
        }
        #endregion


    }
}
